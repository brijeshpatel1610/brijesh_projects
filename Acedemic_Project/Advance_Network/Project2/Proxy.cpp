
/*---------------------------------------------------------------------------*/
/*   Name = Brijesh Patel                                                    */
/*   StudentId = 800597576                                                   */
/*   emailId = brpatel@siue.edu                                              */
/*   Program Name = Server.cpp                                               */
/*                                                                           */
/*  -------------------------------------------------------------------------*/

/*----- Include files -------------------------------------------------------*/

/*----- Include files -------------------------------------------------------*/
#include <iostream>  
#include <stdio.h>          /* Needed for printf()                           */
#include <string.h>         /* Needed for memcpy() and strcpy()              */
#include <time.h>           /* Needed for clock() and CLK_TCK                */
#include <fcntl.h>          /* for O_WRONLY, O_CREAT                         */
#include <stdlib.h>   
#include <sys/stat.h> 
#include <stddef.h>   
#include <process.h>  
#include <io.h>       
#include <windows.h>        /* Needed for all Winsock stuff                  */
#include <sys\timeb.h>      /* Needed for ftime() and timeb structure        */
using namespace std;
/*----- Defines -------------------------------------------------------------*/
#define  SERV_PORT_NUM  1080   /* Server Side Port number                    */
#define  PROXY_PORT_NUM 9080   
#define  MAX_BUFFER     4096    /* Maximum Buffer Size                        */
#define INTERVAL    1  
#define SERVER_IP "127.0.0.1"

/*===== The Main ============================================================*/

/*------ Prototypes ---------------------------------------------------------*/
void sleep(clock_t wait);      /* wait for specific time (in milisecond)     */

void ClientToProxyThread(void *data);
void ProxyToServerThread(void *data);

int nChild1_status;
int nChild2_status;

SOCKADDR_IN   server_addr;
unsigned int serverSocket;
unsigned int serverConnect;

int main(int argc, char *argv[])
{
	WORD sockVersion;
	WSADATA wsaData;

	sockVersion = MAKEWORD(1, 1);			// We'd like Winsock version 1.1
	WSAStartup(sockVersion, &wsaData);      // We begin by initializing Winsock

	int nret;

	// Next, create the listening socket
	SOCKET listeningSocket;

	// Use a SOCKADDR_IN struct to fill in address information
	SOCKADDR_IN proxyServerInfo;
	proxyServerInfo.sin_family = AF_INET;
	proxyServerInfo.sin_addr.s_addr = INADDR_ANY;	// Since this socket is listening for connections, any local address will do
	proxyServerInfo.sin_port = htons(PROXY_PORT_NUM);		// Convert integer 8888 to network-byte order and insert into the port field

	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(SERVER_IP);
	server_addr.sin_port = htons(int(SERV_PORT_NUM));

	// Create socket for a client
	SOCKET theClient;

	// Requested ClientInfo
	struct sockaddr_in clientInfo;
	clientInfo.sin_family = AF_INET;
	int clientInfoLen = sizeof(clientInfo);

	// create the socket for server process
	listeningSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);		// Use TCP rather than UDP

	if (listeningSocket == INVALID_SOCKET)
	{
		nret = WSAGetLastError();		// Get a more detailed error
		WSACleanup();				// Shutdown Winsock
	}

	// Bind the socket to our local server address
	nret = bind(listeningSocket, (LPSOCKADDR)&proxyServerInfo, sizeof(struct sockaddr));

	if (nret == SOCKET_ERROR)
	{
		nret = WSAGetLastError();
		WSACleanup();
	}
	// Make the socket listen
	nret = listen(listeningSocket, 10);		// Up to 10 connections may wait at any
	if (nret == SOCKET_ERROR)
	{
		nret = WSAGetLastError();
		WSACleanup();
	}

	printf("Waiting for incoming connections.. \n");

	while (1) {
		//if a connection is found: show the message!
		theClient = accept(listeningSocket, (LPSOCKADDR)&clientInfo, &clientInfoLen);
		printf("Incoming connection accepted \n");

		nChild1_status = true;
		nChild2_status = true;
		serverSocket = socket(AF_INET, SOCK_STREAM, 0);
		serverConnect = connect(serverSocket, (struct sockaddr *)&server_addr, sizeof(server_addr));

		if (theClient == INVALID_SOCKET)
		{
			printf("Accept failed with error code: %d\n", WSAGetLastError());
			break;
		}


		if (_beginthread(ClientToProxyThread, MAX_BUFFER, (void *)theClient) < 0)
		{
			printf("ERROR - Unable to create client to proxy thread \n");
			exit(1);
		}


		/* Spin-loop until both client threads finish --- */
		while (nChild1_status == true)
		{
			;
		}

		if (_beginthread(ProxyToServerThread, MAX_BUFFER, (void *)theClient) < 0)
		{
			printf("ERROR - Unable to create proxy to server thread \n");
			exit(1);
		}
		/* Spin-loop until both server threads finish --- */

		while (nChild2_status == true)
		{
			;
		}

		closesocket(theClient);
		closesocket(serverSocket);
	}

	closesocket(listeningSocket);
	WSACleanup();
	return(1);
}

void ClientToProxyThread(void *data)
{
	char           in_buf[MAX_BUFFER];
	unsigned int clientSocket = (unsigned int)data;
	unsigned int receiveFromClient = recv(clientSocket, in_buf, MAX_BUFFER, 0);
	

	if (receiveFromClient < 0)
	{
		printf("Cannot read");
	}
	else
	{
		send(serverSocket, in_buf, MAX_BUFFER, 0);
	}

	nChild1_status = false;
	_endthread();
}

void ProxyToServerThread(void *data)
{

	char out_buf[MAX_BUFFER];
	
	unsigned int receiveFromServer = recv(serverSocket, out_buf, MAX_BUFFER, 0);
	
	if (receiveFromServer < 0)
	{
		printf("Cannot read");
	}
	else
	{
		unsigned int clientSocket = (unsigned int)data;
		send(clientSocket, out_buf, MAX_BUFFER, 0);
	}

	nChild2_status = false;
	_endthread();

}
