
/*---------------------------------------------------------------------------*/
/*   Name = Brijesh Patel                                                    */
/*   StudentId = 800597576                                                    */
/*   emailId = brpatel@siue.edu                                                    */
/*   Program Name = Server.cpp                                               */
/*                                                                           */
/*  -------------------------------------------------------------------------*/

/*----- Include files -------------------------------------------------------*/
#include <stdio.h>          /* Needed for printf()                           */
#include <string.h>         /* Needed for memcpy() and strcpy()              */
#include <time.h>           /* Needed for clock() and CLK_TCK                */
#include <fcntl.h>          /* for O_WRONLY, O_CREAT                         */

#include <windows.h>        /* Needed for all Winsock stuff                  */
#include <sys\timeb.h>      /* Needed for ftime() and timeb structure        */

/*----- Defines -------------------------------------------------------------*/
#define  SERV_PORT_NUM  1050   /* Server Side Port number                    */
#define  MAX_BUFFER     100    /* Maximum Buffer Size                        */
#define  NUM_LOOPS      5     /* Number of client repeats                   */
#define  PROHIBITED_CLIENT  "192.1.200.15"    /* Prohibited client IP address                 */

/*------ Prototypes ---------------------------------------------------------*/
void sleep(clock_t wait);      /* wait for specific time (in milisecond)     */

							   /*===== The Main ============================================================*/
void main()
{

	WORD sockVersion;
	WSADATA wsaData;

	sockVersion = MAKEWORD(1, 1);			// We'd like Winsock version 1.1
	WSAStartup(sockVersion, &wsaData);      // We begin by initializing Winsock


	char in_buf[MAX_BUFFER];            /* The in-coming buffer              */
	int nret;

	// Next, create the listening socket
	SOCKET listeningSocket;

	// Use a SOCKADDR_IN struct to fill in address information
	SOCKADDR_IN serverInfo;
	serverInfo.sin_family = AF_INET;
	serverInfo.sin_addr.s_addr = INADDR_ANY;	// Since this socket is listening for connections, any local address will do
	serverInfo.sin_port = htons(SERV_PORT_NUM);		// Convert integer 8888 to network-byte order and insert into the port field

													// Create socket for a client
	SOCKET theClient;

	// Requested ClientInfo
	struct sockaddr_in clientInfo;
	clientInfo.sin_family = AF_INET;
	int clientInfoLen = sizeof(clientInfo);

	// Prohibited ClientInfo
	struct sockaddr_in prohibitedClientInfo;
	prohibitedClientInfo.sin_family = AF_INET;
	prohibitedClientInfo.sin_addr.s_addr = inet_addr(PROHIBITED_CLIENT);


	// create the socket for server process
	listeningSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);		// Use TCP rather than UDP

	if (listeningSocket == INVALID_SOCKET)
	{
		nret = WSAGetLastError();		// Get a more detailed error
		WSACleanup();				// Shutdown Winsock
	}

	// Bind the socket to our local server address
	nret = bind(listeningSocket, (LPSOCKADDR)&serverInfo, sizeof(struct sockaddr));

	if (nret == SOCKET_ERROR)
	{
		nret = WSAGetLastError();
		WSACleanup();
	}

	// Make the socket listen
	nret = listen(listeningSocket, 10);		// Up to 10 connections may wait at any
	if (nret == SOCKET_ERROR)
	{
		nret = WSAGetLastError();
		WSACleanup();
	}


	while (1) {

		//if a connection is found: show the message!
		theClient = accept(listeningSocket, (LPSOCKADDR)&clientInfo, &clientInfoLen);
		printf("----------------------------------------------------\n");
		printf("a connection request arrived!\n");
		printf("Client's IP address is: %s\n", inet_ntoa(clientInfo.sin_addr));
		printf("Client's port is: %d\n", (int)ntohs(clientInfo.sin_port));

		/* Receiving a message from the server */
		int NumBytes = recv(theClient, in_buf, MAX_BUFFER, 0);
		if (NumBytes < 0)
		{
			printf("Receiving Error ... \n");
		}
		else
		{
			in_buf[NumBytes] = '\0';
			printf("Reply Received: %d bytes received.\n", NumBytes);
			/* Display the returned clientId */
			printf("Request from client : %s\n", in_buf);
			printf("----------------------------------------------------\n");
		}

		/*If it is the prohibited client, the server returns a message for
		rejection (�Your access is denied by this server!�) to the client,
		instead of the timestamp message*/
		if (clientInfo.sin_addr.s_addr == prohibitedClientInfo.sin_addr.s_addr) {
			char message[256] = "Your access is denied by this server!";

			nret = send(theClient, message, sizeof(message), 0);   // Send to client
		}
		else {
			char time[MAX_BUFFER];                /* for loacl time stamp              */
			_strtime_s(time);                    /* retrieve local time stamp         */

			nret = send(theClient, time, sizeof(time), 0);   // Send to client
		}

		/* Insert 1 second delay */
		sleep((clock_t)1 * CLOCKS_PER_SEC);



		// Send and receive from the client, and finally,
		closesocket(theClient);

	}

	closesocket(listeningSocket);


	// Shutdown Winsock
	WSACleanup();
}

/* Wait for specific time (in milisecond) ---------------------------------- */
void sleep(clock_t wait)
{
	clock_t goal;
	goal = wait + clock();
	while (goal > clock());
}
