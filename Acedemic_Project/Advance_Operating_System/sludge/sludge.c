#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <openssl/md5.h>
#include <unistd.h>

#define MAX_DUP_FILE 10

#define MAX_FILENAME_SIZE 256

struct header {
	char *name[MAX_DUP_FILE];
	unsigned char hash[MD5_DIGEST_LENGTH];
	int length;
	int dup_file_count;
};


void computeHash(char * filename, struct header *h);

int main(int argc, char **argv)
{



	if (argc < 3) {
		printf("Please execute gunk with correct argument\n");
	}
	else if (argc == 3 && strcmp(argv[1], "-l") == 0) {
		FILE *archivefp = fopen(argv[2], "a+");
		if (archivefp == NULL) {
			printf("Error opening a file name %s\n", argv[2]);
		}
		else {


			char *fileName[MAX_DUP_FILE];
			unsigned char fileHash[MD5_DIGEST_LENGTH];
			char filelength[4], dupfilecount[4];
			long int sizeOfArchive = 0, totalBytePassed = 0;
			fseek(archivefp, 0, SEEK_END);

			sizeOfArchive = ftell(archivefp);
			fseek(archivefp, 0, SEEK_SET);
			printf("size Of archive--%ld\n",sizeOfArchive);
			if (sizeOfArchive > 0) {
				while (totalBytePassed < sizeOfArchive) {
					printf("archivefp---%ld\n",ftell(archivefp));
					int read=fread(fileName, 1, sizeof(char *) * (MAX_DUP_FILE), archivefp);
					printf("archivefp---%s\n",fileName[0]);
					fread(fileHash, 1, MD5_DIGEST_LENGTH, archivefp);
					printf("archivefp---%ld\n",ftell(archivefp));
					fread(filelength, 1, 4, archivefp);
					printf("archivefp---%ld\n",ftell(archivefp));
					fread(dupfilecount, 1, 4, archivefp);
					printf("archivefp---%ld\n",ftell(archivefp));
					int no_of_duplicate_file = *(int *)dupfilecount;
					printf("no_of_duplicate_file---%d\n",no_of_duplicate_file);
					//char tempfilename[255];
					for (int fileCount = 0; fileCount <= no_of_duplicate_file; fileCount++) {
						printf("test\n");
							printf("%s\n",fileName[fileCount]);
					}
					
					fseek(archivefp, *(int *)filelength, SEEK_CUR);
					totalBytePassed = ftell(archivefp);

					if (totalBytePassed == sizeOfArchive) {
						break;
					}

				}

			}

		}
	}
	else if (argc > 3) {

		if (strcmp(argv[1], "-c") == 0) {
			FILE *archivefp = fopen(argv[2], "w+");
			for (int i = 3; i < argc; i++) {
				char buffer[1024];
				struct header h1;
				computeHash(argv[i], &h1);


				////////////////////////////////
				int read;
				char *filename[MAX_DUP_FILE];
				unsigned char fileHash[MD5_DIGEST_LENGTH];
				char filelength[4], dupfilecount[4];
				long int sizeOfArchive = 0, totalBytePassed = 0;
				fseek(archivefp, 0, SEEK_END);

				sizeOfArchive = ftell(archivefp);
				fseek(archivefp, 0, SEEK_SET);
				int duplicateFound = 0;

				if (sizeOfArchive > 0) {
					while (totalBytePassed < sizeOfArchive) {
						fread(filename, 1, sizeof(char *) * (MAX_DUP_FILE), archivefp);
						fread(fileHash, 1, MD5_DIGEST_LENGTH, archivefp);
						int ret = memcmp(fileHash, h1.hash, MD5_DIGEST_LENGTH);
						fread(filelength, 1, 4, archivefp);
						fread(dupfilecount, 1, 4, archivefp);
						if (ret == 0) {
							printf("\nmemcompare\n");

							printf("\n%d\n", ret);
							duplicateFound = 1;
							int no_of_duplicate_file = *(int *)dupfilecount;
							int sameFileName = 0;
							for (int fileCount = 0; fileCount <= no_of_duplicate_file; fileCount++) {
								printf("test--%s\n",filename[fileCount]);
								if (strcmp(filename[fileCount], argv[i]) == 0) {
									sameFileName = 1;
									break;

								}

							}
							if (sameFileName == 1) {
								printf("\nsame file name--\n");

							}
							else {
								fseek(archivefp, (-1) * 4, SEEK_CUR);
								fseek(archivefp, (-1) * 4, SEEK_CUR);
								fseek(archivefp, (-1) * MD5_DIGEST_LENGTH, SEEK_CUR);
								fseek(archivefp, (-1) * sizeof(char *) * (MAX_DUP_FILE), SEEK_CUR);
								filename[++no_of_duplicate_file] = malloc(MAX_FILENAME_SIZE);
								strcpy(filename[no_of_duplicate_file], argv[i]);

								printf("new filename %s\n", (char *)filename[no_of_duplicate_file]);

								fwrite(filename, sizeof(char *) * (MAX_DUP_FILE), 1, archivefp);
								fseek(archivefp, MD5_DIGEST_LENGTH, SEEK_CUR);
								fseek(archivefp, 4, SEEK_CUR);
								fwrite(&no_of_duplicate_file, 4, 1, archivefp);
								printf("file pointer %ld\n", ftell(archivefp));
								fseek(archivefp, 0, SEEK_SET);
								printf("file pointer %ld\n", ftell(archivefp));
							}
							break;

						}


						fseek(archivefp, *(int *)filelength, SEEK_CUR);
						totalBytePassed = ftell(archivefp);

						if (totalBytePassed == sizeOfArchive) {
							break;
						}

					}

				}

				///////////////////////////////
				if (duplicateFound == 0) {

					h1.dup_file_count = 0;
					h1.name[h1.dup_file_count] = malloc(MAX_FILENAME_SIZE);
					strcpy(h1.name[h1.dup_file_count], argv[i]);

					FILE *fp = fopen(argv[i], "r");
					if (fp == NULL) {
						printf("Error opening a file name %s\n", argv[i]);
					}
					else {
						fseek(fp, 0, SEEK_END);
						h1.length = ftell(fp);

						fseek(fp, 0, SEEK_SET);


						fwrite(&h1, sizeof(struct header), 1, archivefp);

						while ((read = fread(buffer, 1, 1024, fp)) > 0) {
							fwrite(buffer, read, 1, archivefp);
						}

						fclose(fp);

					}
				}

			}
			fclose(archivefp);
		}
	}
	return 0;
}


void computeHash(char * filename, struct header *h) {

	unsigned char hashstring[MD5_DIGEST_LENGTH];
	printf("filename in hash-- %s\n", (char *)filename);

	FILE *inFile = fopen(filename, "rb");
	MD5_CTX mdContext;
	int bytes;
	unsigned char data[1024];

	if (inFile == NULL) {
		printf("%s can't be opened.\n", filename);
	}

	MD5_Init(&mdContext);
	while ((bytes = fread(data, 1, 1024, inFile)) != 0)
		MD5_Update(&mdContext, data, bytes);

	MD5_Final(hashstring, &mdContext);
	printf("\n");
	//printf("hashstring--%s--\n",(char *)hashstring);

	for (int i = 0; i < MD5_DIGEST_LENGTH; i++) {
		h->hash[i] = hashstring[i];
	}


	fclose(inFile);
}
