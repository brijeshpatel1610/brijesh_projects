#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <openssl/md5.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#define MAX_FILENAME_SIZE 1024
#define DUP_FILE_NAME "dupArchive.sludge"

struct header {
	char name[MAX_FILENAME_SIZE];
	unsigned char hash[MD5_DIGEST_LENGTH];
	int length;
};

void createDirectoryIfNotExists(char *directoryPath){
	struct stat st = {0};

	if (stat(directoryPath, &st) == -1) {
		mkdir(directoryPath, 0700);
	}
}



void computeHash(char * filename, struct header *h) {

	unsigned char hashstring[MD5_DIGEST_LENGTH];

	FILE *inFile = fopen(filename, "rb");
	MD5_CTX mdContext;
	int bytes;
	unsigned char data[1024];

	if (inFile == NULL) {
		printf("%s can't be opened.\n", filename);
	}

	MD5_Init(&mdContext);
	while ((bytes = fread(data, 1, 1024, inFile)) != 0)
		MD5_Update(&mdContext, data, bytes);

	MD5_Final(hashstring, &mdContext);

	for (int i = 0; i < MD5_DIGEST_LENGTH; i++) {
		h->hash[i] = hashstring[i];
	}


	fclose(inFile);
}

int checkDuplicateFiles(struct header *h1, char **argv, int fileIndex) {
	FILE *archive = fopen(argv[2], "r+");
	if (archive == NULL) {
		return 0;
	}
	else {

		char filename[MAX_FILENAME_SIZE] = "";
		unsigned char fileHash[MD5_DIGEST_LENGTH];
		char filelength[4];
		long int sizeOfArchive = 0, totalBytePassed = 0;

		fseek(archive, 0, SEEK_END);
		sizeOfArchive = ftell(archive);
		fseek(archive, 0, SEEK_SET);

		int duplicateFound = 0;

		if (sizeOfArchive > 0) {
			while (totalBytePassed < sizeOfArchive) {
				fread(filename, 1, MAX_FILENAME_SIZE, archive);
				fread(fileHash, 1, MD5_DIGEST_LENGTH, archive);
				fread(filelength, 1, 4, archive);
				int ret = memcmp(fileHash, h1->hash, MD5_DIGEST_LENGTH);
				if (ret == 0) {
					duplicateFound = 1;
					int sameFileName = 0;
					char newFileName[MAX_FILENAME_SIZE];
					strcpy(newFileName, filename);
					const char delim[2] = ";";
					char *token = strtok(newFileName, delim);
					while (token != NULL)
					{
						if (strcmp(token, argv[fileIndex]) == 0) {
							sameFileName = 1;
							break;

						}
						token = strtok(NULL, delim);

					}

					if (sameFileName == 1) {
						printf("\n%s already exists\n", argv[fileIndex]);

					}
					else {

						fseek(archive, (-1) * 4, SEEK_CUR);
						fseek(archive, (-1) * MD5_DIGEST_LENGTH, SEEK_CUR);
						fseek(archive, (-1) * MAX_FILENAME_SIZE, SEEK_CUR);

						strcat(filename, argv[fileIndex]);
						strcat(filename, ";");


						fwrite(filename, MAX_FILENAME_SIZE, 1, archive);
						fseek(archive, MD5_DIGEST_LENGTH, SEEK_CUR);
						fseek(archive, 4, SEEK_CUR);
						fseek(archive, 0, SEEK_SET);
					}
					break;
				}


				fseek(archive, *(int *)filelength, SEEK_CUR);
				totalBytePassed = ftell(archive);

				if (totalBytePassed == sizeOfArchive) {
					break;
				}

			}

		}

		fclose(archive);
		return duplicateFound;
	}

}

void addfiles(int argc, char **argv) {

	if (strcmp(argv[1], "-c") == 0) {
		FILE *truncateArchive = fopen(argv[2], "w");
		fclose(truncateArchive);
	}


	for (int i = 3; i < argc; i++) {
		char buffer[1024];
		int read;
		struct header h1;
		computeHash(argv[i], &h1);


		////////////////////////////////
		////Check Duplicate File////////
		////////////////////////////////

		int duplicateFound = checkDuplicateFiles(&h1, argv, i);
		///////////////////////////////
		if (duplicateFound == 0) {
			strcpy(h1.name, argv[i]);
			strcat(h1.name, ";");


			FILE *archivefp = fopen(argv[2], "a");

			FILE *fp = fopen(argv[i], "r");
			if (fp == NULL) {
				printf("Error opening a file name %s\n", argv[i]);
			}
			else {
				fseek(fp, 0, SEEK_END);
				h1.length = ftell(fp);

				fseek(fp, 0, SEEK_SET);

				fwrite(&h1, sizeof(struct header), 1, archivefp);

				while ((read = fread(buffer, 1, 1024, fp)) > 0) {
					fwrite(buffer, read, 1, archivefp);
				}

				fclose(fp);

			}
			fclose(archivefp);
		}

	}

}


int copy_file(char *old_filename, char  *new_filename)
{
	FILE  *ptr_old, *ptr_new;
	int  a;

	ptr_old = fopen(old_filename, "r");
	ptr_new = fopen(new_filename, "w");

	if (ptr_old == NULL)
		return  -1;

	while (1)
	{
		a = fgetc(ptr_old);

		if (!feof(ptr_old))
			fputc(a, ptr_new);
		else
			break;
	}

	fclose(ptr_new);
	fclose(ptr_old);
	return  0;
}

void listFiles(int argc, char **argv){
		FILE *archivefp = fopen(argv[2], "r");
		if (archivefp == NULL) {
			printf("Error opening a file name %s\n", argv[2]);
		}
		else {

			char filename[MAX_FILENAME_SIZE] = "";
			unsigned char fileHash[MD5_DIGEST_LENGTH];
			char filelength[4];
			long int sizeOfArchive = 0, totalBytePassed = 0;

			fseek(archivefp, 0, SEEK_END);
			sizeOfArchive = ftell(archivefp);
			fseek(archivefp, 0, SEEK_SET);

			if (sizeOfArchive > 0) {
				while (totalBytePassed < sizeOfArchive) {
					fread(filename, 1, MAX_FILENAME_SIZE, archivefp);
					fread(fileHash, 1, MD5_DIGEST_LENGTH, archivefp);
					fread(filelength, 1, 4, archivefp);

					const char delim[2] = ";";
					char *token = strtok(filename, delim);
					while (token != NULL)
					{
						printf("%s\n", token);
						token = strtok(NULL, delim);

					}


					fseek(archivefp, *(int *)filelength, SEEK_CUR);
					totalBytePassed = ftell(archivefp);

					if (totalBytePassed == sizeOfArchive) {
						break;
					}

				}

			}
			fclose(archivefp);
		}


}

void extractFiles(int argc, char **argv){
		FILE *archivefp = fopen(argv[2], "r");
		if (archivefp == NULL) {
			printf("Error opening a filename %s\n", argv[2]);
		}
		else {
		
			
			char filename[MAX_FILENAME_SIZE] = "";
			unsigned char fileHash[MD5_DIGEST_LENGTH];
			char filelength[4], buffer[1];
			long int sizeOfArchive = 0;
			fseek(archivefp, 0, SEEK_END);
			sizeOfArchive = ftell(archivefp);
			fseek(archivefp, 0, SEEK_SET);

			if (argc > 3) {

				for (int i = 3; i < argc; i++) {
					fseek(archivefp, 0, SEEK_SET);
					int searchAhead = 1;
					long int totalBytePassed = 0;

					while (searchAhead == 1 && totalBytePassed < sizeOfArchive) {

						fread(filename, 1, MAX_FILENAME_SIZE, archivefp);
						fread(fileHash, 1, MD5_DIGEST_LENGTH, archivefp);
						fread(filelength, 1, 4, archivefp);
						char newFileName[MAX_FILENAME_SIZE] = "";
						strcpy(newFileName, filename);

						const char delim[2] = ";";
						char *token = strtok(newFileName, delim);
						while (token != NULL)
						{
							if (strcmp(token, argv[i]) == 0) {
							
								//create directory if not exists
								char *last = strrchr(token, '/');
								char directoryPath[255];
								if (last != NULL) {
									memcpy(directoryPath,token,(last-token));
									createDirectoryIfNotExists(directoryPath);
								}
								
								searchAhead = 0;
								FILE *newFilefp = fopen(argv[i], "w");
								long int fileLength = *(int *)filelength;

								long int read = 0;
								int read1 = 0;
								while (read < fileLength) {
									read1 = fread(buffer, 1, 1, archivefp);
									fwrite(buffer, read1, 1, newFilefp);
									read += read1;
								}


								fclose(newFilefp);
								break;

							}
							token = strtok(NULL, delim);

						}


						if (searchAhead == 0) {
							break;

						}
						else {

							fseek(archivefp, *(int *)filelength, SEEK_CUR);
							totalBytePassed = ftell(archivefp);

							if (totalBytePassed == sizeOfArchive) {
								printf("\nFile %s  not found in %s\n", argv[i], argv[2]);
								break;
							}
						}
					}


				}
			}
			else {

				long int totalBytePassed = 0;

				while (totalBytePassed < sizeOfArchive) {

					fread(filename, 1, MAX_FILENAME_SIZE, archivefp);
					fread(fileHash, 1, MD5_DIGEST_LENGTH, archivefp);
					fread(filelength, 1, 4, archivefp);
					char newFileName[MAX_FILENAME_SIZE] = "";
					strcpy(newFileName, filename);

					const char delim[2] = ";";
					char *token = strtok(newFileName, delim);
					long int fileLength = *(int *)filelength;

					while (token != NULL)
					{
						//create directory if not exists
						char *last = strrchr(token, '/');
						char directoryPath[255];
						if (last != NULL) {
							memcpy(directoryPath,token,(last-token));
							directoryPath[last-token] = '\0';
							createDirectoryIfNotExists(directoryPath);
						}
					
						FILE *newFilefp = fopen(token, "w");

						long int read = 0;
						int read1 = 0;
						while (read < fileLength) {
							read1 = fread(buffer, 1, 1, archivefp);
							fwrite(buffer, read1, 1, newFilefp);
							read += read1;
						}


						fclose(newFilefp);
						token = strtok(NULL, delim);
						fseek(archivefp, (-1)*fileLength, SEEK_CUR);

					}

					fseek(archivefp, fileLength, SEEK_CUR);
					totalBytePassed = ftell(archivefp);
					if (totalBytePassed == sizeOfArchive) {
						break;
					}

				}

			}
			fclose(archivefp);
		}


}

void removeFiles(int argc, char **argv){
		copy_file(argv[2], DUP_FILE_NAME);
		FILE *duparchivefp = fopen(DUP_FILE_NAME, "r+");
		if (duparchivefp == NULL) {
			printf("Error opening a filename dupArchive.sludge\n");
		}
		else {
			FILE *archivefp = fopen(argv[2], "w+");
			char filename[MAX_FILENAME_SIZE] = "";
			unsigned char fileHash[MD5_DIGEST_LENGTH];
			char filelength[4];
			long int sizeOfArchive = 0, totalBytePassed = 0;

			fseek(duparchivefp, 0, SEEK_END);
			sizeOfArchive = ftell(duparchivefp);
			fseek(duparchivefp, 0, SEEK_SET);


			if (sizeOfArchive > 0) {
				while (totalBytePassed < sizeOfArchive) {

					fread(filename, 1, MAX_FILENAME_SIZE, duparchivefp);
					char newFileName[MAX_FILENAME_SIZE] = "";
					const char delim[2] = ";";
					char *token = strtok(filename, delim);
					int file_count = 0;
					int fileFound = 0;
					while (token != NULL)
					{

						if (strcmp(token, argv[3]) == 0) {
							fileFound = 1;
						}
						else {
							file_count++;
							strcat(newFileName, token);
							strcat(newFileName, ";");
						}
						token = strtok(NULL, delim);

					}
					if ((fileFound == 1 && file_count > 0) || (fileFound == 0)) {

						fwrite(newFileName, MAX_FILENAME_SIZE, 1, archivefp);
						fread(fileHash, 1, MD5_DIGEST_LENGTH, duparchivefp);
						fwrite(fileHash, MD5_DIGEST_LENGTH, 1, archivefp);
						fread(filelength, 1, 4, duparchivefp);
						fwrite(filelength, 4, 1, archivefp);
						char buffer[*(int *)filelength];
						int read = fread(buffer, 1, *(int *)filelength, duparchivefp);
						fwrite(buffer, read, 1, archivefp);



					}
					else if (fileFound == 1 && file_count == 0) {
						fseek(duparchivefp, MD5_DIGEST_LENGTH, SEEK_CUR);
						fread(filelength, 1, 4, duparchivefp);
						fseek(duparchivefp, *(int *)filelength, SEEK_CUR);
					}

					totalBytePassed = ftell(duparchivefp);

					if (totalBytePassed == sizeOfArchive) {
						break;
					}

				}

			}

			///////////////////////////////
			fclose(archivefp);



			fclose(duparchivefp);

		}
		remove(DUP_FILE_NAME);


}

int main(int argc, char **argv)
{
	if (argc < 3) {
		printf("Please execute gunk with correct argument\n");
	}
	else if (strcmp(argv[1], "-c") == 0 || strcmp(argv[1], "-a") == 0) {
		addfiles(argc, argv);
	}
	else if (argc == 3 && strcmp(argv[1], "-l") == 0) {
		listFiles(argc, argv);
	}
	else if (strcmp(argv[1], "-e") == 0) {
		extractFiles(argc, argv);
	}
	else if (strcmp(argv[1], "-r") == 0) {
		removeFiles(argc, argv);
	}

	return 0;
}
