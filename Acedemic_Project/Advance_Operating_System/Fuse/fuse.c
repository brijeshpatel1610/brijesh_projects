/*
  FUSE: Filesystem in Userspace
  Copyright (C) 2001-2007  Miklos Szeredi <miklos@szeredi.hu>

  This program can be distributed under the terms of the GNU GPL.
  See the file COPYING.

  gcc -Wall `pkg-config fuse --cflags --libs` hello.c -o hello
*/

#define FUSE_USE_VERSION 30
#define MAX_FILENAME_SIZE 1024

#include <stdio.h>
#include <fuse.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <openssl/md5.h>


#define MAX_FILENAME_SIZE 1024
#define DUP_FILE_NAME "dupArchive.sludge"

static char *archivefile;
struct header {
	char name[MAX_FILENAME_SIZE];
	unsigned char hash[MD5_DIGEST_LENGTH];
	int length;
};



void computeHash(const char * filename, struct header *h) {

	unsigned char hashstring[MD5_DIGEST_LENGTH];

	FILE *inFile = fopen(filename, "rb");
	MD5_CTX mdContext;
	int bytes;
	unsigned char data[1024];

	if (inFile == NULL) {
		printf("%s can't be opened.\n", filename);
	}

	MD5_Init(&mdContext);
	while ((bytes = fread(data, 1, 1024, inFile)) != 0)
		MD5_Update(&mdContext, data, bytes);

	MD5_Final(hashstring, &mdContext);

	for (int i = 0; i < MD5_DIGEST_LENGTH; i++) {
		h->hash[i] = hashstring[i];
	}


	fclose(inFile);
}
int checkDuplicateFiles(struct header *h1,const char *newfile) {
	FILE *archive = fopen(archivefile, "r+");
	if (archive == NULL) {
		return 0;
	}
	else {

		char filename[MAX_FILENAME_SIZE] = "";
		unsigned char fileHash[MD5_DIGEST_LENGTH];
		char filelength[4];
		long int sizeOfArchive = 0, totalBytePassed = 0;

		fseek(archive, 0, SEEK_END);
		sizeOfArchive = ftell(archive);
		fseek(archive, 0, SEEK_SET);

		int duplicateFound = 0;

		if (sizeOfArchive > 0) {
			while (totalBytePassed < sizeOfArchive) {
				fread(filename, 1, MAX_FILENAME_SIZE, archive);
				fread(fileHash, 1, MD5_DIGEST_LENGTH, archive);
				fread(filelength, 1, 4, archive);
				int ret = memcmp(fileHash, h1->hash, MD5_DIGEST_LENGTH);
				if (ret == 0) {
					duplicateFound = 1;
					int sameFileName = 0;
					char newFileName[MAX_FILENAME_SIZE];
					strcpy(newFileName, filename);
					const char delim[2] = ";";
					char *token = strtok(newFileName, delim);
					while (token != NULL)
					{
						if (strcmp(token, newfile) == 0) {
							sameFileName = 1;
							break;

						}
						token = strtok(NULL, delim);

					}

					if (sameFileName == 1) {
						printf("\n%s already exists\n", newfile);

					}
					else {

						fseek(archive, (-1) * 4, SEEK_CUR);
						fseek(archive, (-1) * MD5_DIGEST_LENGTH, SEEK_CUR);
						fseek(archive, (-1) * MAX_FILENAME_SIZE, SEEK_CUR);

						strcat(filename, newfile);
						strcat(filename, ";");


						fwrite(filename, MAX_FILENAME_SIZE, 1, archive);
						fseek(archive, MD5_DIGEST_LENGTH, SEEK_CUR);
						fseek(archive, 4, SEEK_CUR);
						fseek(archive, 0, SEEK_SET);
					}
					break;
				}


				fseek(archive, *(int *)filelength, SEEK_CUR);
				totalBytePassed = ftell(archive);

				if (totalBytePassed == sizeOfArchive) {
					break;
				}

			}

		}

		fclose(archive);
		return duplicateFound;
	}

}



static int hello_getattr(const char *path, struct stat *stbuf)
{
	int res = 0;

	memset(stbuf, 0, sizeof(struct stat));
	if (strcmp(path, "/") == 0) {
		stbuf->st_mode = S_IFDIR | 0755;
		stbuf->st_nlink = 2;
	} else  {

		//////////////////////////////////////////
		FILE *archivefp = fopen(archivefile, "r");
			char filename[MAX_FILENAME_SIZE] = "";
			char filelength[4];
			long int sizeOfArchive = 0, totalBytePassed = 0;
			int filefound=0;
			fseek(archivefp, 0, SEEK_END);
			sizeOfArchive = ftell(archivefp);
			fseek(archivefp, 0, SEEK_SET);
			
			if (sizeOfArchive > 0) {
				while (totalBytePassed < sizeOfArchive) {
					fread(filename, 1, MAX_FILENAME_SIZE, archivefp);
					fseek(archivefp, MD5_DIGEST_LENGTH, SEEK_CUR);
					fread(filelength, 1, 4, archivefp);
					int internalPath=0;
					int size=0;
					char *end_str;
					char *token =strtok_r(filename, ";",&end_str);
					
					while (token != NULL)
					{
					
						if (strcmp(path+1, token) == 0) {
							internalPath++;
							stbuf->st_mode = S_IFREG | 0666;
							stbuf->st_nlink = 1;
							stbuf->st_size = *(int *)filelength;
							filefound=1;
							break;
						}else{
							const char delim1[2] = "/";
							char dupToken[255]="";
							strcpy(dupToken,token);
							char *internalToken = strtok(dupToken, delim1);
							char newPath[255]="";
							internalPath=0;
							while (internalToken != NULL)
							{
								
								internalPath++;
								strcat(newPath,internalToken);
								
								if (strcmp(path+1, newPath) == 0) {
									
									filefound=1;
									size+=*(int *)filelength;
									
								}
								strcat(newPath,"/");
								
								
								internalToken = strtok(NULL, delim1);
							}
						}
						
						if(internalPath > 1 && filefound == 1){
							stbuf->st_mode = S_IFDIR | 0755;
							stbuf->st_nlink = 2;
							stbuf->st_size = size;
						}
						
						token =  strtok_r(NULL, ";",&end_str);
				
					}
								
					if(filefound == 1){
						break;
					}
					
					fseek(archivefp, *(int *)filelength, SEEK_CUR);
					totalBytePassed = ftell(archivefp);

					if (totalBytePassed == sizeOfArchive) {
						if(filefound == 0){
							res = -ENOENT;
						}
						break;
					}
					
				}

			}else{
				res = -ENOENT;
			}
			fclose(archivefp);
		
		//////////////////////////////////////////
		
	} 

	return res;
}


static int hello_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
			 off_t offset, struct fuse_file_info *fi)
{
	(void) offset;
	(void) fi;
	
	filler(buf, ".", NULL, 0);
	filler(buf, "..", NULL, 0);
	//////////////////////////////////////////
	FILE *archivefp = fopen(archivefile, "r");
	char filename[MAX_FILENAME_SIZE] = "";
	char filelength[4];
	long int sizeOfArchive = 0, totalBytePassed = 0;
	char directoryListing[255]="";
		
	fseek(archivefp, 0, SEEK_END);
	sizeOfArchive = ftell(archivefp);
	fseek(archivefp, 0, SEEK_SET);
			
	if (sizeOfArchive > 0) {
		while (totalBytePassed < sizeOfArchive) {
			fread(filename, 1, MAX_FILENAME_SIZE, archivefp);
			fseek(archivefp, MD5_DIGEST_LENGTH, SEEK_CUR);
			fread(filelength, 1, 4, archivefp);
			int internalPath=0;		
			
			char *end_str;
			char *token = strtok_r(filename, ";",&end_str);
			
			while (token != NULL)
			{
				
				const char delim1[2] = "/";
				char dupToken[255]="";
				strcpy(dupToken,token);
				
				char *internalToken = strtok(dupToken, delim1);
				char newPath[255]="";
				internalPath=0;
				
				while (internalToken != NULL)
				{
					
					
					/*
					The following if block will be excute if user 
					specify directory name as parameter 
					in ls command
					
					For example: ls dir1/
						Result:  file1 			
					*/
					
					if (strcmp(path, "/") != 0){
						strcat(newPath,internalToken);
						if(strcmp(path+1,newPath)==0){
							internalToken = strtok(NULL, delim1);
							
							if(internalToken){
								internalPath++;
								strcpy(newPath,internalToken);
								
							}
							internalToken = strtok(NULL, delim1);
							
							if(internalToken){
								internalPath++;
								
							}
							
							break;
						}else{
								internalToken = strtok(NULL, delim1);
								strcat(newPath,"/");
						}
					}
					/*
					The following else block will excute for the root directory of mount
					directory.
					
					For example: ls 
						Result:  dir1 file2 file7 			
					*/
					else if(strcmp(path, "/") == 0) {
						internalPath++;
						strcpy(newPath,dupToken);
						internalToken = strtok(NULL, delim1);
					}
				}
							
				if(internalPath == 1){
					filler(buf, newPath, NULL, 0);
				}else if(internalPath > 1 && strstr(directoryListing, newPath) == NULL){
						
						strcat(directoryListing,newPath);
						strcat(directoryListing,";");
						struct stat *stbuf1 = malloc(sizeof(struct stat));
						stbuf1->st_mode = S_IFDIR | 0755;
						stbuf1->st_nlink = 2;
						filler(buf, newPath, stbuf1, 0);
					
					
					
					
				}
				
				token =  strtok_r(NULL, ";",&end_str);
				
			}
			
				
			fseek(archivefp, *(int *)filelength, SEEK_CUR);
			totalBytePassed = ftell(archivefp);
			if (totalBytePassed == sizeOfArchive) {
				break;
			}	
		}
	}
	fclose(archivefp);
	//////////////////////////////////////////
	return 0;
}

static int hello_open(const char *path, struct fuse_file_info *fi)
{
	return 0;
}

static int hello_read(const char *path, char *buf, size_t size, off_t offset,
		      struct fuse_file_info *fi)
{
		
	/////////////////////////////////////////////////////////////
		FILE *archivefp = fopen(archivefile, "r");
		long int fileLength=0;
		if (archivefp == NULL) {
			printf("Error opening a archive file\n");
		}
		else {
			char filename[MAX_FILENAME_SIZE] = "";
			char filelength[4];
			long int sizeOfArchive = 0;
			long int totalBytePassed = 0;
			int filefound = 0;

			fseek(archivefp, 0, SEEK_END);
			sizeOfArchive = ftell(archivefp);
			fseek(archivefp, 0, SEEK_SET);

			
			while (totalBytePassed < sizeOfArchive) {

				fread(filename, 1, MAX_FILENAME_SIZE, archivefp);
				fseek(archivefp, MD5_DIGEST_LENGTH, SEEK_CUR);
				fread(filelength, 1, 4, archivefp);
				int internalPath=0;
				char *end_str;
				char *token = strtok_r(filename, ";",&end_str);
				while (token != NULL)
				{
					
					if (strcmp(path+1, token) == 0) {
						internalPath++;
						fileLength = *(int *)filelength;
						char buffer[fileLength+1];
						fread(buffer, fileLength, 1, archivefp);
						memcpy(buf, buffer, fileLength);
						filefound=1;
						break;
					}
					
					token =  strtok_r(NULL, ";",&end_str);
				}

				if (filefound == 1) {
					break;
				}
				else {
					fseek(archivefp, *(int *)filelength, SEEK_CUR);
					totalBytePassed = ftell(archivefp);
					if (totalBytePassed == sizeOfArchive) {
							break;
					}
				}
			}
		}
			
	return fileLength;
}


int copy_file(char *old_filename, char  *new_filename)
{
	FILE  *ptr_old, *ptr_new;
	int  a;

	ptr_old = fopen(old_filename, "r");
	ptr_new = fopen(new_filename, "w");

	if (ptr_old == NULL)
		return  -1;

	while (1)
	{
		a = fgetc(ptr_old);

		if (!feof(ptr_old))
			fputc(a, ptr_new);
		else
			break;
	}

	fclose(ptr_new);
	fclose(ptr_old);
	return  0;
}


static int hello_unlink(const char* path)
{
	
		copy_file(archivefile, DUP_FILE_NAME);
		FILE *duparchivefp = fopen(DUP_FILE_NAME, "r+");
		if (duparchivefp == NULL) {
			printf("Error opening a filename dupArchive.sludge\n");
		}
		else {
			FILE *archivefp = fopen(archivefile, "w+");
			char filename[MAX_FILENAME_SIZE] = "";
			unsigned char fileHash[MD5_DIGEST_LENGTH];
			char filelength[4];
			long int sizeOfArchive = 0, totalBytePassed = 0;

			fseek(duparchivefp, 0, SEEK_END);
			sizeOfArchive = ftell(duparchivefp);
			fseek(duparchivefp, 0, SEEK_SET);


			if (sizeOfArchive > 0) {
				while (totalBytePassed < sizeOfArchive) {

					fread(filename, 1, MAX_FILENAME_SIZE, duparchivefp);
					char newFileName[MAX_FILENAME_SIZE] = "";
					const char delim[2] = ";";
					char *token = strtok(filename, delim);
					int file_count = 0;
					int fileFound = 0;
					while (token != NULL)
					{

						if (strcmp(token, path+1) == 0) {
							fileFound = 1;
						}
						else {
							file_count++;
							strcat(newFileName, token);
							strcat(newFileName, ";");
						}
						token = strtok(NULL, delim);

					}
					if ((fileFound == 1 && file_count > 0) || (fileFound == 0)) {

						fwrite(newFileName, MAX_FILENAME_SIZE, 1, archivefp);
						fread(fileHash, 1, MD5_DIGEST_LENGTH, duparchivefp);
						fwrite(fileHash, MD5_DIGEST_LENGTH, 1, archivefp);
						fread(filelength, 1, 4, duparchivefp);
						fwrite(filelength, 4, 1, archivefp);
						char buffer[*(int *)filelength];
						int read = fread(buffer, 1, *(int *)filelength, duparchivefp);
						fwrite(buffer, read, 1, archivefp);



					}
					else if (fileFound == 1 && file_count == 0) {
						fseek(duparchivefp, MD5_DIGEST_LENGTH, SEEK_CUR);
						fread(filelength, 1, 4, duparchivefp);
						fseek(duparchivefp, *(int *)filelength, SEEK_CUR);
					}

					totalBytePassed = ftell(duparchivefp);

					if (totalBytePassed == sizeOfArchive) {
						break;
					}

				}

			}

			///////////////////////////////
			fclose(archivefp);



			fclose(duparchivefp);

		}
		remove(DUP_FILE_NAME);


return 0;
}


static int hello_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{

		char buffer[1024];
		int read;
		struct header h1;
		computeHash(path+1, &h1);


		////////////////////////////////
		////Check Duplicate File////////
		////////////////////////////////

		int duplicateFound = checkDuplicateFiles(&h1,path+1);
		///////////////////////////////
		if (duplicateFound == 0) {
			strcpy(h1.name, path+1);
			strcat(h1.name, ";");


			FILE *archivefp = fopen(archivefile, "a");

			FILE *fp = fopen(path+1, "r");
			if (fp == NULL) {
				printf("Error opening a file name %s\n", path+1);
			}
			else {
				fseek(fp, 0, SEEK_END);
				h1.length = ftell(fp);

				fseek(fp, 0, SEEK_SET);

				fwrite(&h1, sizeof(struct header), 1, archivefp);

				while ((read = fread(buffer, 1, 1024, fp)) > 0) {
					fwrite(buffer, read, 1, archivefp);
				}

				fclose(fp);

			}
			fclose(archivefp);
		}


	return 0;
}


static int hello_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
		
	return 0;
}

static int hello_flush(const char* path, struct fuse_file_info* fi){

	return 0;

}

static int hello_release(const char* path, struct fuse_file_info *fi){
		
	return 0;
}

static int hello_getxattr(const char* path, const char* name,char* value, size_t size){
	
	return 0;
}


static struct fuse_operations hello_oper = {
	.getattr	= hello_getattr,
	.readdir	= hello_readdir,
	.open		= hello_open,
	.read		= hello_read,
	.create 	= hello_create,
	.flush		= hello_flush,
	.write 		= hello_write,
	.getxattr   = hello_getxattr,
	.release	= hello_release,
	.unlink		= hello_unlink,
};
int main(int argc, char *argv[])
{

	if (argc == 4) {
		if (strcmp(argv[1], "-f") != 0) {
			printf("\nPlease Enter the argument in correct way as shown below.\nUsage:\n\t./fuse -f [mount point folder] [archivefile name] \n\n");
			return 0;
		}
		archivefile = argv[3];
		argv[3] = NULL;
		argc = 3;
		return fuse_main(argc, argv, &hello_oper, NULL);
	}
	else {
		printf("\nPlease Enter the argument in correct way as shown below.\nUsage:\n\t./fuse -f [mount point folder] [archivefile name] \n\n");
		return 0;
	}
		
}		

