/* original parser id follows */
/* yysccsid[] = "@(#)yaccpar	1.9 (Berkeley) 02/21/93" */
/* (use YYMAJOR/YYMINOR for ifdefs dependent on parser version) */

#define YYBYACC 1
#define YYMAJOR 1
#define YYMINOR 9
#define YYPATCH 20141128

#define YYEMPTY        (-1)
#define yyclearin      (yychar = YYEMPTY)
#define yyerrok        (yyerrflag = 0)
#define YYRECOVERING() (yyerrflag != 0)
#define YYENOMEM       (-2)
#define YYEOF          0
#define YYPREFIX "yy"

#define YYPURE 0

#line 2 "ish.y"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <pwd.h>
#include <errno.h>
#include <ctype.h>

#define MAX_COMMAND 256
#define MAX_ARG 10
#define MAX_FILE_PATH 256

#define READ_END 0
#define WRITE_END 1



struct command{
	char *args[MAX_ARG];
	int numberOfargument;
	int _background;
	int _pipe;
	int _semicolon;
	pid_t parent_id;
	char *filepath;
};

struct backGroundProcess{
	char *processName;
	int _pid;
};

struct job{
	char *processName;
	int numberOfargument;
	char *args[MAX_ARG];
	char *filepath;
	char *jobStatus;
	int _pid;
};

int numberOfCommands=0;
int numberOfEnvironmentVariable=0;
int numberOfStoppedProcess=0;
int numberOfBackgroundProcess=0;
char *inputfile;
char *outputfile;
char *appendfile;
int appendError=0;
int outputError=0;
struct command commands[MAX_COMMAND];
struct backGroundProcess backGroundProcesses[MAX_COMMAND];
struct job jobList[MAX_COMMAND];
const char *builtInCommands[8]={"bg","cd","exit","fg","jobs","kill","setenv","unsetenv"};
pid_t pid;
char *env[MAX_COMMAND];
#line 65 "ish.y"
#ifdef YYSTYPE
#undef  YYSTYPE_IS_DECLARED
#define YYSTYPE_IS_DECLARED 1
#endif
#ifndef YYSTYPE_IS_DECLARED
#define YYSTYPE_IS_DECLARED 1
typedef union
{
    char	*string;
    int		integer;
} YYSTYPE;
#endif /* !YYSTYPE_IS_DECLARED */
#line 96 "y.tab.c"

/* compatibility with bison */
#ifdef YYPARSE_PARAM
/* compatibility with FreeBSD */
# ifdef YYPARSE_PARAM_TYPE
#  define YYPARSE_DECL() yyparse(YYPARSE_PARAM_TYPE YYPARSE_PARAM)
# else
#  define YYPARSE_DECL() yyparse(void *YYPARSE_PARAM)
# endif
#else
# define YYPARSE_DECL() yyparse(void)
#endif

/* Parameters sent to lex. */
#ifdef YYLEX_PARAM
# define YYLEX_DECL() yylex(void *YYLEX_PARAM)
# define YYLEX yylex(YYLEX_PARAM)
#else
# define YYLEX_DECL() yylex(void)
# define YYLEX yylex()
#endif

/* Parameters sent to yyerror. */
#ifndef YYERROR_DECL
#define YYERROR_DECL() yyerror(const char *s)
#endif
#ifndef YYERROR_CALL
#define YYERROR_CALL(msg) yyerror(msg)
#endif

extern int YYPARSE_DECL();

#define WORD 257
#define COMMAND 258
#define FILENAME 259
#define BACKGROUND 260
#define PIPE 261
#define PIPE_ERROR 262
#define SEMICOLON 263
#define REDIRECT_IN 264
#define REDIRECT_OUT 265
#define REDIRECT_ERROR 266
#define APPEND 267
#define APPEND_ERROR 268
#define OPTION 269
#define STRING 270
#define LOGICAL_AND 271
#define LOGICAL_OR 272
#define YYERRCODE 256
typedef short YYINT;
static const YYINT yylhs[] = {                           -1,
    0,    0,    0,    0,    0,    0,    1,    1,    1,    1,
    2,    2,    2,    2,    2,    2,    2,    2,    2,
};
static const YYINT yylen[] = {                            2,
    4,    2,    2,    2,    0,    1,    1,    1,    1,    1,
    2,    2,    2,    3,    3,    3,    3,    3,    0,
};
static const YYINT yydefred[] = {                         0,
    6,   19,    0,    0,    0,    8,    9,    0,    0,   13,
    0,    0,    0,    0,    0,   11,   12,   19,   14,   15,
   16,   17,   18,    0,
};
static const YYINT yydgoto[] = {                          3,
    9,    4,
};
static const YYINT yysindex[] = {                      -250,
    0,    0, -258, -244,    0,    0,    0,    0, -249,    0,
 -259, -247, -245, -243, -242,    0,    0,    0,    0,    0,
    0,    0,    0, -244,
};
static const YYINT yyrindex[] = {                        11,
    0,    0,    0,   15,    1,    0,    0,    7,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,   19,
};
static const YYINT yygindex[] = {                         0,
    0,   -8,
};
#define YYTABLESIZE 282
static const YYINT yytable[] = {                         19,
    3,    5,    6,    7,    8,    1,    4,    2,   18,   24,
    5,   20,   10,   21,    2,   22,   23,    0,    1,   11,
   12,   13,   14,   15,   16,   17,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    7,    0,
    3,    3,    3,    3,   10,    0,    4,    4,    4,    4,
    5,    5,    5,    5,    2,    2,    2,    2,    1,    1,
    1,    1,
};
static const YYINT yycheck[] = {                        259,
    0,  260,  261,  262,  263,  256,    0,  258,  258,   18,
    0,  259,  257,  259,    0,  259,  259,   -1,    0,  264,
  265,  266,  267,  268,  269,  270,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,  258,   -1,
  260,  261,  262,  263,  258,   -1,  260,  261,  262,  263,
  260,  261,  262,  263,  260,  261,  262,  263,  260,  261,
  262,  263,
};
#define YYFINAL 3
#ifndef YYDEBUG
#define YYDEBUG 0
#endif
#define YYMAXTOKEN 272
#define YYUNDFTOKEN 277
#define YYTRANSLATE(a) ((a) > YYMAXTOKEN ? YYUNDFTOKEN : (a))
#if YYDEBUG
static const char *const yyname[] = {

"end-of-file",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"WORD","COMMAND","FILENAME",
"BACKGROUND","PIPE","PIPE_ERROR","SEMICOLON","REDIRECT_IN","REDIRECT_OUT",
"REDIRECT_ERROR","APPEND","APPEND_ERROR","OPTION","STRING","LOGICAL_AND",
"LOGICAL_OR",0,0,0,0,"illegal-symbol",
};
static const char *const yyrule[] = {
"$accept : cmd_line",
"cmd_line : cmd_line separator COMMAND parameters",
"cmd_line : COMMAND parameters",
"cmd_line : cmd_line BACKGROUND",
"cmd_line : cmd_line SEMICOLON",
"cmd_line :",
"cmd_line : error",
"separator : BACKGROUND",
"separator : PIPE",
"separator : PIPE_ERROR",
"separator : SEMICOLON",
"parameters : parameters OPTION",
"parameters : parameters STRING",
"parameters : parameters WORD",
"parameters : parameters REDIRECT_IN FILENAME",
"parameters : parameters REDIRECT_OUT FILENAME",
"parameters : parameters REDIRECT_ERROR FILENAME",
"parameters : parameters APPEND FILENAME",
"parameters : parameters APPEND_ERROR FILENAME",
"parameters :",

};
#endif

int      yydebug;
int      yynerrs;

int      yyerrflag;
int      yychar;
YYSTYPE  yyval;
YYSTYPE  yylval;

/* define the initial stack-sizes */
#ifdef YYSTACKSIZE
#undef YYMAXDEPTH
#define YYMAXDEPTH  YYSTACKSIZE
#else
#ifdef YYMAXDEPTH
#define YYSTACKSIZE YYMAXDEPTH
#else
#define YYSTACKSIZE 10000
#define YYMAXDEPTH  10000
#endif
#endif

#define YYINITSTACKSIZE 200

typedef struct {
    unsigned stacksize;
    YYINT    *s_base;
    YYINT    *s_mark;
    YYINT    *s_last;
    YYSTYPE  *l_base;
    YYSTYPE  *l_mark;
} YYSTACKDATA;
/* variables for the parser stack */
static YYSTACKDATA yystack;
#line 186 "ish.y"
extern int yylex();
extern int yyparse();
extern int yyerror(char *s);
extern yy_scan_string(const char * str);
int findCorrectExecutablePath(char *cmd, char *filepath);
char * removeFirstcharactor (char *charBuffer);
int checkBuiltInCommand(char *cmd);
void executeBuiltInCommand(struct command cmd);
void fillupBackgroundProcess(struct command cmd);
void setEnvironment(struct command cmd);
char * getEnv(char * VAR);
static void setHomeDirectory();
static void setCurrentDirectory();
static void readISHRCfile();

int yyerror(char *s)
{
	fprintf(stderr, "syntax error\n");
	return 0;
}


/* Signal Handler for SIGINT */
void sigintHandler(int sig_num)
{
	if (!pid || pid < 0)
		printf("\nbrijesh@ish: %s $: ",(char *)getEnv("PWD"));
	pid = -1;
	fflush(stdout);
}

/* Signal Handler for SIGINT */
void sigstopHandler(int sig_num) {
	if (pid > 0) {
		for (int i = 0; i <= numberOfCommands; i++) {
			
			if(commands[i].parent_id==pid && commands[i]._background !=1){
					signal(SIGTSTP, sigstopHandler);
					pid_t stdin_PGID;

					stdin_PGID = tcgetpgrp(STDIN_FILENO);
					tcsetpgrp(STDIN_FILENO, stdin_PGID);
					jobList[numberOfStoppedProcess].jobStatus=malloc(20);
					jobList[numberOfStoppedProcess].filepath=malloc(MAX_FILE_PATH);
					char *completeCommand=malloc(MAX_FILE_PATH);
					jobList[numberOfStoppedProcess].processName=malloc(MAX_FILE_PATH);
							for (int j = 0; j < commands[i].numberOfargument-1; j++) {
								jobList[numberOfStoppedProcess].args[j] = malloc(20);
								strcpy(jobList[numberOfStoppedProcess].args[j], (char *)commands[i].args[j]);
								strcat((char *)completeCommand, (char *)commands[i].args[j]);
								strcat((char *)completeCommand, " ");
							}
					strcat(jobList[numberOfStoppedProcess].processName, completeCommand);
					strcpy(jobList[numberOfStoppedProcess].jobStatus, "Stopped");
					strcpy(jobList[numberOfStoppedProcess].filepath, commands[i].filepath);
					jobList[numberOfStoppedProcess].numberOfargument = commands[i].numberOfargument;
					jobList[numberOfStoppedProcess]._pid=pid;
					int currentProcess=numberOfStoppedProcess++;
                                        printf("\n[%d]+	%s			%s\n",numberOfStoppedProcess,(char *)jobList[currentProcess].jobStatus,(char *)completeCommand);
				
			}
			break;
		}
	}
	else {
		printf("\nbrijesh@ish: %s $: ",(char *)getEnv("PWD"));
	}
	fflush(stdout);
}


int main()
{

	
	setHomeDirectory();
	setCurrentDirectory();
	readISHRCfile();
	char *commandline = NULL;
	size_t size;
	int nextCommand;
	while(1){
		nextCommand=0;
		signal(SIGINT, sigintHandler);		
		signal(SIGTSTP, sigstopHandler);

		/* Print the command prompt */
		printf("brijesh@ish: %s $: ",(char *)getEnv("PWD"));


		/* Read a command line */
		if (getline(&commandline, &size, stdin) != -1) {
			if (commandline != NULL && strlen((char *)commandline) > 1) {
				yy_scan_string(commandline);
				yyparse();

				
				
				int tempin = dup(STDIN_FILENO);
				int tempout = dup(STDOUT_FILENO);
				int temperror = dup(STDERR_FILENO);
				int fdout;
				int fdin;
				int fderror;
				
				

				if (inputfile) {
					fdin = open(inputfile, O_RDONLY, 0);
					if (-1 == fdin)
					{
						printf("\n [%s]\n", strerror(errno));
						return 1;
					}

				}
				else {
					fdin = dup(tempin);
				}

				
				for (int i = 0; i <= numberOfCommands; i++) {
					
					if (checkBuiltInCommand(commands[i].args[0]) > 0) {
						executeBuiltInCommand(commands[i]);

					}
					else {
						char *filepath = malloc(MAX_FILE_PATH);
						int success = 0;

						if ((commands[i].args[0])[0] == '/') {
							filepath = commands[i].args[0];
							success = 1;
						}
						else {
							success = findCorrectExecutablePath(commands[i].args[0], filepath);
							
						}

						commands[i].filepath = malloc(MAX_FILE_PATH);
						strcpy((char *)commands[i].filepath,filepath);


						if (success > 0) {
							
				

							dup2(fdin, STDIN_FILENO);
							close(fdin);
							if (commands[i]._pipe == 1 || ((i - 1) >= 0 && commands[i - 1]._pipe == 1) || inputfile || outputfile || appendfile) {

								if (commands[i]._semicolon == 1 || i == numberOfCommands) {
									if (outputfile) {
										fdout = open(outputfile, O_WRONLY | O_CREAT | O_TRUNC, 0640);
										if (-1 == fdout)

										{
											printf("\n [%s]\n", strerror(errno));
											return 1;
										}
										if(outputError == 1){
											fderror=dup(fdout);
										}
										outputfile = NULL;
										
									}
									else if (appendfile) {
										fdout = open(appendfile, O_RDWR | O_CREAT | O_APPEND, 0640);
										if (-1 == fdout)
										{
											printf("\n [%s]\n", strerror(errno));
											return 1;
										}
										if(appendError == 1){
											fderror=dup(fdout);
										}
										appendfile = NULL;
									}
									else {
										fdout = dup(tempout);
									}
								}
								else {
									int fd[2];
									pipe(fd);

									fdin = fd[0];
									if (commands[i]._pipe == 1) {
										fdout = fd[1];
									}
									else {
										fdout = dup(tempout);
									}


								}
							}
							else {
								
								dup2(tempin, STDIN_FILENO);
								dup2(tempout, STDOUT_FILENO);
								dup2(temperror, STDERR_FILENO);

								close(tempin);
								close(tempout);
								close(temperror);
							}
							inputfile = NULL;
							dup2(fdout, STDOUT_FILENO);
							if(appendError ==1 || outputError ==1){
								dup2(fderror, STDERR_FILENO);
								close(fderror);
								appendError = 0;
								outputError = 0;
								
							}
							
							close(fdout);

							pid = fork();
							
							if (pid == 0) {
								execve(filepath, commands[i].args, env);
								_exit(0);
							}
							else {
								commands[i].parent_id = pid;
								if (commands[i]._background != 1) {
									int foreGroundstatus=0;
									do {
										
										
										printf("\n|-----STATUS-----|\n");
										printf("COMMAND: %s\n", (char *)commands[i].args[0]);
										printf("continued:	%d\n"
										"Exit:		%d	exited status:	%i\n"
										"stopped:	%d	stopped signal:	%i\n"
										"signaled:	%d	exited signal:	%i\n",
										WIFCONTINUED(foreGroundstatus), WIFEXITED(foreGroundstatus), WEXITSTATUS(foreGroundstatus), WIFSTOPPED(foreGroundstatus), WSTOPSIG(foreGroundstatus), WIFSIGNALED(foreGroundstatus), WTERMSIG(foreGroundstatus));								
										printf("|-----STATUS-----|\n\n");
										waitpid(pid, &foreGroundstatus, WUNTRACED | WCONTINUED);
									
										
									} while (!WIFEXITED(foreGroundstatus)  && !WIFSTOPPED(foreGroundstatus));
									
								}
								else {
									int backGroundstatus=0;
									do {
										
										printf("\n|-----STATUS--back---|\n");
										printf("COMMAND: %s\n", (char *)commands[i].args[0]);
										printf("continued:	%d\n"
										"Exit:		%d	exited status:	%i\n"
										"stopped:	%d	stopped signal:	%i\n"
										"signaled:	%d	exited signal:	%i\n",
										WIFCONTINUED(backGroundstatus), WIFEXITED(backGroundstatus), WEXITSTATUS(backGroundstatus), WIFSTOPPED(backGroundstatus), WSTOPSIG(backGroundstatus), WIFSIGNALED(backGroundstatus), WTERMSIG(backGroundstatus));
										printf("|-----STATUS-back----|\n\n");
										waitpid(pid, &backGroundstatus, WNOHANG);
										
										
									} while (!WIFEXITED(backGroundstatus) && !WIFSTOPPED(backGroundstatus));
									jobList[numberOfStoppedProcess]._pid = commands[i].parent_id;
									jobList[numberOfStoppedProcess].jobStatus=malloc(20);
									strcpy(jobList[numberOfStoppedProcess].jobStatus, "Done");
									backGroundProcesses[numberOfBackgroundProcess]._pid = commands[i].parent_id;
									numberOfStoppedProcess++;
									numberOfBackgroundProcess++;
									printf("\n[%d]		%d\n",numberOfBackgroundProcess,backGroundProcesses[numberOfBackgroundProcess-1]._pid);
									fillupBackgroundProcess(commands[i]);
									
								}
								

								pid = -1;
							}

						}
						else {
							printf("%s: command not found\n", commands[i].args[0]);
						}
						
						
					}
				}


				dup2(tempin, STDIN_FILENO);
				dup2(tempout, STDOUT_FILENO);
				dup2(temperror, STDERR_FILENO);

				close(tempin);
				close(tempout);
				close(temperror);
				
				numberOfCommands = 0;
				memset(commands, 0, sizeof(commands));

			}
		}
		nextCommand=1;
	}
	
	return 0;
}

void fillupBackgroundProcess(struct command cmd) {
	for (int l = 0; l < numberOfStoppedProcess; l++) {
			if (cmd.parent_id == jobList[l]._pid) {
				char *completeBackGroundCommand = malloc(MAX_FILE_PATH);
				jobList[l].processName = malloc(MAX_FILE_PATH);
				jobList[l].filepath = malloc(MAX_FILE_PATH);
				jobList[l].args[0] = malloc(MAX_FILE_PATH);
				strcpy(jobList[l].args[0], cmd.args[0]);
				strcat((char *)completeBackGroundCommand, cmd.args[0]);
				strcat((char *)completeBackGroundCommand, " ");
					
				for (int j = 1; j < cmd.numberOfargument-1; j++) {
					jobList[l].args[j] = malloc(MAX_FILE_PATH);
					strcpy(jobList[l].args[j], cmd.args[j]);
					strcat((char *)completeBackGroundCommand, cmd.args[j]);
					strcat((char *)completeBackGroundCommand, " ");
				}
				strcat((char *)completeBackGroundCommand, "&");
				jobList[l].numberOfargument = cmd.numberOfargument;
				
				strcat(jobList[l].processName, (char *)completeBackGroundCommand);
				strcpy(jobList[l].filepath, cmd.filepath);
				
			}
	}

}


int findCorrectExecutablePath(char *cmd, char *filepath) {

	char *envPATHValue = getEnv("PATH");
	const char delim[2] = ":";
	char *token;
	int success = -1;

	/* get the first token */
	token = strtok(envPATHValue, delim);


	/* walk through other tokens */
	while (token != NULL)
	{
		strcpy(filepath, token);
		strcat(filepath, "/");
		strcat(filepath, cmd);

		
		if (access(filepath, X_OK) == 0) {
			success = 1;
			break;
		}
		else {
			token = strtok(NULL, delim);
		}
	}

	return success;

}

int checkBuiltInCommand(char *cmd) {
	for (int i = 0; i < 8; i++) {
		if (strcmp(builtInCommands[i], cmd) == 0) {
			return 1;

		}
	}
	return 0;
}

void executeBuiltInCommand(struct command cmd) {
	if (strcmp(cmd.args[0], "exit") == 0) {
		exit(0);
	}
	else if (strcmp(cmd.args[0], "jobs") == 0) {
		
		for (int i = 0; i < numberOfStoppedProcess; i++) {
			if(jobList[i].jobStatus != "Dead" && jobList[i].jobStatus !=  "Terminated"){
				printf("[%d]	%s			%s\n", (i+1), (char *)jobList[i].jobStatus, (char *)jobList[i].processName);					
			}
			
		}
	}
	else if (strcmp(cmd.args[0], "fg") == 0) {
		char *newString = removeFirstcharactor((char *)cmd.args[1]);
		int jobindex = atoi(newString)-1;
		if (jobindex > -1 && jobindex < numberOfStoppedProcess && jobList[jobindex].jobStatus != "Dead") {
			jobList[jobindex].jobStatus="Dead";
			
			commands[0].filepath = jobList[jobindex].filepath;
			commands[0].numberOfargument = jobList[jobindex].numberOfargument;
			for (int i = 0; i < jobList[jobindex].numberOfargument - 1; i++) {
				commands[0].args[i] = jobList[jobindex].args[i];
			}
			commands[0]._background = 0;
			commands[0].args[jobList[jobindex].numberOfargument] = NULL;
			pid = fork();
			
			if (pid == 0) {
				execve(commands[0].filepath, commands[0].args, env);
				_exit(0);
			}
			else {
				commands[0].parent_id = pid;
				int foreGroundstatus=0;
				do {


					printf("\n|-----STATUS-----|\n");
					printf("COMMAND: %s\n", (char *)commands[0].args[0]);
					printf("continued:	%d\n"
						"Exit:		%d	exited status:	%i\n"
						"stopped:	%d	stopped signal:	%i\n"
						"signaled:	%d	exited signal:	%i\n",
						WIFCONTINUED(foreGroundstatus), WIFEXITED(foreGroundstatus), WEXITSTATUS(foreGroundstatus), WIFSTOPPED(foreGroundstatus), WSTOPSIG(foreGroundstatus), WIFSIGNALED(foreGroundstatus), WTERMSIG(foreGroundstatus));
					printf("|-----STATUS-----|\n\n");
					waitpid(pid, &foreGroundstatus, WUNTRACED | WCONTINUED);


				} while (!WIFEXITED(foreGroundstatus) && !WIFSTOPPED(foreGroundstatus));
				pid = -1;
			}

		}else{
			printf("ish: fg: %d: no such job\n",(jobindex+1));

		}
	}
	else if (strcmp(cmd.args[0], "bg") == 0) {
		char *newString = removeFirstcharactor((char *)cmd.args[1]);
		int jobindex = atoi(newString) - 1;
		if (jobindex > -1 && jobindex < numberOfStoppedProcess &&jobList[jobindex].jobStatus != "Dead") {
			jobList[jobindex].jobStatus="Dead";
			commands[0]._background = 1;
			commands[0].filepath = jobList[jobindex].filepath;
			commands[0].numberOfargument = jobList[jobindex].numberOfargument;
				
			for (int i = 0; i < jobList[jobindex].numberOfargument - 1; i++) {
				commands[0].args[i] = jobList[jobindex].args[i];
			}
			commands[0].args[jobList[jobindex].numberOfargument] = NULL;
			
			pid = fork();
			
			if (pid == 0) {
				execve(commands[0].filepath, commands[0].args, env);
				_exit(0);
			}
			else {
				commands[0].parent_id = pid;
				pid=-1;
				int backGroundstatus = 0;
				do {

					printf("\n|-----STATUS--back---|\n");
					printf("COMMAND: %s\n", (char *)commands[0].args[0]);
					printf("continued:	%d\n"
						"Exit:		%d	exited status:	%i\n"
						"stopped:	%d	stopped signal:	%i\n"
						"signaled:	%d	exited signal:	%i\n",
						WIFCONTINUED(backGroundstatus), WIFEXITED(backGroundstatus), WEXITSTATUS(backGroundstatus), WIFSTOPPED(backGroundstatus), WSTOPSIG(backGroundstatus), WIFSIGNALED(backGroundstatus), WTERMSIG(backGroundstatus));
					printf("|-----STATUS-back----|\n\n");
					waitpid(pid, &backGroundstatus, WNOHANG);


				} while (!WIFEXITED(backGroundstatus) && !WIFSTOPPED(backGroundstatus));
				jobList[numberOfStoppedProcess]._pid = commands[0].parent_id;
				jobList[numberOfStoppedProcess].jobStatus = malloc(20);
				strcpy(jobList[numberOfStoppedProcess].jobStatus, "Done");
				backGroundProcesses[numberOfBackgroundProcess]._pid = commands[0].parent_id;
				numberOfStoppedProcess++;
				numberOfBackgroundProcess++;
				printf("\n[%d]		%d\n", numberOfBackgroundProcess, backGroundProcesses[numberOfBackgroundProcess - 1]._pid);
				fillupBackgroundProcess(commands[0]);
			}

		}else{
			printf("ish: bg: %d: no such job\n",(jobindex+1));

		}
	}else if(strcmp(cmd.args[0], "cd") == 0){
		char *newDirectoryPath=malloc(256);
		if(cmd.args[1] != NULL){
			if ((cmd.args[1])[0] == '/') {
				strcpy(newDirectoryPath,cmd.args[1]);
			}
			else {
				strcpy(newDirectoryPath,(char *)getEnv("PWD"));
				strcat(newDirectoryPath,"/");
				strcat(newDirectoryPath,cmd.args[1]);
			}
		}else{
			strcpy(newDirectoryPath,(char *)getEnv("HOME"));
		}
		
		int ret = chdir (newDirectoryPath);
		if(ret == 0){
			cmd.args[0]="setenv";
			cmd.args[1]="PWD";
			cmd.args[2]= newDirectoryPath;
			setEnvironment(cmd);
		}else{
			printf("ish: cd: %s: No such file or directory\n",(char *)cmd.args[1]);
		}
		
	}else if(strcmp(cmd.args[0], "kill") == 0){
		
		char *newString = removeFirstcharactor((char *)cmd.args[1]);
		int jobindex = atoi(newString)-1;
		if (jobindex > -1 && jobindex < numberOfStoppedProcess && (jobList[jobindex].jobStatus != "Dead" || jobList[jobindex].jobStatus != "Terminated")) {
			int success=kill(jobList[jobindex]._pid,SIGTERM);
			if(success == 0){
				jobList[jobindex].jobStatus = "Terminated";
				printf("[%d]	%s			%s\n", (jobindex+1), (char *)jobList[jobindex].jobStatus, (char *)jobList[jobindex].processName);	
			}		
			
		}else{
			printf("ish: kill: %d: no such job\n",(jobindex+1));

		}
		
	}else if(strcmp(cmd.args[0], "setenv") == 0){
			setEnvironment(cmd);
			
	}else if(strcmp(cmd.args[0], "unsetenv") == 0){
		int environmentVaribleExist=-1;
		for(int i=0;i<numberOfEnvironmentVariable;i++){
			const char delim[2] = "=";
			char *token;
			/* get the first token */
			char *tempEnv=malloc(strlen(env[i])+1);
			strcpy(tempEnv,env[i]);
			token = strtok(tempEnv, delim);
			if(strcmp(cmd.args[1], token) == 0){
				environmentVaribleExist=i;
				break;
			}
		}
		if(environmentVaribleExist > -1){
			for(int i=environmentVaribleExist;i<numberOfEnvironmentVariable-1;i++){
				env[i]=env[i+1];
			}
			numberOfEnvironmentVariable--;

		}
	}
	
}

char * removeFirstcharactor (char *charBuffer) {
  int length = strlen(charBuffer);
  char *str;
  if (length <= 1) {
    str = (char *) malloc(1);
    str[0] = '\0';
  } else {
    str = (char *) malloc(length+1);
    strcpy(str, &charBuffer[1]);
  }
  return str;
}


void setEnvironment(struct command cmd) {
	if (cmd.args[0] != NULL && cmd.args[1] != NULL && cmd.args[2] != NULL) {
		int environmentVaribleExist = -1;
		for (int i = 0; i<numberOfEnvironmentVariable; i++) {
			const char delim[2] = "=";
			char *token;
			/* get the first token */
			char *tempEnv = malloc(strlen(env[i])+1);
			strcpy(tempEnv, env[i]);
			token = strtok(tempEnv, delim);
			if (strcmp(cmd.args[1], token) == 0) {
				environmentVaribleExist = i;
				break;
			}
		}
		char *pathvalue = malloc(MAX_FILE_PATH);
		if ((cmd.args[1])[0] == '"' && (cmd.args[1])[strlen(cmd.args[1]) - 1] == '"') {
			memmove(cmd.args[1], cmd.args[1] + 1, strlen(cmd.args[1]));
			(cmd.args[1])[strlen(cmd.args[1]) - 1] = 0;
		}
		strcpy(pathvalue, "");
		strcat(pathvalue, cmd.args[1]);
		strcat(pathvalue, "=");
		if ((cmd.args[2])[0] == '"' && (cmd.args[2])[strlen(cmd.args[2]) - 1] == '"') {
			memmove(cmd.args[2], cmd.args[2] + 1, strlen(cmd.args[2]));
			(cmd.args[2])[strlen(cmd.args[2]) - 1] = 0;
		}
		strcat(pathvalue, cmd.args[2]);
		if (environmentVaribleExist > -1) {
			env[environmentVaribleExist] = pathvalue;

		}
		else {
			env[numberOfEnvironmentVariable] = pathvalue;
			numberOfEnvironmentVariable++;
		}

	}
	else if (cmd.args[0] != NULL && cmd.args[1] != NULL && cmd.args[2] == NULL) {
		int environmentVaribleExist = -1;
		for (int i = 0; i<numberOfEnvironmentVariable; i++) {
			const char delim[2] = "=";
			char *token;
			/* get the first token */
			char *tempEnv = malloc(strlen(env[i])+1);
			strcpy(tempEnv, env[i]);
			token = strtok(tempEnv, delim);
			if (strcmp(cmd.args[1], token) == 0) {
				environmentVaribleExist = i;
				break;
			}
		}
		char *pathvalue = malloc(MAX_FILE_PATH);
		strcpy(pathvalue, "");
		strcat(pathvalue, cmd.args[1]);
		strcat(pathvalue, "=");
		strcat(pathvalue, "");
		if (environmentVaribleExist > -1) {
			env[environmentVaribleExist] = pathvalue;

		}
		else {

			env[numberOfEnvironmentVariable] = pathvalue;
			numberOfEnvironmentVariable++;
		}
	}
	else if (cmd.args[0] != NULL && cmd.args[1] == NULL && cmd.args[2] == NULL) {
		for (int i = 0; i<numberOfEnvironmentVariable; i++) {
			printf("%s\n", (char *)env[i]);
			
		}
	}

}
char *RemoveSpaces(char* str)
{
  char *end;

  // Trim leading space
  while(isspace((unsigned char)*str)) str++;

  if(*str == 0)  // All spaces?
    return str;

  // Trim trailing space
  end = str + strlen(str) - 1;
  while(end > str && isspace((unsigned char)*end)) end--;

  // Write new null terminator
  *(end+1) = 0;

  return str;
}


char * getEnv(char * VAR) {
		for (int i = 0; i<numberOfEnvironmentVariable; i++) {
			const char delim[2] = "=";
			/* get the first token */
			char *tempEnv = malloc(strlen(env[i])+1);
			strcpy(tempEnv, env[i]);
			char *token = strtok(tempEnv, delim);
			char *varValue = strtok(NULL, "");
			if (strcmp(VAR, token) == 0) {
				return varValue;
			}
		}
}


static void setHomeDirectory(){
	struct passwd *pw = getpwuid(getuid());

	char *homedir = pw->pw_dir;
	struct command cmd;
	cmd.args[0]="setenv";
	cmd.args[1]="HOME";
	cmd.args[2]= homedir;
	setEnvironment(cmd);
}

static void setCurrentDirectory(){
	
	char currentWorkingDirectory[1024];
	getcwd(currentWorkingDirectory, sizeof(currentWorkingDirectory));
	struct command cmd;
	cmd.args[0]="setenv";
	cmd.args[1]="PWD";
	cmd.args[2]= currentWorkingDirectory;
	setEnvironment(cmd);
}


static void readISHRCfile(){
	
	if( access( ".ishrc", R_OK ) != -1 ) {
		FILE* fp;
		char line[1024];
	    	fp = fopen(".ishrc", "r");

		if(fp != NULL){
			while(fgets(line, 1024, (FILE*) fp)) {
			   	 /* get the first token */
			   	const char delim[2] = " ";
				char *token = strtok(line, delim);
				if(strcmp(token,"setenv")==0){
					/* walk through other tokens */
					int numberOfArgument=0;
					struct command cmd;
					cmd.args[numberOfArgument++]=token;
					while (token != NULL)
					{
						token = strtok(NULL, delim);
						if(token != NULL){
							cmd.args[numberOfArgument++]=RemoveSpaces(token);
						}
						
						
					
					}
					setEnvironment(cmd);
					
				}
			}
		}
		fclose(fp);
	
	}else{
		printf(".ishrc File not found for initial setup\n");
	}
	
	
}
#line 1064 "y.tab.c"

#if YYDEBUG
#include <stdio.h>		/* needed for printf */
#endif

#include <stdlib.h>	/* needed for malloc, etc */
#include <string.h>	/* needed for memset */

/* allocate initial stack or double stack size, up to YYMAXDEPTH */
static int yygrowstack(YYSTACKDATA *data)
{
    int i;
    unsigned newsize;
    YYINT *newss;
    YYSTYPE *newvs;

    if ((newsize = data->stacksize) == 0)
        newsize = YYINITSTACKSIZE;
    else if (newsize >= YYMAXDEPTH)
        return YYENOMEM;
    else if ((newsize *= 2) > YYMAXDEPTH)
        newsize = YYMAXDEPTH;

    i = (int) (data->s_mark - data->s_base);
    newss = (YYINT *)realloc(data->s_base, newsize * sizeof(*newss));
    if (newss == 0)
        return YYENOMEM;

    data->s_base = newss;
    data->s_mark = newss + i;

    newvs = (YYSTYPE *)realloc(data->l_base, newsize * sizeof(*newvs));
    if (newvs == 0)
        return YYENOMEM;

    data->l_base = newvs;
    data->l_mark = newvs + i;

    data->stacksize = newsize;
    data->s_last = data->s_base + newsize - 1;
    return 0;
}

#if YYPURE || defined(YY_NO_LEAKS)
static void yyfreestack(YYSTACKDATA *data)
{
    free(data->s_base);
    free(data->l_base);
    memset(data, 0, sizeof(*data));
}
#else
#define yyfreestack(data) /* nothing */
#endif

#define YYABORT  goto yyabort
#define YYREJECT goto yyabort
#define YYACCEPT goto yyaccept
#define YYERROR  goto yyerrlab

int
YYPARSE_DECL()
{
    int yym, yyn, yystate;
#if YYDEBUG
    const char *yys;

    if ((yys = getenv("YYDEBUG")) != 0)
    {
        yyn = *yys;
        if (yyn >= '0' && yyn <= '9')
            yydebug = yyn - '0';
    }
#endif

    yynerrs = 0;
    yyerrflag = 0;
    yychar = YYEMPTY;
    yystate = 0;

#if YYPURE
    memset(&yystack, 0, sizeof(yystack));
#endif

    if (yystack.s_base == NULL && yygrowstack(&yystack) == YYENOMEM) goto yyoverflow;
    yystack.s_mark = yystack.s_base;
    yystack.l_mark = yystack.l_base;
    yystate = 0;
    *yystack.s_mark = 0;

yyloop:
    if ((yyn = yydefred[yystate]) != 0) goto yyreduce;
    if (yychar < 0)
    {
        if ((yychar = YYLEX) < 0) yychar = YYEOF;
#if YYDEBUG
        if (yydebug)
        {
            yys = yyname[YYTRANSLATE(yychar)];
            printf("%sdebug: state %d, reading %d (%s)\n",
                    YYPREFIX, yystate, yychar, yys);
        }
#endif
    }
    if ((yyn = yysindex[yystate]) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
#if YYDEBUG
        if (yydebug)
            printf("%sdebug: state %d, shifting to state %d\n",
                    YYPREFIX, yystate, yytable[yyn]);
#endif
        if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack) == YYENOMEM)
        {
            goto yyoverflow;
        }
        yystate = yytable[yyn];
        *++yystack.s_mark = yytable[yyn];
        *++yystack.l_mark = yylval;
        yychar = YYEMPTY;
        if (yyerrflag > 0)  --yyerrflag;
        goto yyloop;
    }
    if ((yyn = yyrindex[yystate]) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
        yyn = yytable[yyn];
        goto yyreduce;
    }
    if (yyerrflag) goto yyinrecovery;

    YYERROR_CALL("syntax error");

    goto yyerrlab;

yyerrlab:
    ++yynerrs;

yyinrecovery:
    if (yyerrflag < 3)
    {
        yyerrflag = 3;
        for (;;)
        {
            if ((yyn = yysindex[*yystack.s_mark]) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == YYERRCODE)
            {
#if YYDEBUG
                if (yydebug)
                    printf("%sdebug: state %d, error recovery shifting\
 to state %d\n", YYPREFIX, *yystack.s_mark, yytable[yyn]);
#endif
                if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack) == YYENOMEM)
                {
                    goto yyoverflow;
                }
                yystate = yytable[yyn];
                *++yystack.s_mark = yytable[yyn];
                *++yystack.l_mark = yylval;
                goto yyloop;
            }
            else
            {
#if YYDEBUG
                if (yydebug)
                    printf("%sdebug: error recovery discarding state %d\n",
                            YYPREFIX, *yystack.s_mark);
#endif
                if (yystack.s_mark <= yystack.s_base) goto yyabort;
                --yystack.s_mark;
                --yystack.l_mark;
            }
        }
    }
    else
    {
        if (yychar == YYEOF) goto yyabort;
#if YYDEBUG
        if (yydebug)
        {
            yys = yyname[YYTRANSLATE(yychar)];
            printf("%sdebug: state %d, error recovery discards token %d (%s)\n",
                    YYPREFIX, yystate, yychar, yys);
        }
#endif
        yychar = YYEMPTY;
        goto yyloop;
    }

yyreduce:
#if YYDEBUG
    if (yydebug)
        printf("%sdebug: state %d, reducing by rule %d (%s)\n",
                YYPREFIX, yystate, yyn, yyrule[yyn]);
#endif
    yym = yylen[yyn];
    if (yym)
        yyval = yystack.l_mark[1-yym];
    else
        memset(&yyval, 0, sizeof yyval);
    switch (yyn)
    {
case 1:
#line 91 "ish.y"
	{ 
			  	commands[numberOfCommands].args[0] = yystack.l_mark[-1].string;
			  	commands[numberOfCommands].numberOfargument++;
			  	commands[numberOfCommands].args[commands[numberOfCommands].numberOfargument] = 0;
			}
break;
case 2:
#line 97 "ish.y"
	{
                	  	commands[numberOfCommands].args[0] = yystack.l_mark[-1].string;
			  	commands[numberOfCommands].numberOfargument++;
			  	commands[numberOfCommands].args[commands[numberOfCommands].numberOfargument] = 0;
			}
break;
case 3:
#line 103 "ish.y"
	{
				commands[numberOfCommands]._background=1;
                	}
break;
case 4:
#line 108 "ish.y"
	{
				commands[numberOfCommands]._semicolon=1;
                	}
break;
case 7:
#line 116 "ish.y"
	{
				commands[numberOfCommands]._background=1;
                		numberOfCommands++;
			}
break;
case 8:
#line 121 "ish.y"
	{
				commands[numberOfCommands]._pipe=1;
				numberOfCommands++;
			}
break;
case 10:
#line 127 "ish.y"
	{
			
				commands[numberOfCommands]._semicolon=1;
                		numberOfCommands++;
			}
break;
case 11:
#line 136 "ish.y"
	{
				if(commands[numberOfCommands].numberOfargument == 0){
					commands[numberOfCommands].numberOfargument++;
				}
				commands[numberOfCommands].args[commands[numberOfCommands].numberOfargument] = yystack.l_mark[0].string;
				commands[numberOfCommands].numberOfargument++;
			}
break;
case 12:
#line 144 "ish.y"
	{
				if(commands[numberOfCommands].numberOfargument == 0){
					commands[numberOfCommands].numberOfargument++;
				}
				commands[numberOfCommands].args[commands[numberOfCommands].numberOfargument] = yystack.l_mark[0].string;
				commands[numberOfCommands].numberOfargument++;
			}
break;
case 13:
#line 152 "ish.y"
	{
				if(commands[numberOfCommands].numberOfargument == 0){
					commands[numberOfCommands].numberOfargument++;
				}
				commands[numberOfCommands].args[commands[numberOfCommands].numberOfargument] = yystack.l_mark[0].string;
				commands[numberOfCommands].numberOfargument++;
			}
break;
case 14:
#line 160 "ish.y"
	{
				
				inputfile = yystack.l_mark[0].string;
			}
break;
case 15:
#line 165 "ish.y"
	{
				outputfile = yystack.l_mark[0].string;
			}
break;
case 16:
#line 169 "ish.y"
	{
				outputError=1;
				outputfile = yystack.l_mark[0].string;
			}
break;
case 17:
#line 174 "ish.y"
	{
				appendfile = yystack.l_mark[0].string;
			}
break;
case 18:
#line 178 "ish.y"
	{
				appendError=1;
				appendfile = yystack.l_mark[0].string;
			}
break;
#line 1379 "y.tab.c"
    }
    yystack.s_mark -= yym;
    yystate = *yystack.s_mark;
    yystack.l_mark -= yym;
    yym = yylhs[yyn];
    if (yystate == 0 && yym == 0)
    {
#if YYDEBUG
        if (yydebug)
            printf("%sdebug: after reduction, shifting from state 0 to\
 state %d\n", YYPREFIX, YYFINAL);
#endif
        yystate = YYFINAL;
        *++yystack.s_mark = YYFINAL;
        *++yystack.l_mark = yyval;
        if (yychar < 0)
        {
            if ((yychar = YYLEX) < 0) yychar = YYEOF;
#if YYDEBUG
            if (yydebug)
            {
                yys = yyname[YYTRANSLATE(yychar)];
                printf("%sdebug: state %d, reading %d (%s)\n",
                        YYPREFIX, YYFINAL, yychar, yys);
            }
#endif
        }
        if (yychar == YYEOF) goto yyaccept;
        goto yyloop;
    }
    if ((yyn = yygindex[yym]) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn];
    else
        yystate = yydgoto[yym];
#if YYDEBUG
    if (yydebug)
        printf("%sdebug: after reduction, shifting from state %d \
to state %d\n", YYPREFIX, *yystack.s_mark, yystate);
#endif
    if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack) == YYENOMEM)
    {
        goto yyoverflow;
    }
    *++yystack.s_mark = (YYINT) yystate;
    *++yystack.l_mark = yyval;
    goto yyloop;

yyoverflow:
    YYERROR_CALL("yacc stack overflow");

yyabort:
    yyfreestack(&yystack);
    return (1);

yyaccept:
    yyfreestack(&yystack);
    return (0);
}
