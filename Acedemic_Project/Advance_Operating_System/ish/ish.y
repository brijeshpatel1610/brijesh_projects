%{
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <pwd.h>
#include <errno.h>
#include <ctype.h>

#define MAX_COMMAND 256
#define MAX_ARG 10
#define MAX_FILE_PATH 256

#define READ_END 0
#define WRITE_END 1



struct command{
	char *args[MAX_ARG];
	int numberOfargument;
	int _background;
	int _pipe;
	int _semicolon;
	pid_t parent_id;
	char *filepath;
};

struct backGroundProcess{
	char *processName;
	int _pid;
};

struct job{
	char *processName;
	int numberOfargument;
	char *args[MAX_ARG];
	char *filepath;
	char *jobStatus;
	int _pid;
};

int numberOfCommands=0;
int numberOfEnvironmentVariable=0;
int numberOfStoppedProcess=0;
int numberOfBackgroundProcess=0;
char *inputfile;
char *outputfile;
char *appendfile;
int appendError=0;
int outputError=0;
struct command commands[MAX_COMMAND];
struct backGroundProcess backGroundProcesses[MAX_COMMAND];
struct job jobList[MAX_COMMAND];
const char *builtInCommands[8]={"bg","cd","exit","fg","jobs","kill","setenv","unsetenv"};
pid_t pid;
char *env[MAX_COMMAND];
%}

%union
{
    char	*string;
    int		integer;
}

%token 	<string>	WORD
%token 	<string>	COMMAND
%token 	<string>	FILENAME
%token	<int>		BACKGROUND
%token	<int>		PIPE
%token	<int>		PIPE_ERROR
%token	<int>		SEMICOLON
%token	<int>		REDIRECT_IN
%token	<int>		REDIRECT_OUT
%token	<int>		REDIRECT_ERROR
%token	<int>		APPEND
%token	<int>		APPEND_ERROR
%token	<string>	OPTION
%token	<string>	STRING
%token	<int>		LOGICAL_AND
%token	<int>		LOGICAL_OR

%%

cmd_line 	: cmd_line separator COMMAND parameters 
			{ 
			  	commands[numberOfCommands].args[0] = $3;
			  	commands[numberOfCommands].numberOfargument++;
			  	commands[numberOfCommands].args[commands[numberOfCommands].numberOfargument] = 0;
			}
                | COMMAND parameters 
                	{
                	  	commands[numberOfCommands].args[0] = $1;
			  	commands[numberOfCommands].numberOfargument++;
			  	commands[numberOfCommands].args[commands[numberOfCommands].numberOfargument] = 0;
			}
		| cmd_line BACKGROUND 
			{
				commands[numberOfCommands]._background=1;
                	}
			
		| cmd_line SEMICOLON
			{
				commands[numberOfCommands]._semicolon=1;
                	}
		|
		| error 
		;

separator 	: BACKGROUND  
			{
				commands[numberOfCommands]._background=1;
                		numberOfCommands++;
			}
		| PIPE	
			{
				commands[numberOfCommands]._pipe=1;
				numberOfCommands++;
			}
		| PIPE_ERROR
		| SEMICOLON 
			{
			
				commands[numberOfCommands]._semicolon=1;
                		numberOfCommands++;
			}
			
		;

parameters	: parameters OPTION 
			{
				if(commands[numberOfCommands].numberOfargument == 0){
					commands[numberOfCommands].numberOfargument++;
				}
				commands[numberOfCommands].args[commands[numberOfCommands].numberOfargument] = $2;
				commands[numberOfCommands].numberOfargument++;
			}
		| parameters STRING
			{
				if(commands[numberOfCommands].numberOfargument == 0){
					commands[numberOfCommands].numberOfargument++;
				}
				commands[numberOfCommands].args[commands[numberOfCommands].numberOfargument] = $2;
				commands[numberOfCommands].numberOfargument++;
			}
		| parameters WORD 
			{
				if(commands[numberOfCommands].numberOfargument == 0){
					commands[numberOfCommands].numberOfargument++;
				}
				commands[numberOfCommands].args[commands[numberOfCommands].numberOfargument] = $2;
				commands[numberOfCommands].numberOfargument++;
			}
		| parameters REDIRECT_IN FILENAME
			{
				
				inputfile = $3;
			}
                | parameters REDIRECT_OUT FILENAME
	                {
				outputfile = $3;
			}
		| parameters REDIRECT_ERROR FILENAME
			{
				outputError=1;
				outputfile = $3;
			}
		| parameters APPEND FILENAME
			{
				appendfile = $3;
			}
		| parameters APPEND_ERROR FILENAME
			{
				appendError=1;
				appendfile = $3;
			}
		|
		;

%%
extern int yylex();
extern int yyparse();
extern int yyerror(char *s);
extern yy_scan_string(const char * str);
int findCorrectExecutablePath(char *cmd, char *filepath);
char * removeFirstcharactor (char *charBuffer);
int checkBuiltInCommand(char *cmd);
void executeBuiltInCommand(struct command cmd);
void fillupBackgroundProcess(struct command cmd);
void setEnvironment(struct command cmd);
char * getEnv(char * VAR);
static void setHomeDirectory();
static void setCurrentDirectory();
static void readISHRCfile();

int yyerror(char *s)
{
	fprintf(stderr, "syntax error\n");
	return 0;
}


/* Signal Handler for SIGINT */
void sigintHandler(int sig_num)
{
	if (!pid || pid < 0)
		printf("\nbrijesh@ish: %s $: ",(char *)getEnv("PWD"));
	pid = -1;
	fflush(stdout);
}

/* Signal Handler for SIGINT */
void sigstopHandler(int sig_num) {
	if (pid > 0) {
		for (int i = 0; i <= numberOfCommands; i++) {
			
			if(commands[i].parent_id==pid && commands[i]._background !=1){
					signal(SIGTSTP, sigstopHandler);
					pid_t stdin_PGID;

					stdin_PGID = tcgetpgrp(STDIN_FILENO);
					tcsetpgrp(STDIN_FILENO, stdin_PGID);
					jobList[numberOfStoppedProcess].jobStatus=malloc(20);
					jobList[numberOfStoppedProcess].filepath=malloc(MAX_FILE_PATH);
					char *completeCommand=malloc(MAX_FILE_PATH);
					jobList[numberOfStoppedProcess].processName=malloc(MAX_FILE_PATH);
							for (int j = 0; j < commands[i].numberOfargument-1; j++) {
								jobList[numberOfStoppedProcess].args[j] = malloc(20);
								strcpy(jobList[numberOfStoppedProcess].args[j], (char *)commands[i].args[j]);
								strcat((char *)completeCommand, (char *)commands[i].args[j]);
								strcat((char *)completeCommand, " ");
							}
					strcat(jobList[numberOfStoppedProcess].processName, completeCommand);
					strcpy(jobList[numberOfStoppedProcess].jobStatus, "Stopped");
					strcpy(jobList[numberOfStoppedProcess].filepath, commands[i].filepath);
					jobList[numberOfStoppedProcess].numberOfargument = commands[i].numberOfargument;
					jobList[numberOfStoppedProcess]._pid=pid;
					int currentProcess=numberOfStoppedProcess++;
                                        printf("\n[%d]+	%s			%s\n",numberOfStoppedProcess,(char *)jobList[currentProcess].jobStatus,(char *)completeCommand);
				
			}
			break;
		}
	}
	else {
		printf("\nbrijesh@ish: %s $: ",(char *)getEnv("PWD"));
	}
	fflush(stdout);
}


int main()
{

	
	setHomeDirectory();
	setCurrentDirectory();
	readISHRCfile();
	char *commandline = NULL;
	size_t size;
	int nextCommand;
	while(1){
		nextCommand=0;
		signal(SIGINT, sigintHandler);		
		signal(SIGTSTP, sigstopHandler);

		/* Print the command prompt */
		printf("brijesh@ish: %s $: ",(char *)getEnv("PWD"));


		/* Read a command line */
		if (getline(&commandline, &size, stdin) != -1) {
			if (commandline != NULL && strlen((char *)commandline) > 1) {
				yy_scan_string(commandline);
				yyparse();

				
				
				int tempin = dup(STDIN_FILENO);
				int tempout = dup(STDOUT_FILENO);
				int temperror = dup(STDERR_FILENO);
				int fdout;
				int fdin;
				int fderror;
				
				

				if (inputfile) {
					fdin = open(inputfile, O_RDONLY, 0);
					if (-1 == fdin)
					{
						printf("\n [%s]\n", strerror(errno));
						return 1;
					}

				}
				else {
					fdin = dup(tempin);
				}

				
				for (int i = 0; i <= numberOfCommands; i++) {
					
					if (checkBuiltInCommand(commands[i].args[0]) > 0) {
						executeBuiltInCommand(commands[i]);

					}
					else {
						char *filepath = malloc(MAX_FILE_PATH);
						int success = 0;

						if ((commands[i].args[0])[0] == '/') {
							filepath = commands[i].args[0];
							success = 1;
						}
						else {
							success = findCorrectExecutablePath(commands[i].args[0], filepath);
							
						}

						commands[i].filepath = malloc(MAX_FILE_PATH);
						strcpy((char *)commands[i].filepath,filepath);


						if (success > 0) {
							
				

							dup2(fdin, STDIN_FILENO);
							close(fdin);
							if (commands[i]._pipe == 1 || ((i - 1) >= 0 && commands[i - 1]._pipe == 1) || inputfile || outputfile || appendfile) {

								if (commands[i]._semicolon == 1 || i == numberOfCommands) {
									if (outputfile) {
										fdout = open(outputfile, O_WRONLY | O_CREAT | O_TRUNC, 0640);
										if (-1 == fdout)

										{
											printf("\n [%s]\n", strerror(errno));
											return 1;
										}
										if(outputError == 1){
											fderror=dup(fdout);
										}
										outputfile = NULL;
										
									}
									else if (appendfile) {
										fdout = open(appendfile, O_RDWR | O_CREAT | O_APPEND, 0640);
										if (-1 == fdout)
										{
											printf("\n [%s]\n", strerror(errno));
											return 1;
										}
										if(appendError == 1){
											fderror=dup(fdout);
										}
										appendfile = NULL;
									}
									else {
										fdout = dup(tempout);
									}
								}
								else {
									int fd[2];
									pipe(fd);

									fdin = fd[0];
									if (commands[i]._pipe == 1) {
										fdout = fd[1];
									}
									else {
										fdout = dup(tempout);
									}


								}
							}
							else {
								
								dup2(tempin, STDIN_FILENO);
								dup2(tempout, STDOUT_FILENO);
								dup2(temperror, STDERR_FILENO);

								close(tempin);
								close(tempout);
								close(temperror);
							}
							inputfile = NULL;
							dup2(fdout, STDOUT_FILENO);
							if(appendError ==1 || outputError ==1){
								dup2(fderror, STDERR_FILENO);
								close(fderror);
								appendError = 0;
								outputError = 0;
								
							}
							
							close(fdout);

							pid = fork();
							
							if (pid == 0) {
								execve(filepath, commands[i].args, env);
								_exit(0);
							}
							else {
								commands[i].parent_id = pid;
								if (commands[i]._background != 1) {
									int foreGroundstatus=0;
									do {
										
										
										printf("\n|-----STATUS-----|\n");
										printf("COMMAND: %s\n", (char *)commands[i].args[0]);
										printf("continued:	%d\n"
										"Exit:		%d	exited status:	%i\n"
										"stopped:	%d	stopped signal:	%i\n"
										"signaled:	%d	exited signal:	%i\n",
										WIFCONTINUED(foreGroundstatus), WIFEXITED(foreGroundstatus), WEXITSTATUS(foreGroundstatus), WIFSTOPPED(foreGroundstatus), WSTOPSIG(foreGroundstatus), WIFSIGNALED(foreGroundstatus), WTERMSIG(foreGroundstatus));								
										printf("|-----STATUS-----|\n\n");
										waitpid(pid, &foreGroundstatus, WUNTRACED | WCONTINUED);
									
										
									} while (!WIFEXITED(foreGroundstatus)  && !WIFSTOPPED(foreGroundstatus));
									
								}
								else {
									int backGroundstatus=0;
									do {
										
										printf("\n|-----STATUS--back---|\n");
										printf("COMMAND: %s\n", (char *)commands[i].args[0]);
										printf("continued:	%d\n"
										"Exit:		%d	exited status:	%i\n"
										"stopped:	%d	stopped signal:	%i\n"
										"signaled:	%d	exited signal:	%i\n",
										WIFCONTINUED(backGroundstatus), WIFEXITED(backGroundstatus), WEXITSTATUS(backGroundstatus), WIFSTOPPED(backGroundstatus), WSTOPSIG(backGroundstatus), WIFSIGNALED(backGroundstatus), WTERMSIG(backGroundstatus));
										printf("|-----STATUS-back----|\n\n");
										waitpid(pid, &backGroundstatus, WNOHANG);
										
										
									} while (!WIFEXITED(backGroundstatus) && !WIFSTOPPED(backGroundstatus));
									jobList[numberOfStoppedProcess]._pid = commands[i].parent_id;
									jobList[numberOfStoppedProcess].jobStatus=malloc(20);
									strcpy(jobList[numberOfStoppedProcess].jobStatus, "Done");
									backGroundProcesses[numberOfBackgroundProcess]._pid = commands[i].parent_id;
									numberOfStoppedProcess++;
									numberOfBackgroundProcess++;
									printf("\n[%d]		%d\n",numberOfBackgroundProcess,backGroundProcesses[numberOfBackgroundProcess-1]._pid);
									fillupBackgroundProcess(commands[i]);
									
								}
								

								pid = -1;
							}

						}
						else {
							printf("%s: command not found\n", commands[i].args[0]);
						}
						
						
					}
				}


				dup2(tempin, STDIN_FILENO);
				dup2(tempout, STDOUT_FILENO);
				dup2(temperror, STDERR_FILENO);

				close(tempin);
				close(tempout);
				close(temperror);
				
				numberOfCommands = 0;
				memset(commands, 0, sizeof(commands));

			}
		}
		nextCommand=1;
	}
	
	return 0;
}

void fillupBackgroundProcess(struct command cmd) {
	for (int l = 0; l < numberOfStoppedProcess; l++) {
			if (cmd.parent_id == jobList[l]._pid) {
				char *completeBackGroundCommand = malloc(MAX_FILE_PATH);
				jobList[l].processName = malloc(MAX_FILE_PATH);
				jobList[l].filepath = malloc(MAX_FILE_PATH);
				jobList[l].args[0] = malloc(MAX_FILE_PATH);
				strcpy(jobList[l].args[0], cmd.args[0]);
				strcat((char *)completeBackGroundCommand, cmd.args[0]);
				strcat((char *)completeBackGroundCommand, " ");
					
				for (int j = 1; j < cmd.numberOfargument-1; j++) {
					jobList[l].args[j] = malloc(MAX_FILE_PATH);
					strcpy(jobList[l].args[j], cmd.args[j]);
					strcat((char *)completeBackGroundCommand, cmd.args[j]);
					strcat((char *)completeBackGroundCommand, " ");
				}
				strcat((char *)completeBackGroundCommand, "&");
				jobList[l].numberOfargument = cmd.numberOfargument;
				
				strcat(jobList[l].processName, (char *)completeBackGroundCommand);
				strcpy(jobList[l].filepath, cmd.filepath);
				
			}
	}

}


int findCorrectExecutablePath(char *cmd, char *filepath) {

	char *envPATHValue = getEnv("PATH");
	const char delim[2] = ":";
	char *token;
	int success = -1;

	/* get the first token */
	token = strtok(envPATHValue, delim);


	/* walk through other tokens */
	while (token != NULL)
	{
		strcpy(filepath, token);
		strcat(filepath, "/");
		strcat(filepath, cmd);

		
		if (access(filepath, X_OK) == 0) {
			success = 1;
			break;
		}
		else {
			token = strtok(NULL, delim);
		}
	}

	return success;

}

int checkBuiltInCommand(char *cmd) {
	for (int i = 0; i < 8; i++) {
		if (strcmp(builtInCommands[i], cmd) == 0) {
			return 1;

		}
	}
	return 0;
}

void executeBuiltInCommand(struct command cmd) {
	if (strcmp(cmd.args[0], "exit") == 0) {
		exit(0);
	}
	else if (strcmp(cmd.args[0], "jobs") == 0) {
		
		for (int i = 0; i < numberOfStoppedProcess; i++) {
			if(jobList[i].jobStatus != "Dead" && jobList[i].jobStatus !=  "Terminated"){
				printf("[%d]	%s			%s\n", (i+1), (char *)jobList[i].jobStatus, (char *)jobList[i].processName);					
			}
			
		}
	}
	else if (strcmp(cmd.args[0], "fg") == 0) {
		char *newString = removeFirstcharactor((char *)cmd.args[1]);
		int jobindex = atoi(newString)-1;
		if (jobindex > -1 && jobindex < numberOfStoppedProcess && jobList[jobindex].jobStatus != "Dead") {
			jobList[jobindex].jobStatus="Dead";
			
			commands[0].filepath = jobList[jobindex].filepath;
			commands[0].numberOfargument = jobList[jobindex].numberOfargument;
			for (int i = 0; i < jobList[jobindex].numberOfargument - 1; i++) {
				commands[0].args[i] = jobList[jobindex].args[i];
			}
			commands[0]._background = 0;
			commands[0].args[jobList[jobindex].numberOfargument] = NULL;
			pid = fork();
			
			if (pid == 0) {
				execve(commands[0].filepath, commands[0].args, env);
				_exit(0);
			}
			else {
				commands[0].parent_id = pid;
				int foreGroundstatus=0;
				do {


					printf("\n|-----STATUS-----|\n");
					printf("COMMAND: %s\n", (char *)commands[0].args[0]);
					printf("continued:	%d\n"
						"Exit:		%d	exited status:	%i\n"
						"stopped:	%d	stopped signal:	%i\n"
						"signaled:	%d	exited signal:	%i\n",
						WIFCONTINUED(foreGroundstatus), WIFEXITED(foreGroundstatus), WEXITSTATUS(foreGroundstatus), WIFSTOPPED(foreGroundstatus), WSTOPSIG(foreGroundstatus), WIFSIGNALED(foreGroundstatus), WTERMSIG(foreGroundstatus));
					printf("|-----STATUS-----|\n\n");
					waitpid(pid, &foreGroundstatus, WUNTRACED | WCONTINUED);


				} while (!WIFEXITED(foreGroundstatus) && !WIFSTOPPED(foreGroundstatus));
				pid = -1;
			}

		}else{
			printf("ish: fg: %d: no such job\n",(jobindex+1));

		}
	}
	else if (strcmp(cmd.args[0], "bg") == 0) {
		char *newString = removeFirstcharactor((char *)cmd.args[1]);
		int jobindex = atoi(newString) - 1;
		if (jobindex > -1 && jobindex < numberOfStoppedProcess &&jobList[jobindex].jobStatus != "Dead") {
			jobList[jobindex].jobStatus="Dead";
			commands[0]._background = 1;
			commands[0].filepath = jobList[jobindex].filepath;
			commands[0].numberOfargument = jobList[jobindex].numberOfargument;
				
			for (int i = 0; i < jobList[jobindex].numberOfargument - 1; i++) {
				commands[0].args[i] = jobList[jobindex].args[i];
			}
			commands[0].args[jobList[jobindex].numberOfargument] = NULL;
			
			pid = fork();
			
			if (pid == 0) {
				execve(commands[0].filepath, commands[0].args, env);
				_exit(0);
			}
			else {
				commands[0].parent_id = pid;
				pid=-1;
				int backGroundstatus = 0;
				do {

					printf("\n|-----STATUS--back---|\n");
					printf("COMMAND: %s\n", (char *)commands[0].args[0]);
					printf("continued:	%d\n"
						"Exit:		%d	exited status:	%i\n"
						"stopped:	%d	stopped signal:	%i\n"
						"signaled:	%d	exited signal:	%i\n",
						WIFCONTINUED(backGroundstatus), WIFEXITED(backGroundstatus), WEXITSTATUS(backGroundstatus), WIFSTOPPED(backGroundstatus), WSTOPSIG(backGroundstatus), WIFSIGNALED(backGroundstatus), WTERMSIG(backGroundstatus));
					printf("|-----STATUS-back----|\n\n");
					waitpid(pid, &backGroundstatus, WNOHANG);


				} while (!WIFEXITED(backGroundstatus) && !WIFSTOPPED(backGroundstatus));
				jobList[numberOfStoppedProcess]._pid = commands[0].parent_id;
				jobList[numberOfStoppedProcess].jobStatus = malloc(20);
				strcpy(jobList[numberOfStoppedProcess].jobStatus, "Done");
				backGroundProcesses[numberOfBackgroundProcess]._pid = commands[0].parent_id;
				numberOfStoppedProcess++;
				numberOfBackgroundProcess++;
				printf("\n[%d]		%d\n", numberOfBackgroundProcess, backGroundProcesses[numberOfBackgroundProcess - 1]._pid);
				fillupBackgroundProcess(commands[0]);
			}

		}else{
			printf("ish: bg: %d: no such job\n",(jobindex+1));

		}
	}else if(strcmp(cmd.args[0], "cd") == 0){
		char *newDirectoryPath=malloc(256);
		if(cmd.args[1] != NULL){
			if ((cmd.args[1])[0] == '/') {
				strcpy(newDirectoryPath,cmd.args[1]);
			}
			else {
				strcpy(newDirectoryPath,(char *)getEnv("PWD"));
				strcat(newDirectoryPath,"/");
				strcat(newDirectoryPath,cmd.args[1]);
			}
		}else{
			strcpy(newDirectoryPath,(char *)getEnv("HOME"));
		}
		
		int ret = chdir (newDirectoryPath);
		if(ret == 0){
			cmd.args[0]="setenv";
			cmd.args[1]="PWD";
			cmd.args[2]= newDirectoryPath;
			setEnvironment(cmd);
		}else{
			printf("ish: cd: %s: No such file or directory\n",(char *)cmd.args[1]);
		}
		
	}else if(strcmp(cmd.args[0], "kill") == 0){
		
		char *newString = removeFirstcharactor((char *)cmd.args[1]);
		int jobindex = atoi(newString)-1;
		if (jobindex > -1 && jobindex < numberOfStoppedProcess && (jobList[jobindex].jobStatus != "Dead" || jobList[jobindex].jobStatus != "Terminated")) {
			int success=kill(jobList[jobindex]._pid,SIGTERM);
			if(success == 0){
				jobList[jobindex].jobStatus = "Terminated";
				printf("[%d]	%s			%s\n", (jobindex+1), (char *)jobList[jobindex].jobStatus, (char *)jobList[jobindex].processName);	
			}		
			
		}else{
			printf("ish: kill: %d: no such job\n",(jobindex+1));

		}
		
	}else if(strcmp(cmd.args[0], "setenv") == 0){
			setEnvironment(cmd);
			
	}else if(strcmp(cmd.args[0], "unsetenv") == 0){
		int environmentVaribleExist=-1;
		for(int i=0;i<numberOfEnvironmentVariable;i++){
			const char delim[2] = "=";
			char *token;
			/* get the first token */
			char *tempEnv=malloc(strlen(env[i])+1);
			strcpy(tempEnv,env[i]);
			token = strtok(tempEnv, delim);
			if(strcmp(cmd.args[1], token) == 0){
				environmentVaribleExist=i;
				break;
			}
		}
		if(environmentVaribleExist > -1){
			for(int i=environmentVaribleExist;i<numberOfEnvironmentVariable-1;i++){
				env[i]=env[i+1];
			}
			numberOfEnvironmentVariable--;

		}
	}
	
}

char * removeFirstcharactor (char *charBuffer) {
  int length = strlen(charBuffer);
  char *str;
  if (length <= 1) {
    str = (char *) malloc(1);
    str[0] = '\0';
  } else {
    str = (char *) malloc(length+1);
    strcpy(str, &charBuffer[1]);
  }
  return str;
}


void setEnvironment(struct command cmd) {
	if (cmd.args[0] != NULL && cmd.args[1] != NULL && cmd.args[2] != NULL) {
		int environmentVaribleExist = -1;
		for (int i = 0; i<numberOfEnvironmentVariable; i++) {
			const char delim[2] = "=";
			char *token;
			/* get the first token */
			char *tempEnv = malloc(strlen(env[i])+1);
			strcpy(tempEnv, env[i]);
			token = strtok(tempEnv, delim);
			if (strcmp(cmd.args[1], token) == 0) {
				environmentVaribleExist = i;
				break;
			}
		}
		char *pathvalue = malloc(MAX_FILE_PATH);
		if ((cmd.args[1])[0] == '"' && (cmd.args[1])[strlen(cmd.args[1]) - 1] == '"') {
			memmove(cmd.args[1], cmd.args[1] + 1, strlen(cmd.args[1]));
			(cmd.args[1])[strlen(cmd.args[1]) - 1] = 0;
		}
		strcpy(pathvalue, "");
		strcat(pathvalue, cmd.args[1]);
		strcat(pathvalue, "=");
		if ((cmd.args[2])[0] == '"' && (cmd.args[2])[strlen(cmd.args[2]) - 1] == '"') {
			memmove(cmd.args[2], cmd.args[2] + 1, strlen(cmd.args[2]));
			(cmd.args[2])[strlen(cmd.args[2]) - 1] = 0;
		}
		strcat(pathvalue, cmd.args[2]);
		if (environmentVaribleExist > -1) {
			env[environmentVaribleExist] = pathvalue;

		}
		else {
			env[numberOfEnvironmentVariable] = pathvalue;
			numberOfEnvironmentVariable++;
		}

	}
	else if (cmd.args[0] != NULL && cmd.args[1] != NULL && cmd.args[2] == NULL) {
		int environmentVaribleExist = -1;
		for (int i = 0; i<numberOfEnvironmentVariable; i++) {
			const char delim[2] = "=";
			char *token;
			/* get the first token */
			char *tempEnv = malloc(strlen(env[i])+1);
			strcpy(tempEnv, env[i]);
			token = strtok(tempEnv, delim);
			if (strcmp(cmd.args[1], token) == 0) {
				environmentVaribleExist = i;
				break;
			}
		}
		char *pathvalue = malloc(MAX_FILE_PATH);
		strcpy(pathvalue, "");
		strcat(pathvalue, cmd.args[1]);
		strcat(pathvalue, "=");
		strcat(pathvalue, "");
		if (environmentVaribleExist > -1) {
			env[environmentVaribleExist] = pathvalue;

		}
		else {

			env[numberOfEnvironmentVariable] = pathvalue;
			numberOfEnvironmentVariable++;
		}
	}
	else if (cmd.args[0] != NULL && cmd.args[1] == NULL && cmd.args[2] == NULL) {
		for (int i = 0; i<numberOfEnvironmentVariable; i++) {
			printf("%s\n", (char *)env[i]);
			
		}
	}

}
char *RemoveSpaces(char* str)
{
  char *end;

  // Trim leading space
  while(isspace((unsigned char)*str)) str++;

  if(*str == 0)  // All spaces?
    return str;

  // Trim trailing space
  end = str + strlen(str) - 1;
  while(end > str && isspace((unsigned char)*end)) end--;

  // Write new null terminator
  *(end+1) = 0;

  return str;
}


char * getEnv(char * VAR) {
		for (int i = 0; i<numberOfEnvironmentVariable; i++) {
			const char delim[2] = "=";
			/* get the first token */
			char *tempEnv = malloc(strlen(env[i])+1);
			strcpy(tempEnv, env[i]);
			char *token = strtok(tempEnv, delim);
			char *varValue = strtok(NULL, "");
			if (strcmp(VAR, token) == 0) {
				return varValue;
			}
		}
}


static void setHomeDirectory(){
	struct passwd *pw = getpwuid(getuid());

	char *homedir = pw->pw_dir;
	struct command cmd;
	cmd.args[0]="setenv";
	cmd.args[1]="HOME";
	cmd.args[2]= homedir;
	setEnvironment(cmd);
}

static void setCurrentDirectory(){
	
	char currentWorkingDirectory[1024];
	getcwd(currentWorkingDirectory, sizeof(currentWorkingDirectory));
	struct command cmd;
	cmd.args[0]="setenv";
	cmd.args[1]="PWD";
	cmd.args[2]= currentWorkingDirectory;
	setEnvironment(cmd);
}


static void readISHRCfile(){
	
	if( access( ".ishrc", R_OK ) != -1 ) {
		FILE* fp;
		char line[1024];
	    	fp = fopen(".ishrc", "r");

		if(fp != NULL){
			while(fgets(line, 1024, (FILE*) fp)) {
			   	 /* get the first token */
			   	const char delim[2] = " ";
				char *token = strtok(line, delim);
				if(strcmp(token,"setenv")==0){
					/* walk through other tokens */
					int numberOfArgument=0;
					struct command cmd;
					cmd.args[numberOfArgument++]=token;
					while (token != NULL)
					{
						token = strtok(NULL, delim);
						if(token != NULL){
							cmd.args[numberOfArgument++]=RemoveSpaces(token);
						}
						
						
					
					}
					setEnvironment(cmd);
					
				}
			}
		}
		fclose(fp);
	
	}else{
		printf(".ishrc File not found for initial setup\n");
	}
	
	
}
