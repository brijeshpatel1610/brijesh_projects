/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Brijesh
 */
public class ExternalMergeSort {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        // TODO code application logic here
        final int RECORDSIZE = 36;
        final int STRING_LEN = 10;
        final int INTEGER_LEN = 8;
        final int FILESIZE = (int) Math.pow(2.0, 20.0) * 100; // size of files
        int RECORD_FIT_IN_EXTERNAL_BUFFER_FROM_EACH_FILE = (int) Math.pow(2.0, 20.0) * 9 / RECORDSIZE; //Number of records that fit in each buffer
        final int numRecordsPerFile = (int) (FILESIZE / RECORDSIZE);

        final String INPUT = "relation.bin";
        final String TEMPFILE = "temp";
        final String OUTPUT = "sorted";

        File file = new File(INPUT);
        FileInputStream fis = null;

        long startTime = System.currentTimeMillis();
        try {
            fis = new FileInputStream(file);
            final int RELATION_FILE_SIZE = fis.available(); //length of relation.bin
            final int TOTAL_RECORDS_IN_RELATION_FILE = RELATION_FILE_SIZE / RECORDSIZE;

            final int numFiles = (int) Math.ceil((double) RELATION_FILE_SIZE / (double) FILESIZE);
            byte[] integerByteArray1 = new byte[INTEGER_LEN];
            byte[] integerByteArray2 = new byte[INTEGER_LEN];
            byte[] stringByteArray1 = new byte[STRING_LEN];
            byte[] stringByteArray2 = new byte[STRING_LEN];

            for (int i = 0; i < numFiles; i++) {

                List<Record> InternalBuffer = new ArrayList<>();
                int NumOfBytesInCurrentFile = Math.min(numRecordsPerFile * RECORDSIZE, (TOTAL_RECORDS_IN_RELATION_FILE - i * numRecordsPerFile) * RECORDSIZE);
                byte[] fileBuffer = new byte[NumOfBytesInCurrentFile];

                fis.read(fileBuffer, 0, NumOfBytesInCurrentFile);
            
                for (int j = 0; j < NumOfBytesInCurrentFile; j = j + RECORDSIZE) {
                    integerByteArray1 = Arrays.copyOfRange(fileBuffer, j, j + 8);
                    integerByteArray2 = Arrays.copyOfRange(fileBuffer, j + 8, j + 16);
                    stringByteArray1 = Arrays.copyOfRange(fileBuffer, j + 16, j + 26);
                    stringByteArray2 = Arrays.copyOfRange(fileBuffer, j + 26, j + 36);
                    Record r = new Record(bytesToLong(integerByteArray1), bytesToLong(integerByteArray2), new String(stringByteArray1), new String(stringByteArray2));
                    InternalBuffer.add(r);
                }

                Collections.sort(InternalBuffer);

                File fout = new File(TEMPFILE + i + ".bin");
                FileOutputStream fos = new FileOutputStream(fout);
                int sizeOfInternalBuffer = InternalBuffer.size();
                ByteBuffer fileWriteBuffer = ByteBuffer.allocate(NumOfBytesInCurrentFile);
                for (int j = 0; j < sizeOfInternalBuffer; j++) {
                    fileWriteBuffer.put(longToBytes(InternalBuffer.get(j).integer1));
                    fileWriteBuffer.put(longToBytes(InternalBuffer.get(j).integer2));
                    fileWriteBuffer.put(InternalBuffer.get(j).string1.getBytes());
                    fileWriteBuffer.put(InternalBuffer.get(j).string1.getBytes());
                }
                fos.write(fileWriteBuffer.array());
                fos.close();
                System.out.println("------internal file " + i + " created and sorted----------");
            }
            fis.close();
            System.out.println("------all files created and sorted----------");

            for (int attempt = 0; attempt < numFiles + 1; attempt++) {

                List<Record> ExternalBuffer = new ArrayList<>();

                for (int fileNumber = 0; fileNumber < numFiles; fileNumber++) {

                    File file1 = new File(TEMPFILE + fileNumber + ".bin");
                    FileInputStream fis1 = new FileInputStream(file1);

                    fis1.skip(attempt * RECORDSIZE * RECORD_FIT_IN_EXTERNAL_BUFFER_FROM_EACH_FILE);

                    int availableRecordInfile = fis1.available() / RECORDSIZE;

                    if (availableRecordInfile > 0) {
                        int size = Math.min(availableRecordInfile, RECORD_FIT_IN_EXTERNAL_BUFFER_FROM_EACH_FILE);
                        byte[] fileBuffer = new byte[size * RECORDSIZE];
                        fis1.read(fileBuffer, 0, size * RECORDSIZE);
                        for (int j = 0; j < size * RECORDSIZE; j = j + RECORDSIZE) {
                            integerByteArray1 = Arrays.copyOfRange(fileBuffer, j, j + 8);
                            integerByteArray2 = Arrays.copyOfRange(fileBuffer, j + 8, j + 16);
                            stringByteArray1 = Arrays.copyOfRange(fileBuffer, j + 16, j + 26);
                            stringByteArray2 = Arrays.copyOfRange(fileBuffer, j + 26, j + 36);
                            Record r = new Record(bytesToLong(integerByteArray1), bytesToLong(integerByteArray2), new String(stringByteArray1), new String(stringByteArray2));
                            ExternalBuffer.add(r);
                        }
                    }
                    fis1.close();
                }

                System.out.println("------External buffer(100 MB) fill up with equal size(9MB) data from each file----------");

                Collections.sort(ExternalBuffer);
                System.out.println("------External buffer sorted----------");

                File finalWrite = new File(OUTPUT + ".bin");
                FileOutputStream finalos = new FileOutputStream(finalWrite, true);
                int ExternalBufferSize = ExternalBuffer.size();
                ByteBuffer fileWriteBuffer = ByteBuffer.allocate(ExternalBufferSize * RECORDSIZE);

                for (int j = 0; j < ExternalBufferSize; j++) {
                    fileWriteBuffer.put(longToBytes(ExternalBuffer.get(j).integer1));
                    fileWriteBuffer.put(longToBytes(ExternalBuffer.get(j).integer2));
                    fileWriteBuffer.put(ExternalBuffer.get(j).string1.getBytes());
                    fileWriteBuffer.put(ExternalBuffer.get(j).string1.getBytes());
                }
                finalos.write(fileWriteBuffer.array());

                finalos.close();
                System.out.println("------Data from External buffer(100MB) dumped into sorted.bin file----------");
            }
            long endTime = System.currentTimeMillis();
            System.out.println("\n Success !!! \n");
            System.out.println("\n You have sorted.bin file which has sorted data of relation.bin file \n");
            System.out.printf("\nIt took %.2f Minutes\n", (double) (endTime - startTime) / 60000);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }

    public static long bytesToLong(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.put(bytes);
        buffer.flip();
        return buffer.getLong();
    }

    public static byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.putLong(x);
        return buffer.array();
    }
}
