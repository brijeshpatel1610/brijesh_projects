/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Comparator;

/**
 *
 * @author Brijesh
 */
public class Record implements Comparable<Record> {

    public long integer1;
    public long integer2;
    public String string1;
    public String string2;

    public Record(long integer1, long integer2, String string1, String string2) {
        this.integer1 = integer1;
        this.integer2 = integer2;
        this.string1 = string1;
        this.string2 = string2;
    }

    public String getString1() {
        return string1;
    }

    public String getString2() {
        return string2;
    }
    
    

    @Override
    public int compareTo(Record r2) {
        return string1.compareTo(r2.string1);
    }

}
