
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Brijesh
 */
public class Search {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        final int RECORDSIZE = 36;
        final int Num_Record_In_OutputFile = 31060840;
        final int STRING_LEN = 10;
        final int INTEGER_LEN = 8;
        final String OUTPUT = "sorted.bin";
        byte[] integerByteArray1 = new byte[INTEGER_LEN];
        byte[] integerByteArray2 = new byte[INTEGER_LEN];
        byte[] stringByteArray1 = new byte[STRING_LEN];
        byte[] stringByteArray2 = new byte[STRING_LEN];
        long startTime = System.currentTimeMillis();

        boolean stop = false;

        File file = new File(OUTPUT);
        RandomAccessFile fis = new RandomAccessFile(file, "r");

        System.out.println("Please enter a word you want to search: ");
        Scanner sc = new Scanner(System.in);
        String searchString = sc.next();
        boolean found = false;
        if (searchString != "") {
            searchString = makeStringOfLength10(searchString);
            int lowerPosition = 0;
            int upperPosition = (int) Num_Record_In_OutputFile;
            int count = 0;

            while (upperPosition >= lowerPosition) {
                count++;
                int middlePosition = (lowerPosition + upperPosition) / 2;
                fis.seek(middlePosition * RECORDSIZE);
                fis.read(integerByteArray1, 0, 8);
                fis.read(integerByteArray2, 0, 8);
                fis.read(stringByteArray1, 0, 10);
                fis.read(stringByteArray2, 0, 10);

                if ((searchString.toLowerCase().compareTo(new String(stringByteArray1).toLowerCase())) < 0) {
                    upperPosition = middlePosition - 1;
                } else if ((searchString.toLowerCase().compareTo(new String(stringByteArray1).toLowerCase())) > 0) {
                    lowerPosition = middlePosition + 1;
                } else {
                    found = true;
                    break;
                }
            }
            if (found) {
                System.out.println("Success!!!");
                System.out.println(bytesToLong(integerByteArray1)+" "+bytesToLong(integerByteArray2)+" "+new String(stringByteArray1)+" "+new String(stringByteArray2));
            } else {
                System.out.println("Sorry no matching string found !!!");
            }
            long endTime = System.currentTimeMillis();
            System.out.printf("It took %.2f Seconds\n", (double) (endTime - startTime) / 1000);
        }

    }

    public static String makeStringOfLength10(String searchString) {
        while (searchString.length() < 10) {
            searchString = searchString + " ";
        }
        return searchString;
    }

    public static long bytesToLong(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.put(bytes);
        buffer.flip();
        return buffer.getLong();
    }
}
