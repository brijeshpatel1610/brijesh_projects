/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adb_project2;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 *
 * @author Brijesh
 */
public class ADB_PROJECT2 {

    /**
     * @param args the command line arguments
     */
    private static int resultFileNumber = 1;

    public static void main(String[] args) throws IOException {
        // TODO code application logic here

        //check result directory exists or not, if not then create
        final String resultDirectoryName = "result/";
        final String queryFilePath = "query.txt";
        final String inputDirectoryName = "files/";
        final String inputFilePath = "dataset.txt";

        long startTime = System.currentTimeMillis();

        // Read the Query File
        List<String> LinesInQueryFile = Files.readAllLines(Paths.get(queryFilePath), StandardCharsets.UTF_8);

        // Read the data File
        List<String> LinesInDataSetFile = Files.readAllLines(Paths.get(inputFilePath), StandardCharsets.UTF_8);

        //////////////////////////////////////////////////////////////
        LinesInQueryFile.parallelStream().forEach(
                queryFileLine -> {

                    //variable declaration
                    List<Point> queryTrajectoryPoints = new ArrayList<>();
                    List<Point> trajectoryPoints;
                    List<String> LinesInTrajectoryFile;
                    int noOfLinesInQueryTrajectoryFile = 0;
                    int noOfLinesInTrajectoryFile = 0;
                    Point point;
                    String[] tokens;

                    //query file variable declaration
                    String queryTrajectoryFileName;
                    double queryFrechetDistance;
                    
                    //variable require during frechet distance calculation
                    double frechetDistanceBetweenTwoTrajectory;
                    double[][] frechetDistanceArray;

                    tokens = queryFileLine.split(Pattern.quote(" "));
                    queryTrajectoryFileName = tokens[0];
                    queryFrechetDistance = Double.parseDouble(tokens[1]);

                    //put all the points of query trajectory into one list.
                    List<String> LinesInQueryTrajectoryFile = new ArrayList<>();
                    try {
                        LinesInQueryTrajectoryFile = Files.readAllLines(Paths.get(inputDirectoryName + queryTrajectoryFileName), StandardCharsets.UTF_8);
                    } catch (IOException ex) {
                        Logger.getLogger(ADB_PROJECT2.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    noOfLinesInQueryTrajectoryFile = LinesInQueryTrajectoryFile.size();

                    for (int i = 1; i < noOfLinesInQueryTrajectoryFile; i++) {
                        tokens = LinesInQueryTrajectoryFile.get(i).split(Pattern.quote(" "));
                        point = new Point(Double.parseDouble(tokens[0]), Double.parseDouble(tokens[1]));
                        queryTrajectoryPoints.add(point);
                    }

                    // calculate frechet distance between query trajectory and each trajectory contain in trajectory list. 
                    for (int t = 1; t < LinesInDataSetFile.size(); t++) {

                        LinesInTrajectoryFile = new ArrayList<>();
                        trajectoryPoints = new ArrayList<>();

                        try {
                            LinesInTrajectoryFile = Files.readAllLines(Paths.get(inputDirectoryName + LinesInDataSetFile.get(t)), StandardCharsets.UTF_8);
                        } catch (IOException ex) {
                            Logger.getLogger(ADB_PROJECT2.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        noOfLinesInTrajectoryFile = LinesInTrajectoryFile.size();
                        for (int i = 1; i < noOfLinesInTrajectoryFile; i++) {
                            tokens = LinesInTrajectoryFile.get(i).split(Pattern.quote(" "));
                            point = new Point(Double.parseDouble(tokens[0]), Double.parseDouble(tokens[1]));
                            trajectoryPoints.add(point);
                        }

                        frechetDistanceArray = new double[queryTrajectoryPoints.size()][trajectoryPoints.size()];

                        // initialize all values to -1
                        for (int j = 0; j < frechetDistanceArray.length; j++) {
                            for (int k = 0; k < frechetDistanceArray[j].length; k++) {
                                frechetDistanceArray[j][k] = -1.0;
                            }
                        }

                        for (int i = 0; i < queryTrajectoryPoints.size(); i++) {
                            for (int j = 0; j < trajectoryPoints.size(); j++) {

                                frechetDistanceBetweenTwoTrajectory = computeFrechetDistance(i, j, frechetDistanceArray, queryTrajectoryPoints.get(i), trajectoryPoints.get(j));
                                if ((frechetDistanceBetweenTwoTrajectory < Math.pow(queryFrechetDistance, 2)) && (i == queryTrajectoryPoints.size() - 1) && (j == trajectoryPoints.size() - 1)) {
                                    try {
                                        PrintWriter writer = new PrintWriter(resultDirectoryName + "result-" + (resultFileNumber++) + ".txt", "UTF-8");
                                        writer.println(queryTrajectoryFileName);
                                        writer.println(LinesInDataSetFile.get(t));
                                        writer.close();
                                    } catch (IOException e1) {
                                        System.out.println("-----------IOException OCCURE while writing the IO File-------------");
                                    }
                                }

                            }
                        }

                    }

                }
        );

        long endTime = System.currentTimeMillis();

        System.out.printf("\nDONE !!!\n");
        System.out.printf("\nIt took %.2f Seconds\n", (double) (endTime - startTime) / 1000);
        System.out.printf("\nPlease check the result directory for output !!!\n");

    }

    public static double computeFrechetDistance(int i, int j, double[][] frechetDistanceArray, Point p1, Point p2) {
        if (frechetDistanceArray[i][j] > -1) {
            return frechetDistanceArray[i][j];
        } else if (i == 0 && j == 0) {
            frechetDistanceArray[i][j] = euclideanSqureDistance(p1, p2);
        } else if (i > 0 && j == 0) {
            frechetDistanceArray[i][j] = Math.max(frechetDistanceArray[i - 1][0], euclideanSqureDistance(p1, p2));
        } else if (i == 0 && j > 0) {
            frechetDistanceArray[i][j] = Math.max(frechetDistanceArray[0][j - 1], euclideanSqureDistance(p1, p2));
        } else if (i > 0 && j > 0) {
            frechetDistanceArray[i][j] = Math.max(Math.min(Math.min(frechetDistanceArray[i - 1][j], frechetDistanceArray[i - 1][j - 1]), frechetDistanceArray[i][j - 1]), euclideanSqureDistance(p1, p2));
        } else {
            frechetDistanceArray[i][j] = Integer.MAX_VALUE;
        }

        return frechetDistanceArray[i][j];

    }

    public static double euclideanSqureDistance(Point p1, Point p2) {

        // for each dimension
        double Xdifference = Math.pow(Math.abs(p1.getX() - p2.getX()), 2);
        double Ydifference = Math.pow(Math.abs(p1.getY() - p2.getY()), 2);

        return (Xdifference + Ydifference);
    }

}
