
***********************
Algorithm Description *
***********************

1) First check that if the result directory is exists in project folder or not, if not then create the result directory in which result files will be created.

2) Then open the query.txt file and read all content of query file in one attempt and store it into one list and iterate that list line by line, and get 
queryfileNames and queryFrechetdistances.

3) For each queryFileName and and queryFrechetdistance, do the following process in parallel.

				----------------------------------------------------------------------------
				Parallel Process descriotion
				----------------------------------------------------------------------------

				1) open the queryFile(Trajectory) and put all the points of queryFile into one list by reading that file.
				2) create the bounding box for query trajectory by calculating the Minimum X , Minimum Y, Maximum X, Maximum Y of that query trajectory.
				
				3) open the dataset.txt file and read all the file names in one attempt and put them into one list.
				4) iterate filename list and for each file, do the following process.
							
							- create one Map<String filename, List<point> trajectoryPoints>, which will store the filename of trajectory and list of points of that trajectory.
							- calculate the bounding box for each trajectory specified in dataset.txt file.
							
							- find the minimum distance between query trajectory bounding box and dataset trajectory bounding box.
							- if the minimum distance is less then query_Frechet_distance then put that trajectory into one trajectory list in which all the trajectory has
							  minimum distance less the query_Frechet_distance.
							- Use the above trajectory list for finding the similar trajectory by appling the frechet distance formula. 