
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Brijesh
 */
public class JarvisMarch {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {
        // TODO code application logic here
        String fileName = "QuickWorstCase3.txt";
        Scanner fileScanner = new Scanner(new File(fileName));
        int totalNumberOfPoints = 0;
        List<Point> points = new ArrayList<Point>();
        while (fileScanner.hasNext()) {
            totalNumberOfPoints++;
            Point p = new Point(fileScanner.nextInt(), fileScanner.nextInt());
            points.add(p);
        }

        convexHull(points, totalNumberOfPoints);

    }

    public static List<Point> convexHull(List<Point> points, int totalNumberOfPoints) {
        System.out.println("--------------input point sequence for jarvis march-------------------");
        for (Point point : points) {
            System.out.println(point.getX()+" " +point.getY());
        }
        System.out.println("--------------input point sequence for jarvis march-------------------");
        if (totalNumberOfPoints < 3) {
            return null;
        } else {
            /**
             * find the leftmost point *
             */
            int leftMost = 0;
            for (int i = 1; i < totalNumberOfPoints; i++) {
                if (points.get(i).getX() < points.get(leftMost).getX()) {
                    leftMost = i;
                }
            }
            int p = leftMost;
            int q;
            List<Point> finalResult = new ArrayList<Point>();
            /**
             * iterate till p becomes leftMost *
             */
            int loopExecutionCount = 0;
            do {
                /**
                 * wrapping *
                 */
                finalResult.add(points.get(p));

                q = (p + 1) % totalNumberOfPoints;

                for (int i = 0; i < totalNumberOfPoints; i++) {
                    loopExecutionCount++;
                    if (findOrientation(points.get(p), points.get(i), points.get(q)) == 2) {
                        q = i;
                    }
                }

                p = q;
            } while (p != leftMost);
            return finalResult;
        }

    }

    public static int distance(Point p1, Point p2) {
        return (p1.getX() - p2.getX()) * (p1.getX() - p2.getX())
                + (p1.getY() - p2.getY()) * (p1.getY() - p2.getY());
    }

    public static int findOrientation(Point p, Point q, Point r) {
        int angle = (q.getY() - p.getY()) * (r.getX() - q.getX())
                - (q.getX() - p.getX()) * (r.getY() - q.getY());

        if (angle == 0) {
            return 0;
        }
        return (angle > 0) ? 1 : 2;
    }

}
