
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.Stack;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Brijesh
 */
public class Project2_cs456 {

    public static void main(String args[]) throws FileNotFoundException {

        String fileName = "small.txt";
        Scanner fileScanner = new Scanner(new File(fileName));
        int totalNumberOfPoints = 0;
        List<Point> originalPoints = new ArrayList<Point>();

        while (fileScanner.hasNext()) {
            totalNumberOfPoints++;
            originalPoints.add(new Point(fileScanner.nextInt(), fileScanner.nextInt()));
        }
        Date d1 = new Date();
        GrahamScan gs = new GrahamScan();

        Stack<Point> convexHullStack = gs.convexHull(originalPoints, totalNumberOfPoints);
        // Now stack has the output points, print contents of stack
        if (convexHullStack == null) {
            System.out.println("ConvexHull not possible");
        } else {
            while (!convexHullStack.empty()) {
               // System.out.println(convexHullStack.peek().getX() + " " + convexHullStack.peek().getY());
                convexHullStack.pop();
            }
        }

        Date d2 = new Date();
        long grahamTime = d2.getTime() - d1.getTime();
        //System.out.println("Graham: " + grahamTime);

        ////////////////////////////////////////
        Date d3 = new Date();
        JarvisMarch jm = new JarvisMarch();
        List<Point> jmConvexHul = null;
        jmConvexHul= jm.convexHull(originalPoints, totalNumberOfPoints);
        Date d4 = new Date();
        long jarvisTime = d4.getTime() - d3.getTime();
        if (jmConvexHul == null) {
            System.out.println("ConvexHull not possible");
        } else {
            for (Point jmconvexhull1 : jmConvexHul) {
                System.out.println(jmconvexhull1.getX() + " " + jmconvexhull1.getY());
            }
        }
        System.out.println("Jarvis: " + jarvisTime);
        System.out.println("Jarvis Total convexHull Points: " + jmConvexHul.size());

        ///////////////////////////////////
        Date d5 = new Date();
        QuickHull qh = new QuickHull();
        Set<Point> convexhull = qh.convexHull(originalPoints, totalNumberOfPoints);
        Date d6 = new Date();
        if (convexhull == null) {
            System.out.println("ConvexHull not possible");
        } else {
            for (Point convexhull1 : convexhull) {
               //System.out.println(convexhull1.getX() + " " + convexhull1.getY());
            }
        }
        long quickTime = d6.getTime() - d5.getTime();
       // System.out.println("Quick: " + quickTime);
       // System.out.println("Quick: " + convexhull.size());
    }

}
