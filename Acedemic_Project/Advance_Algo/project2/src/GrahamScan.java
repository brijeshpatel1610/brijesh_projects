
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Brijesh
 */
public class GrahamScan {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {
        // TODO code application logic here
        String fileName = "QuickWorstCase.txt";
        Scanner fileScanner = new Scanner(new File(fileName));
        int totalNumberOfPoints = 0;
        List<Point> points = new ArrayList<Point>();

        while (fileScanner.hasNext()) {
            totalNumberOfPoints++;
            points.add(new Point(fileScanner.nextInt(), fileScanner.nextInt()));
        }

        Stack<Point> convexHullStack = convexHull(points, totalNumberOfPoints);
        // Now stack has the output points, print contents of stack
        if (convexHullStack == null) {
            System.out.println("ConvexHull not possible");
        } else {
            while (!convexHullStack.empty()) {
                System.out.println(convexHullStack.peek().getX() + " " + convexHullStack.peek().getY());
                convexHullStack.pop();
            }
        }

    }

    public static Stack<Point> convexHull(List<Point> points, int totalNumberOfPoints) {

        // Find the bottommost point
      //  List<Point> points = new ArrayList<Point>(inputPoints);
        int minY = points.get(0).getY();
        int minIndex = 0;
        for (int i = 1; i < totalNumberOfPoints; i++) {
            int Y = points.get(i).getY();
            if ((Y < minY) || (minY == Y && points.get(i).getX() < points.get(minIndex).getX())) {
                minY = points.get(i).getY();
                minIndex = i;
            }
        }

        Point bottomMostPoint = points.get(minIndex);
        points.set(minIndex, points.get(0));
        points.set(0, bottomMostPoint);
        try {
            Collections.sort(points, new Comparator<Point>() {
                public int compare(Point first, Point second) {
                    int orientation = findOrientation(points.get(0), first, second);

                    if (orientation == 0) {
                        return (distance(points.get(0), second) >= distance(points.get(0), first)) ? -1 : 1;
                    }

                    return (orientation == 2) ? -1 : 1;
                }
            });
        } catch (IllegalArgumentException e) {
        }
                    
        int maximumPointsNeedToBeConsider = 1; // Initialize size of modified array
        List<Point> newPoints = new ArrayList<Point>();
        newPoints.add(points.get(0));
        for (int i = 1; i < totalNumberOfPoints; i++) {
            // Keep removing i while angle of i and i+1 is same
            // with respect to p0
            while (i < totalNumberOfPoints - 1 && findOrientation(points.get(0), points.get(i), points.get(i + 1)) == 0) {
                i++;
            }

            newPoints.add(points.get(i));
            maximumPointsNeedToBeConsider++;  // Update size of modified array
        }
        if (maximumPointsNeedToBeConsider < 3) {
            return null;
        } else {

            Stack<Point> convexHullStack = new Stack<>();
            convexHullStack.push(newPoints.get(0));
            convexHullStack.push(newPoints.get(1));
            convexHullStack.push(newPoints.get(2));
            for (int i = 3; i < maximumPointsNeedToBeConsider; i++) {
                // Keep removing top while the angle formed by
                // points next-to-top, top, and points[i] makes
                // a non-left turn
                while (convexHullStack.size() > 1 && findOrientation(nextToTop(convexHullStack), convexHullStack.peek(), newPoints.get(i)) != 2) {
                    convexHullStack.pop();
                }
                convexHullStack.push(newPoints.get(i));

            }

            return convexHullStack;
        }
    }

    public static int distance(Point p1, Point p2) {
        return (p1.getX() - p2.getX()) * (p1.getX() - p2.getX())
                + (p1.getY() - p2.getY()) * (p1.getY() - p2.getY());
    }

    public static int findOrientation(Point bottamPoint, Point p1, Point p2) {
        int angle = (p1.getY() - bottamPoint.getY()) * (p2.getX() - p1.getX())
                - (p1.getX() - bottamPoint.getX()) * (p2.getY() - p1.getY());

        if (angle == 0) {
            return 0;  // colinear
        }
        return (angle > 0) ? 1 : 2; // clock or counterclock wise
    }

    public static Point nextToTop(Stack<Point> convexHull) {

        Point top = convexHull.pop();
        Point nextToTop = convexHull.peek();
        convexHull.push(top);

        return nextToTop;
    }

}
