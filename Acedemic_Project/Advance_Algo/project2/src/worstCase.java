
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Brijesh
 */
public class worstCase {

    public static void main(String args[]) {
        List<Point> listpoint = new ArrayList<Point>();
        int number = 0;
        int duplicateNum = 0;
        do {
            Point p = getRandomPoint(1000000000);
            //  System.out.println(p.getX() + " "+p.getY());
            if (listpoint.contains(p)) {
                duplicateNum++;
            } else {
                number++;
                listpoint.add(p);
            }
            //xx and yy are the random number limits called from another part of the code
            System.out.println(number);

        } while (listpoint.size() < 200000);
        for (Point listpoint1 : listpoint) {
            System.out.println(listpoint1.getX() + " " + listpoint1.getY());
        }
    }

    public static Point getRandomPoint(int radius) {
        Random position = new Random();
        int angle = (int) ((position.nextInt(1000000000) + 0) * Math.PI * 2);

        int x = (int) (Math.cos(angle) * radius);
        int y = (int) (Math.sin(angle) * radius);

        return new Point(x, y);

    }
}
