
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Brijesh
 */
public class QuickHull {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) throws FileNotFoundException {
        // TODO code application logic here
        String fileName = "QuickWorstCase3.txt";
        Scanner fileScanner = new Scanner(new File(fileName));
        int totalNumberOfPoints = 0;
        List<Point> points = new ArrayList<Point>();

        while (fileScanner.hasNext()) {
            totalNumberOfPoints++;
            points.add(new Point(fileScanner.nextInt(), fileScanner.nextInt()));
        }

        convexHull(points, totalNumberOfPoints);

    }

    public static Set<Point> convexHull(List<Point> points, int totalNumberOfPoints) {
        List<Point> convexHull = new ArrayList<>();
        if (totalNumberOfPoints < 3) {
            return null;
        } else {
            // search extreme values
            Point rightmostPoint = points.get(0);
            Point leftmostPoint = points.get(0);
            int minY = Integer.MAX_VALUE;
            for (Point point : points) {
                if (point.getX() > rightmostPoint.getX()) {
                    rightmostPoint = point;
                } else if (point.getX() < leftmostPoint.getX()) {
                    leftmostPoint = point;
                }
            }

            convexHull.add(leftmostPoint);
            convexHull.add(rightmostPoint);
            points.remove(leftmostPoint);
            points.remove(rightmostPoint);

            ArrayList<Point> leftSet = new ArrayList<Point>();
            ArrayList<Point> rightSet = new ArrayList<Point>();

            for (int i = 0; i < points.size(); i++) {
                if (findPointLocation(leftmostPoint, rightmostPoint, points.get(i)) == 2) {
                    leftSet.add(points.get(i));
                } else if (findPointLocation(leftmostPoint, rightmostPoint, points.get(i)) == 1) {
                    rightSet.add(points.get(i));
                }
            }
            divideHull(leftmostPoint, rightmostPoint, rightSet, convexHull);
            divideHull(rightmostPoint, leftmostPoint, leftSet, convexHull);
            Set<Point> setConvexHull = new HashSet<>();
            setConvexHull.addAll(convexHull);
            
            return setConvexHull;
        }

    }

    public static int findDistance(Point p1, Point p2, Point p3) {
        int Xdiff = p2.getX() - p1.getX();
        int Ydiff = p2.getY() - p1.getY();
        int distance = Xdiff * (p1.getY() - p3.getY()) - Ydiff * (p1.getX() - p3.getX());
        if (distance < 0) {
            distance = -distance;
        }
        return distance;
    }

    public static void divideHull(Point leftmostPoint, Point rightmostPoint, List<Point> halfSetPoints, List<Point> convexHull) {
        int insertPosition = convexHull.indexOf(rightmostPoint);
        if (halfSetPoints.size() == 0) {
            return;
        }
        if (halfSetPoints.size() == 1) {
            Point onePoint = halfSetPoints.get(0);
            halfSetPoints.remove(onePoint);
            convexHull.add(insertPosition, onePoint);
            return;
        }
        int dist = Integer.MIN_VALUE;
        int furthestPoint = -1;
        for (int i = 0; i < halfSetPoints.size(); i++) {
            int distance = findDistance(leftmostPoint, rightmostPoint, halfSetPoints.get(i));
            if (distance > dist) {
                dist = distance;
                furthestPoint = i;
            }
        }
        Point pointWithMaxDistance = halfSetPoints.get(furthestPoint);
        halfSetPoints.remove(furthestPoint);
        convexHull.add(insertPosition, pointWithMaxDistance);

        // Determine who's to the left of leftmostPoint to pointWithMaxDistance
        ArrayList<Point> leftSet1 = new ArrayList<Point>();
        for (int i = 0; i < halfSetPoints.size(); i++) {
            Point M = halfSetPoints.get(i);
            if (findPointLocation(leftmostPoint, pointWithMaxDistance, M) == 1) {
                leftSet1.add(M);
            }
        }

        // Determine who's to the left of pointWithMaxDistance to rightmostPoint
        ArrayList<Point> leftSet2 = new ArrayList<Point>();
        for (int i = 0; i < halfSetPoints.size(); i++) {
            Point M = halfSetPoints.get(i);
            if (findPointLocation(pointWithMaxDistance, rightmostPoint, M) == 1) {
                leftSet2.add(M);
            }
        }

        divideHull(leftmostPoint, pointWithMaxDistance, leftSet1, convexHull);
        divideHull(pointWithMaxDistance, rightmostPoint, leftSet2, convexHull);
    }

    public static int findPointLocation(Point leftPoint, Point rightPoint, Point p) {
        int angle = (rightPoint.getY() - leftPoint.getY()) * (p.getX() - leftPoint.getX())
                - (rightPoint.getX() - leftPoint.getX()) * (p.getY() - leftPoint.getY());

        if (angle == 0) {
            return 0;  // colinear
        }
        return (angle > 0) ? 1 : 2; // clock or counterclock wise
    }

}
