
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Brijesh
 */
public class PointGenerator {

    public static void main(String args[]) {
        List<Point> listpoint = new ArrayList<Point>();
        Random position = new Random();
        int number = 0;
        int duplicateNum = 0;
        do {
            int x = position.nextInt(60);
            int y = position.nextInt(60);
            Point p = new Point(x, y);
            if (listpoint.contains(p)) {
                duplicateNum++;
            } else {
                listpoint.add(p);
            }
            //xx and yy are the random number limits called from another part of the code
            //System.out.println(x + " " + y);
            number++;
        } while (listpoint.size() < 3000);
        System.out.println(duplicateNum);
        System.out.println(listpoint.size());
        try {
            PrintWriter writer = new PrintWriter(listpoint.size() + ".txt", "UTF-8");
            for (Point listpoint1 : listpoint) {
                writer.println(listpoint1.getX()+" "+ listpoint1.getY());
            }
            writer.close();
        } catch (IOException e) {
            System.out.println("------------in exception-------");
            // do something
        }

    }
}
