
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Brijesh
 */
public class PatelPr01 {

    public static void main(String args[]) throws FileNotFoundException {
        String fileName = args[1];
        int bucketCount = Integer.parseInt(args[0]);
        Scanner lineCountScanner = new Scanner(new File(fileName));
        int totalInputElements = 0;
        while (lineCountScanner.hasNextInt()) {
            totalInputElements++;
            lineCountScanner.nextLine();
        }

        Integer inputArray[] = new Integer[totalInputElements];
        Scanner readLineScanner = new Scanner(new File(fileName));
        int i = 0;
        while (readLineScanner.hasNextInt()) {
            inputArray[i++] = readLineScanner.nextInt();
        }

        Date d1 = new Date();

        PatelBubble bb = new PatelBubble();
        bb.BucketSort(inputArray, bucketCount);

        Date d2 = new Date();
        long bubbleTime = d2.getTime() - d1.getTime();
        PatelQuick bq = new PatelQuick();
        bq.BucketSort(inputArray, bucketCount);

        Date d3 = new Date();
        long quickTime = d3.getTime() - d2.getTime();
        PatelRadix br = new PatelRadix();
        br.BucketSort(inputArray, bucketCount);

        Date d4 = new Date();
        long radixTime = d4.getTime() - d3.getTime();
        try {
            PrintWriter writer = new PrintWriter("Patel-" + totalInputElements + "-" + bucketCount + ".txt", "UTF-8");
            writer.println("Sort Size: " + inputArray.length);
            writer.println("Number of Buckets: " + bucketCount);
            writer.println("Bubble Sort: " + (bubbleTime) + " ms");
            writer.println("Quick Sort: " + (quickTime) + " ms");
            writer.println("Radix Sort: " + (radixTime) + " ms");
            for (int j = 0; j < inputArray.length; j++) {
                writer.println(inputArray[j]);
            }
        } catch (IOException e) {
			System.out.println("------------in exception-------");
            // do something
        }
    }

}
