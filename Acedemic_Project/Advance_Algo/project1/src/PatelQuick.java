/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Brijesh
 */
public class PatelQuick {

    public static Integer[] QuickSort(Integer bucketArray[], int start, int end) {
        int i = start;
        int k = end;

        if (end - start >= 1) {
            int pivot = bucketArray[start];

            while (k > i) {
                while (bucketArray[i] <= pivot && i <= end && k > i) {
                    i++;
                }
                while (bucketArray[k] > pivot && k >= start && k >= i) {
                    k--;
                }
                if (k > i) {
                    int temp = bucketArray[i];
                    bucketArray[i] = bucketArray[k];
                    bucketArray[k] = temp;
                }
            }
            int temp = bucketArray[start];
            bucketArray[start] = bucketArray[k];
            bucketArray[k] = temp;
            QuickSort(bucketArray, start, k - 1);
            QuickSort(bucketArray, k + 1, end);
        }

        return bucketArray;
    }

    public static void BucketSort(Integer[] inputArray, int bucketCount) {
        int inputArraySize = inputArray.length;
        if (inputArraySize == 0) {
            return;
        }

        // Determine minimum and maximum values
        Integer minValue = inputArray[0];
        Integer maxValue = inputArray[0];
        for (int i = 1; i < inputArraySize; i++) {
            if (inputArray[i] < minValue) {
                minValue = inputArray[i];
            } else if (inputArray[i] > maxValue) {
                maxValue = inputArray[i];
            }
        }

        // Initialise buckets
        int interval = (maxValue - minValue) / bucketCount + 1;
        List<List<Integer>> buckets = new ArrayList<List<Integer>>(bucketCount);
        for (int i = 0; i < bucketCount; i++) {
            buckets.add(new ArrayList<Integer>());
        }

        // Distribute input array values into buckets
        for (int i = 0; i < inputArraySize; i++) {
            buckets.get((inputArray[i] - minValue) / interval).add(inputArray[i]);
        }

        // Sort buckets and place back into input array
        int currentIndex = 0;
        for (int i = 0; i < bucketCount; i++) {
            Integer[] bucketArray = new Integer[buckets.get(i).size()];
            bucketArray = buckets.get(i).toArray(bucketArray);
            bucketArray = QuickSort(bucketArray, 0, bucketArray.length - 1);
            buckets.get(i).clear();
            buckets.get(i).addAll(Arrays.asList(bucketArray));
            for (int j = 0; j < bucketArray.length; j++) {
                inputArray[currentIndex++] = bucketArray[j];
            }
        }
    }

    public static void main(String args[]) throws FileNotFoundException {
        String fileName = args[1];
        int bucketCount = Integer.parseInt(args[0]);
        Scanner lineCountScanner = new Scanner(new File(fileName));
        int totalInputElements = 0;
        while (lineCountScanner.hasNextInt()) {
            totalInputElements++;
            lineCountScanner.nextLine();
        }

        Integer inputArray[] = new Integer[totalInputElements];
        Scanner readLineScanner = new Scanner(new File(fileName));
        int i = 0;
        while (readLineScanner.hasNextInt()) {
            inputArray[i++] = readLineScanner.nextInt();
        }

        Date d1 = new Date();

        BucketSort(inputArray, bucketCount);
        for (int j = 0; j < inputArray.length; j++) {
            System.out.println(inputArray[j]);
        }
        Date d2 = new Date();

        long diff = d2.getTime() - d1.getTime();
        System.out.println("Bucket Quick sort: " + diff);

    }
}
