
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Brijesh
 */
public class AstarPatel {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        // TODO code application logic here
        //  String fileName = args[0];
        Scanner sc = new Scanner(System.in);
        String fileName = "900.txt";
        String exit = null;
        do {
            FileInputStream fstream = new FileInputStream(fileName);
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

            //user input for startNode and targetNode
            System.out.print("Enter desired starting point: ");
            int startingPoint = sc.nextInt();
            System.out.print("Enter desired destination: ");
            int targetPoint = sc.nextInt();

            String strLine;
            int lines = 0;

            //Count the number of line in file
            while ((strLine = br.readLine()) != null) {
                // Print the content on the console
                lines++;
            }
            fstream.getChannel().position(0);
            ArrayList<Node> nodeList = new ArrayList<>();
            Node startNode = null;
            Node targetNode = null;
            Date d1 = new Date();
            if (lines == 2) {
                // read the vertex name and position 
                strLine = br.readLine();
                String[] splited;
                splited = strLine.split("\\s+"); // split the line by space delimeter
                Node node = null;
                for (String splited1 : splited) {

                    int nodeNumber = Integer.parseInt(splited1.split(":")[0]);
                    int x = Integer.parseInt(splited1.split(":")[1].split(",")[0]);
                    int y = Integer.parseInt(splited1.split(":")[1].split(",")[1]);
                    node = new Node(nodeNumber, x, y);
                    if (node.getNodeNumber() == targetPoint) {
                        targetNode = node;
                    }
                    nodeList.add(node);
                }
                // read the edges between vertex 
                strLine = br.readLine();
                //Close the input stream
                br.close();
                splited = strLine.split("\\s+");
                Node node1 = null;
                Node node2 = null;
                for (String splited1 : splited) {
                    int fromNode = Integer.parseInt(splited1.split(":")[0].split(",")[0]);
                    int toNode = Integer.parseInt(splited1.split(":")[0].split(",")[1]);
                    int cost = Integer.parseInt(splited1.split(":")[1]);

                    //set toNode in adjcentNodeList of fromNode
                    node1 = nodeList.stream().filter(o -> o.getNodeNumber() == fromNode).findFirst().get();

                    int heuristicDistance1 = directDistance(node1.getxCordinate(), node1.getyCordinate(), targetNode.getxCordinate(), targetNode.getyCordinate());
                    node1.setTargetHeuristicDist(heuristicDistance1);

                    //find heuristic distance from toNode to targetNode and store into toNode
                    node2 = nodeList.stream().filter(o -> o.getNodeNumber() == toNode).findFirst().get();
                    int heuristicDistance2 = directDistance(node2.getxCordinate(), node2.getyCordinate(), targetNode.getxCordinate(), targetNode.getyCordinate());
                    node2.setTargetHeuristicDist(heuristicDistance2);
                    node1.getAdacentList().add(new AdjNode(toNode, cost, heuristicDistance2));
                    node2.getAdacentList().add(new AdjNode(fromNode, cost, heuristicDistance1));
                }

                startNode = nodeList.stream().filter(o -> o.getNodeNumber() == startingPoint).findFirst().get();
                startNode.setTargetHeuristicDist(directDistance(startNode.getxCordinate(), startNode.getyCordinate(), targetNode.getxCordinate(), targetNode.getyCordinate()));
                int costToCurrentNode = 0;
                boolean pathExists = true;
                while (startNode.getTargetHeuristicDist() != 0) {

                    findCombinedHeuristicDistance(startNode.getAdacentList(), costToCurrentNode, startNode.getNodeNumber(), nodeList);
                    boolean nextNodePresent = nodeList.stream().sorted((o1, o2) -> o1.getCombineHeuristicDist() - o2.getCombineHeuristicDist()).filter(o -> o.isVisited() == false && o.getPredecessor() != -1 && o.getAdacentList().size() > 0).findFirst().isPresent();
                    if (nextNodePresent) {
                        Node nextNode = nodeList.stream().sorted((o1, o2) -> o1.getCombineHeuristicDist() - o2.getCombineHeuristicDist()).filter(o -> o.isVisited() == false && o.getPredecessor() != -1 && o.getAdacentList().size() > 0).findFirst().get();
                        costToCurrentNode = nodeList.stream().filter(o -> o.getNodeNumber() == nextNode.getPredecessor()).findFirst().get().getCostToCurrentNode() + nextNode.getAdacentList().stream().filter(o -> o.getNodeNumber() == nextNode.getPredecessor()).findFirst().get().getDistance();
                        startNode = nextNode;
                    } else {
                        System.out.println("Path is not exists between " + startingPoint + " and " + targetPoint);
                        pathExists = false;
                        break;
                    }
                }
                if (pathExists) {
                    System.out.println("For point " + startingPoint + " to point " + targetPoint);
                    System.out.print("The shortest path is ");
                    Map<String, Integer> result = findTheShortestPathSequence(nodeList, targetPoint);
                    System.out.println(result.entrySet().iterator().next().getKey());

                    System.out.println("Number of points in path is " + result.entrySet().iterator().next().getValue());
                    System.out.println("Total cost is " + costToCurrentNode);
                }

            } else {
                System.out.println("Invalid File Format");
                System.exit(0);
            }
            Date d2 = new Date();
            long grahamTime = d2.getTime() - d1.getTime();
            //System.out.println("Total Time: " + grahamTime);
            System.out.println();
            System.out.print("Would you like to search another best path ? (Y/N): ");
            exit = sc.next();
            System.out.println();

        } while (exit.equals(
                "Y") || exit.equals("y"));
    }

    public static int directDistance(int sourceX, int sourceY, int targetNodeX, int targetNodeY) {
        int xDiff = Math.abs(sourceX - targetNodeX);
        int yDiff = Math.abs(sourceY - targetNodeY);

        return (int) Math.sqrt(xDiff * xDiff + yDiff * yDiff);
    }

    public static void findCombinedHeuristicDistance(ArrayList<AdjNode> adjNodeList, int costToCurrentNode, int currentNodeNumber, ArrayList<Node> nodeList) {

        for (int i = 0; i < adjNodeList.size(); i++) {
            int nodeNumber = adjNodeList.get(i).getNodeNumber();
            int distance = costToCurrentNode + adjNodeList.get(i).getDistance() + adjNodeList.get(i).getTargetHeuristicDist();
            boolean nodeIsPresent = nodeList.stream().filter(o -> o.getNodeNumber() == nodeNumber && o.isVisited() == false).findFirst().isPresent();
            Node node = null;
            if (nodeIsPresent) {
                node = nodeList.stream().filter(o -> o.getNodeNumber() == nodeNumber && o.isVisited() == false).findFirst().get();
                node.setCostToCurrentNode(costToCurrentNode + adjNodeList.get(i).getDistance());
                node.setCombineHeuristicDist(distance);
                node.setPredecessor(currentNodeNumber);
            }
        }
        nodeList.stream().filter(o -> o.getNodeNumber() == currentNodeNumber).findFirst().get().setVisited(true);

    }

    public static Map<String, Integer> findTheShortestPathSequence(ArrayList<Node> nodeList, int targetPoint) {
        HashMap<String, Integer> result = new HashMap<>();
        String pathSequence = "";
        Node predessorNode = nodeList.stream().filter(o -> o.getNodeNumber() == targetPoint).findFirst().get();
        int[] pathArray = new int[nodeList.size() + 1];
        int pathArrayIndex = 0;
        while (predessorNode.getPredecessor() != -1) {
            int predessor = predessorNode.getPredecessor();
            pathArray[pathArrayIndex] = predessor;
            pathArrayIndex++;
            predessorNode = nodeList.stream().filter(o -> o.getNodeNumber() == predessor).findFirst().get();
        }
        for (int i = pathArrayIndex - 1; i > -1; i--) {
            pathSequence += String.valueOf(pathArray[i]) + " ";
        }
        pathSequence += String.valueOf(targetPoint);
        result.put(pathSequence, pathArrayIndex + 1);
        return result;
    }
}
