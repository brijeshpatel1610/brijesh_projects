/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Brijesh
 */
public class AdjNode {

    int nodeNumber;
    int distance;
    int targetHeuristicDist;

    public AdjNode(int nodeNumber, int distance) {
        this.nodeNumber = nodeNumber;
        this.distance = distance;
    }

    public AdjNode(int nodeNumber, int distance, int targetHeuristicDist) {
        this.nodeNumber = nodeNumber;
        this.distance = distance;
        this.targetHeuristicDist = targetHeuristicDist;
    }

    public int getNodeNumber() {
        return nodeNumber;
    }

    public void setNodeNumber(int nodeNumber) {
        this.nodeNumber = nodeNumber;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getTargetHeuristicDist() {
        return targetHeuristicDist;
    }

    public void setTargetHeuristicDist(int targetHeuristicDist) {
        this.targetHeuristicDist = targetHeuristicDist;
    }
    

}
