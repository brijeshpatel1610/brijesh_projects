
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Brijesh
 */
public class Node {

    int nodeNumber;
    int xCordinate;
    int yCordinate;
    ArrayList<AdjNode> adacentList = new ArrayList<AdjNode>();
    int targetHeuristicDist;
    int combineHeuristicDist;
    int predecessor = -1;
    int costToCurrentNode;
    boolean visited = false;

    public Node(int name, int xCordinate, int yCordinate) {
        this.nodeNumber = name;
        this.xCordinate = xCordinate;
        this.yCordinate = yCordinate;
    }

    public Node(int nodeNumber, int targetHeuristicDist) {
        this.nodeNumber = nodeNumber;
        this.targetHeuristicDist = targetHeuristicDist;
    }

    public Node(int nodeNumber, int xCordinate, int yCordinate, int targetHeuristicDist) {
        this.nodeNumber = nodeNumber;
        this.xCordinate = xCordinate;
        this.yCordinate = yCordinate;
        this.targetHeuristicDist = targetHeuristicDist;
    }

    public int getTargetHeuristicDist() {
        return targetHeuristicDist;
    }

    public void setTargetHeuristicDist(int targetHeuristicDist) {
        this.targetHeuristicDist = targetHeuristicDist;
    }

    public int getNodeNumber() {
        return nodeNumber;
    }

    public void setNodeNumber(int nodeNumber) {
        this.nodeNumber = nodeNumber;
    }

    public int getxCordinate() {
        return xCordinate;
    }

    public void setxCordinate(int xCordinate) {
        this.xCordinate = xCordinate;
    }

    public int getyCordinate() {
        return yCordinate;
    }

    public void setyCordinate(int yCordinate) {
        this.yCordinate = yCordinate;
    }

    public ArrayList<AdjNode> getAdacentList() {
        return adacentList;
    }

    public void setAdacentList(ArrayList<AdjNode> adacentList) {
        this.adacentList = adacentList;
    }

    public int getCombineHeuristicDist() {
        return combineHeuristicDist;
    }

    public void setCombineHeuristicDist(int combineHeuristicDist) {
        this.combineHeuristicDist = combineHeuristicDist;
    }

    public int getPredecessor() {
        return predecessor;
    }

    public void setPredecessor(int predecessor) {
        this.predecessor = predecessor;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public int getCostToCurrentNode() {
        return costToCurrentNode;
    }

    public void setCostToCurrentNode(int costToCurrentNode) {
        this.costToCurrentNode = costToCurrentNode;
    }

    
}
