
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Brijesh
 */
public class FWPatel {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        // TODO code application logic here
//        String fileName = args[0];
        Scanner sc = new Scanner(System.in);
        String fileName = "video.txt";
        String exit = null;
        do {
            FileInputStream fstream = new FileInputStream(fileName);
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

            //user input for startNode and targetNode
            System.out.print("Enter desired starting point: ");
            int startingPoint = sc.nextInt();
            System.out.print("Enter desired destination: ");
            int targetPoint = sc.nextInt();

            String strLine;
            int lines = 0;

            //Count the number of line in file
            while ((strLine = br.readLine()) != null) {
                // Print the content on the console
                lines++;
            }

            fstream.getChannel().position(0);

            Date d1 = new Date();
            if (lines == 2) {
                ArrayList<Integer> vertexList = new ArrayList<Integer>();
                // read the vertex name 
                strLine = br.readLine();
                String[] splited;
                splited = strLine.split("\\s+"); // split the line by space delimeter
                for (String splited1 : splited) {
                    int nodeNumber = Integer.parseInt(splited1.split(":")[0]);
                    vertexList.add(nodeNumber);
                }
                int vertexSize = vertexList.size() + 1;

                int[][] FWInitialgraph = initGraph(vertexSize);

                // read the edges between vertex 
                strLine = br.readLine();

                //Close the input stream
                br.close();
                splited = strLine.split("\\s+");
                for (String splited1 : splited) {
                    int fromNode = Integer.parseInt(splited1.split(":")[0].split(",")[0]);
                    int toNode = Integer.parseInt(splited1.split(":")[0].split(",")[1]);
                    int cost = Integer.parseInt(splited1.split(":")[1]);
                    //set toNode in adjcentNodeList of fromNode
                    addEdge(fromNode, toNode, cost, FWInitialgraph);

                }
                int[][] shortestDistances = Arrays.copyOf(FWInitialgraph, vertexSize);
                int[][] parentArray = initializeParentArray(vertexSize, FWInitialgraph);
                for (int k = 0; k < vertexSize; k++) {
                    for (int i = 0; i < vertexSize; i++) {
                        for (int j = 0; j < vertexSize; j++) {
                            if ((shortestDistances[i][k] + shortestDistances[k][j]) < shortestDistances[i][j]) {
                                shortestDistances[i][j] = shortestDistances[i][k] + shortestDistances[k][j];
                                parentArray[i][j] = parentArray[k][j];
                            }
                        }
                        if (shortestDistances[k][k] < 0) {
                            System.out.println("Negative Edge cycle in Graph");
                            printArray(shortestDistances,vertexSize);
                             System.exit(0);

                        }
                    }
                }

                Map<String, Integer> result = findTheShortestPathSequence(parentArray, vertexSize, startingPoint, targetPoint);
                if (result != null) {
                    System.out.println("For point " + startingPoint + " to point " + targetPoint);
                    System.out.print("The shortest path is " + result.entrySet().iterator().next().getKey());
                    System.out.println();
                    System.out.println("Number of points in path is " + result.entrySet().iterator().next().getValue());
                    System.out.println("Total cost is " + shortestDistances[startingPoint][targetPoint]);
                }
            } else {
                System.out.println("Invalid File Format");
                System.exit(0);
            }
            Date d2 = new Date();
            long grahamTime = d2.getTime() - d1.getTime();
            //System.out.println("Total Time: " + grahamTime);

            System.out.println();
            System.out.print("Would you like to search another best path ? (Y/N): ");
            exit = sc.next();
            System.out.println();

        } while (exit.equals(
                "Y") || exit.equals("y"));
    }

    public static int[][] initGraph(int numberOfVertex) {
        int[][] FWgraph = new int[numberOfVertex][numberOfVertex];
        for (int i = 0; i < numberOfVertex; i++) {
            for (int j = 0; j < numberOfVertex; j++) {
                if (i == j) {
                    FWgraph[i][j] = 0;
                } else {
                    FWgraph[i][j] = Integer.MAX_VALUE / 2;
                }
            }
        }
        return FWgraph;
    }

    // utility function to add edges to the adjacencyList
    public static void addEdge(int from, int to, int weight, int[][] FWgraph) {

        FWgraph[from][to] = weight;
        FWgraph[to][from] = weight;
    }

    public static int[][] initializeParentArray(int numberOfVertex, int[][] FWgraph) {
        int[][] parentArray = new int[numberOfVertex][numberOfVertex];
        for (int i = 0; i < numberOfVertex; i++) {
            for (int j = 0; j < numberOfVertex; j++) {
                if (i == j || FWgraph[i][j] == Integer.MAX_VALUE / 2) {
                    parentArray[i][j] = -1;
                } else {
                    parentArray[i][j] = i;
                }

            }
        }

        return parentArray;
    }

    public static Map<String, Integer> findTheShortestPathSequence(int next[][], int numberOfVertex, int startingPoint, int targetPoint) {
        HashMap<String, Integer> result = new HashMap<String, Integer>();
        String sequence = "";
        int initialTargetPoint = targetPoint;
        int[] pathArray = new int[numberOfVertex];
        int pathArrayIndex = 0;
        boolean pathExists = true;
        while (startingPoint != targetPoint) {
            try {
                if (next[startingPoint][targetPoint] == -1) {
                    System.out.println("Path is not exists between " + startingPoint + " and " + initialTargetPoint);
                    pathExists = false;
                    break;
                } else {
                    pathArray[pathArrayIndex] = next[startingPoint][targetPoint];
                    targetPoint = next[startingPoint][targetPoint];
                    pathArrayIndex++;
                }
            } catch (ArrayIndexOutOfBoundsException e) {
//                System.out.println(next[startingPoint][targetPoint]);
            }

        }
        if (pathExists) {
            for (int i = pathArrayIndex - 1; i > -1; i--) {
                sequence += " " + String.valueOf(pathArray[i]);
            }
            sequence = sequence + " " + initialTargetPoint;

            result.put(sequence, pathArrayIndex + 1);
            return result;
        } else {
            return null;
        }

    }

    public static void printArray(int[][] shortestDistances, int size) {

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                System.out.print(shortestDistances[i][j]+" ");
            }
            System.out.println();

        }

    }

}
