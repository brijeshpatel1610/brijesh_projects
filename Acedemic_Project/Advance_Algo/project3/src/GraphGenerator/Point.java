/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GraphGenerator;

/**
 *
 * @author Brijesh
 */
public class Point {

    int number;
    int x;
    int y;

    public Point(int number, int x, int y) {
        this.number = number;
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return number + ":" + x + "," + y;
    }

}
