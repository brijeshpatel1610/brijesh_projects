/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GraphGenerator;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 *
 * @author Brijesh
 */
public class GraphGenerator {

    public static void main(String args[]) {
        ArrayList<Point> pointList = new ArrayList<>();
        ArrayList<Edge> edgeList = new ArrayList<>();
        int temp = 1;
        int row = 0;
        int n = 140;
        for (int i = 1; i < n; i = i + 2) {
            for (int j = 1; j < n; j = j + 2) {
                pointList.add(new Point(temp, i, j));
                temp++;
            }
            row++;
        }
        System.out.println(row);
        for (int i = 0; i < (n * n / 4 - 1); i++) {
            int random1 = (int) (Math.random() * 50 + 1);
            int random = (int) (Math.random() * 50 + 1);
            edgeList.add(new Edge(pointList.get(i).number, pointList.get(i + 1).number, random));
            if (i < row * row - (int) Math.floor(n / 2)) {
                edgeList.add(new Edge(pointList.get(i).number, pointList.get(i + (int) Math.floor(n / 2)).number, random1));

            }

        }
        try {
            PrintWriter writer = new PrintWriter(temp + ".txt", "UTF-8");
            for (Point pointList1 : pointList) {
                writer.print(pointList1.toString()+" ");
            }
            writer.println();
            for (Edge edgeList1 : edgeList) {
                writer.print(edgeList1.toString()+" ");
            }
            writer.close();
        } catch (IOException e) {
            System.out.println("------------in exception-------");
            // do something
        }

    }

}
