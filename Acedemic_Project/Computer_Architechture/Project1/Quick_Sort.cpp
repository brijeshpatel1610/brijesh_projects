#include <iostream>
#include <sstream>
#include <time.h>
#include <cstdlib>
#include <omp.h>
using namespace std;

void quickSort(int arr[], int low_index, int high_index);

// create an array of length size of random numbers
// returns a pointer to the array

// seed: seeds the random number generator
int * randNumArray(const int size, const int seed) {
	srand(seed);
	int * array = new int[size];
	for (int i = 0; i < size; i++) {
		array[i] = rand() % 1000000;
	}
	return array;
}
int main(int argc, char** argv) {
	int * array; // the poitner to the array of rands
	int size, seed; // values for the size of the array
					// and the seed for generating
					// random numbers
					// check the command line args
	
	if (argc < 3) {
		cerr << "usage: "
			<< argv[0]
			<< " [amount of random nums to generate] [seed value for rand]"
			<< endl;
		exit(-1);
	}else{
		// convert cstrings to ints
		{
			stringstream ss1(argv[1]);
			ss1 >> size;
		}
		{
			stringstream ss1(argv[2]);
			ss1 >> seed;
		}
	}
	// get the random numbers
	array = randNumArray(size, seed);
	clock_t start = clock();
	cout << "The Array Now Sorted\n";
	quickSort(array,0,size-1);
	cout<<"Elapsed Time : \n"<< ((double)clock() - start) / CLOCKS_PER_SEC<<endl;
	
}

void quickSort(int arr[], int left, int right) {
      int i = left, j = right;
      int tmp;
      int pivot = arr[(left + right) / 2];
 
      /* partition */
      while (i <= j) {
            while (arr[i] < pivot)
                  i++;
            while (arr[j] > pivot)
                  j--;
            if (i <= j) {
                  tmp = arr[i];
                  arr[i] = arr[j];
                  arr[j] = tmp;
                  i++;
                  j--;
            }
      };
 
      /* recursion */
      if (left < j)
            quickSort(arr, left, j);
      if (i < right)
            quickSort(arr, i, right);
}