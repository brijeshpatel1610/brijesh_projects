Brijesh Patel - 800597576

1) I have put source code of all the sort algo in the following path on home.cs.siue.edu server.
       home/brpatel/Project/executeFiles

2) So go to the above path and then run the following command for source code compilation
       make -f makefile.txt

3) Now you can run any of the program as below.
      ./bsp 500 50
	  ./msp 500 50
	  ./qsp 500 50
	  ./bss 500 500
	  ./qss 500 500
	  ./mss 500 50
	  ./reference 500 50