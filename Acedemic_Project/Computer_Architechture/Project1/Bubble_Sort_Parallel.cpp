#include <iostream>
#include <sstream>
#include <time.h>
#include <cstdlib>
#include <omp.h>
using namespace std;

void bubbleSortParallel(int a[], int size);

// create an array of length size of random numbers
// returns a pointer to the array

// seed: seeds the random number generator
int * randNumArray(const int size, const int seed) {
	srand(seed);
	int * array = new int[size];
	for (int i = 0; i < size; i++) {
		array[i] = rand() % 1000000;
	}
	return array;
}
int main(int argc, char** argv) {
	int * array; // the poitner to the array of rands
	int size, seed; // values for the size of the array
					// and the seed for generating
					// random numbers
					// check the command line args
	
	if (argc < 3) {
		cerr << "usage: "
			<< argv[0]
			<< " [amount of random nums to generate] [seed value for rand]"
			<< endl;
		exit(-1);
	}else{
		// convert cstrings to ints
		{
			stringstream ss1(argv[1]);
			ss1 >> size;
		}
		{
			stringstream ss1(argv[2]);
			ss1 >> seed;
		}
	}
	// get the random numbers
	array = randNumArray(size, seed);
//	int * temp = new int[size];
	int max_threads = omp_get_max_threads(); // Max available threads
	cout << "max_threads----" << max_threads << endl;
	clock_t start = clock();
	omp_set_nested(1);
	omp_set_num_threads( max_threads );
	bubbleSortParallel(array, size);
	cout<<"Elapsed Time : \n"<< ((double)clock() - start) / CLOCKS_PER_SEC<<endl;
	
}


void bubbleSortParallel(int a[], int size)
{
	#pragma omp parallel
	{
		#pragma omp parallel 
		{
			for (int j = 0; j < size - 1; j += 2)
			{
				if (a[j] > a[j + 1])
				{
					swap(a[j], a[j + 1]);
				}
			}
		}

		#pragma omp parallel 
		{
			for (int j = 1; j < size - 1; j += 2)
			{
				if (a[j] > a[j + 1])
				{
					swap(a[j], a[j + 1]);
				}
			}
		}
	}
}

