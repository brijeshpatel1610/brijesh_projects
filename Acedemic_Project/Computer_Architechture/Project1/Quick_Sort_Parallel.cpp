#include <iostream>
#include <sstream>
#include <time.h>
#include <cstdlib>
#include <omp.h>
using namespace std;

int partition(int arr[], int low_index, int high_index);
void quicksort(int arr[], int low_index, int high_index);

// create an array of length size of random numbers
// returns a pointer to the array

// seed: seeds the random number generator
int * randNumArray(const int size, const int seed) {
	srand(seed);
	int * array = new int[size];
	for (int i = 0; i < size; i++) {
		array[i] = rand() % 1000000;
	}
	return array;
}
int main(int argc, char** argv) {
	int * array; // the poitner to the array of rands
	int size, seed; // values for the size of the array
					// and the seed for generating
					// random numbers
					// check the command line args
	
	if (argc < 3) {
		cerr << "usage: "
			<< argv[0]
			<< " [amount of random nums to generate] [seed value for rand]"
			<< endl;
		exit(-1);
	}else{
		// convert cstrings to ints
		{
			stringstream ss1(argv[1]);
			ss1 >> size;
		}
		{
			stringstream ss1(argv[2]);
			ss1 >> seed;
		}
	}
	// get the random numbers
	array = randNumArray(size, seed);
	//int * temp = new int[size];
	int max_threads = omp_get_max_threads(); // Max available threads
	cout << "max_threads----" << max_threads << endl;
	clock_t start = clock();
	omp_set_nested(1);
	omp_set_num_threads( max_threads );
	
	quicksort(array, 0, size-1);
	
	cout<<"Elapsed Time : \n"<< ((double)clock() - start) / CLOCKS_PER_SEC<<endl;
	
}

int partition(int arr[], int low_index, int high_index)
{
	int i, j, temp, key;
	key = arr[low_index];
	i = low_index + 1;
	j = high_index;
	while (1)
	{
		while (i < high_index && key >= arr[i])
			i++;
		while (key < arr[j])
			j--;
		if (i < j)
		{
			temp = arr[i];
			arr[i] = arr[j];
			arr[j] = temp;
		}
		else
		{
			temp = arr[low_index];
			arr[low_index] = arr[j];
			arr[j] = temp;
			return(j);
		}
	}
}


void quicksort(int arr[], int low_index, int high_index)
{
	int j;

	if (low_index < high_index)
	{
		j = partition(arr, low_index, high_index);
	
			#pragma omp parallel sections
			{
			#pragma omp section
			{
				quicksort(arr, low_index, j - 1);
			}

			#pragma omp section
			{
				quicksort(arr, j + 1, high_index);
			}

		}
	}
}