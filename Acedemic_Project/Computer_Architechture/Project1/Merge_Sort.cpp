#include <iostream>
#include <cstring>
#include <sstream>
#include <time.h>
#include <cstdlib>
#include <omp.h>
using namespace std;

void merge(int a[], int size, int temp[]);
void mergesort_serial(int a[], int size, int temp[]);
// create an array of length size of random numbers
// returns a pointer to the array

// seed: seeds the random number generator
int * randNumArray(const int size, const int seed) {
	srand(seed);
	int * array = new int[size];
	for (int i = 0; i < size; i++) {
		array[i] = rand() % 1000000;
	}
	return array;
}
int main(int argc, char** argv) {
	int * array; // the poitner to the array of rands
	int size, seed; // values for the size of the array
					// and the seed for generating
					// random numbers
					// check the command line args
	
	if (argc < 3) {
		cerr << "usage: "
			<< argv[0]
			<< " [amount of random nums to generate] [seed value for rand]"
			<< endl;
		exit(-1);
	}else{
		// convert cstrings to ints
		{
			stringstream ss1(argv[1]);
			ss1 >> size;
		}
		{
			stringstream ss1(argv[2]);
			ss1 >> seed;
		}
	}
	// get the random numbers
	array = randNumArray(size, seed);
	int * temp = new int[size];
	clock_t start = clock();
	mergesort_serial(array, size, temp);
	
	cout<<"Elapsed Time : \n"<< ((double)clock() - start) / CLOCKS_PER_SEC<<endl;
	
}

void mergesort_serial(int a[], int size, int temp[]) {
	if (size < 2) return;
	mergesort_serial(a, size / 2, temp);
	mergesort_serial(a + size / 2, size - size / 2, temp);
	// The above call will not work properly in an OpenMP program
	// Merge the two sorted subarrays into a temp array
	merge(a, size, temp);
}

void merge(int a[], int size, int temp[]) {
	int i1 = 0;
	int i2 = size / 2;
	int tempi = 0;
	while (i1 < size / 2 && i2 < size) {
		if (a[i1] < a[i2]) {
			temp[tempi] = a[i1];
			i1++;
		}
		else {
			temp[tempi] = a[i2];
			i2++;
		}
		tempi++;
	}
	while (i1 < size / 2) {
		temp[tempi] = a[i1];
		i1++;
		tempi++;
	}
	while (i2 < size) {
		temp[tempi] = a[i2];
		i2++;
		tempi++;
	}
	// Copy sorted temp array into main array, a
	memcpy(a, temp, size * sizeof(int));
}
