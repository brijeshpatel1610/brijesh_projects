#include <iostream>
#include <sstream>
#include <time.h>
#include <cstdlib>
#include <omp.h>
#include <cstring>
using namespace std;

void merge(int a[], int size, int temp[]);
void mergesort_parallel_omp(int a[], int size, int temp[],int threads);
void mergesort_serial(int a[], int size, int temp[]);
// create an array of length size of random numbers
// returns a pointer to the array

// seed: seeds the random number generator
int * randNumArray(const int size, const int seed) {
	srand(seed);
	int * array = new int[size];
	for (int i = 0; i < size; i++) {
		array[i] = rand() % 1000000;
	}
	return array;
}
int main(int argc, char** argv) {
	int * array; // the poitner to the array of rands
	int size, seed; // values for the size of the array
					// and the seed for generating
					// random numbers
					// check the command line args
	
	if (argc < 3) {
		cerr << "usage: "
			<< argv[0]
			<< " [amount of random nums to generate] [seed value for rand]"
			<< endl;
		exit(-1);
	}else{
		// convert cstrings to ints
		{
			stringstream ss1(argv[1]);
			ss1 >> size;
		}
		{
			stringstream ss1(argv[2]);
			ss1 >> seed;
		}
	}
	// get the random numbers
	array = randNumArray(size, seed);
	
	int * temp = new int[size];
	int max_threads = omp_get_max_threads(); // Max available threads
	cout << "max_threads----" << max_threads << endl;
	clock_t start = clock();
	omp_set_nested(1);
	omp_set_num_threads( max_threads );

	// Parallel mergesort
	mergesort_parallel_omp(array, size, temp, max_threads);	
	
	cout<<"Elapsed Time : \n"<< ((double)clock() - start) / CLOCKS_PER_SEC<<endl;
	
}

// OpenMP merge sort with given number of threads
void mergesort_parallel_omp(int a[], int size, int temp[], int threads) {
	if (threads == 1) {
		//        printf("Thread %d begins serial merge sort\n", omp_get_thread_num());
		mergesort_serial(a, size, temp);
	}
	else if (threads > 1) {
		//#pragma omp parallel sections num_threads(2)
		{
		
		//#pragma omp section
			{ 
                mergesort_parallel_omp(a, size / 2, temp, threads / 2);
			}
		//#pragma omp section
			{ 

				mergesort_parallel_omp(a + size / 2, size - size / 2, temp + size / 2, threads - threads / 2); }
			}
		// Thread allocation is implementation dependent
		// Some threads can execute multiple sections while others are idle 
		// Merge the two sorted sub-arrays through temp
		merge(a, size, temp);
		}
	else {
		cout<<"Error: %d threads\n"<<threads;
		return;
	}
}

void mergesort_serial(int a[], int size, int temp[]) {
	if (size < 2) return;
	mergesort_serial(a, size / 2, temp);
	mergesort_serial(a + size / 2, size - size / 2, temp);
	// The above call will not work properly in an OpenMP program
	// Merge the two sorted subarrays into a temp array
	merge(a, size, temp);
}

void merge(int a[], int size, int temp[]) {
	int i1 = 0;
	int i2 = size / 2;
	int tempi = 0;
	while (i1 < size / 2 && i2 < size) {
		if (a[i1] < a[i2]) {
			temp[tempi] = a[i1];
			i1++;
		}
		else {
			temp[tempi] = a[i2];
			i2++;
		}
		tempi++;
	}
	while (i1 < size / 2) {
		temp[tempi] = a[i1];
		i1++;
		tempi++;
	}
	while (i2 < size) {
		temp[tempi] = a[i2];
		i2++;
		tempi++;
	}
	// Copy sorted temp array into main array, a
	memcpy(a, temp, size * sizeof(int));
}

