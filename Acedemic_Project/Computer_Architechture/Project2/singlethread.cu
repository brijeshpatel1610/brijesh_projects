
/*---------------------------------------------------------------------------*/
/*   Name = Brijesh Patel                                                    */
/*   StudentId = 800597576                                                   */
/*   emailId = brpatel@siue.edu                                              */
/*   Program Name = Single Thread Sorting on cuda                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <limits>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <thrust/sort.h>
using namespace std;

#define CUDA_CHECK_ERROR
#define CudaSafeCall( err ) __cudaSafeCall( err, __FILE__, __LINE__ )
#define CudaCheckError() __cudaCheckError( __FILE__, __LINE__ )
inline void __cudaSafeCall(cudaError err,
	const char *file, const int line)
{
#ifdef CUDA_CHECK_ERROR
#pragma warning( push )
#pragma warning( disable: 4127 ) 

	do
	{
		if (cudaSuccess != err)
		{
			fprintf(stderr,
				"cudaSafeCall() failed at %s:%i : %s\n",
				file, line, cudaGetErrorString(err));
			exit(-1);
		}
	} while (0);
#pragma warning( pop )
#endif 
	return;
}

inline void __cudaCheckError(const char *file, const int line)
{
#ifdef CUDA_CHECK_ERROR
#pragma warning( push )
#pragma warning( disable: 4127 )

	do
	{
		cudaError_t err = cudaGetLastError();
		if (cudaSuccess != err)
		{
			fprintf(stderr,
				"cudaCheckError() failed at %s:%i : %s.\n",
				file, line, cudaGetErrorString(err));
			exit(-1);
		}

		err = cudaThreadSynchronize();
		if (cudaSuccess != err)
		{
			fprintf(stderr,
				"cudaCheckError() with sync failed at %s:%i : %s.\n",
				file, line, cudaGetErrorString(err));
			exit(-1);
		}
	} while (0);
#pragma warning( pop )
#endif 
	return;
}

int * makeRandArray(const int size, const int seed) {
	srand(seed);
	int * array = new int[size];
	for (int i = 0; i < size; i++) {
		array[i] = std::rand() % 1000000;
	}

	return array;
}

__global__ void swapOnKernel(int *array, int size)
{
	int i, j, swap;
 
	for (i = 0 ; i < ( size - 1 ); i++)
	{
    for (j = 0 ; j < size - i - 1; j++)
    {
      if (array[j] > array[j+1]) /* For decreasing order use < */
      {
        swap       = array[j];
        array[j]   = array[j+1];
        array[j+1] = swap;
      }
    }
  }
 


}

int main(int argc, char* argv[])
{
	int * array;
	int size, seed;

	if (argc < 3) {
		std::cerr << "usage: "
			<< argv[0]
			<< " [amount of random nums to generate] [seed value for rand]"
			<< " [1 to print sorted array, 0 otherwise]"
			<< std::endl;
		exit(-1);
	}

	{
		std::stringstream ss1(argv[1]);
		ss1 >> size;
	}
	{
		std::stringstream ss1(argv[2]);
		ss1 >> seed;
	}
	
	array = makeRandArray(size, seed);
	cudaEvent_t startTotal, stopTotal;
	float timeTotal;
	cudaEventCreate(&startTotal);
	cudaEventCreate(&stopTotal);
	cudaEventRecord(startTotal, 0);

	// Step 1:  Create an array on the GPU to hold the numbers that we
	// will do the sum3 computation on
	int *device_array;
	cudaMalloc((void **)&device_array, size * sizeof(int));

	// Step 2: Copy the data to the device array
	cudaMemcpy(device_array, &(array[0]), size * sizeof(int), cudaMemcpyHostToDevice);
	
	swapOnKernel << < 1, 1 >> >(device_array, size);

	cudaMemcpy(array, device_array, size * sizeof(int), cudaMemcpyDeviceToHost);
	cudaEventRecord(stopTotal, 0);
	cudaEventSynchronize(stopTotal);
	cudaEventElapsedTime(&timeTotal, startTotal, stopTotal);
	cudaEventDestroy(startTotal);
	cudaEventDestroy(stopTotal);

	std::cerr << "Total time in seconds: "
		<< timeTotal / 1000.0 << std::endl;
	

}