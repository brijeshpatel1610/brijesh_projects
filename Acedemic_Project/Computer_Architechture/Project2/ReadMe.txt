Brijesh Patel - 800597576

1) I have put source code of all 3 cuda sort code in the following path on jalapeno.cs.siue.edu server.
       home/brpatel/CudaProject

2) So go to the above path and then run the following command for source code compilation
       make -f makefile.txt
     
	(Note: When you run above line it will complile all 3 executable file with below warning
	nvcc warning : The 'compute_20', 'sm_20', and 'sm_21' architectures are deprecated, and 
	may be removed in a future release (Use -Wno-deprecated-gpu-targets to suppress warning)).

	   
3) Now you can run any of the program as below.
      ./thrust 500 50
	  ./singlethread 500 50
	  ./multithread 500 50