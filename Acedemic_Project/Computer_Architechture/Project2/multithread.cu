
/*---------------------------------------------------------------------------*/
/*   Name = Brijesh Patel                                                    */
/*   StudentId = 800597576                                                   */
/*   emailId = brpatel@siue.edu                                              */
/*   Program Name = Multi Thread Sorting on cuda                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/



#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <limits>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <thrust/sort.h>
using namespace std;

#define CUDA_CHECK_ERROR
#define CudaSafeCall( err ) __cudaSafeCall( err, __FILE__, __LINE__ )
#define CudaCheckError() __cudaCheckError( __FILE__, __LINE__ )
inline void __cudaSafeCall(cudaError err,
	const char *file, const int line)
{
#ifdef CUDA_CHECK_ERROR
#pragma warning( push )
#pragma warning( disable: 4127 ) 

	do
	{
		if (cudaSuccess != err)
		{
			fprintf(stderr,
				"cudaSafeCall() failed at %s:%i : %s\n",
				file, line, cudaGetErrorString(err));
			exit(-1);
		}
	} while (0);
#pragma warning( pop )
#endif 
	return;
}

inline void __cudaCheckError(const char *file, const int line)
{
#ifdef CUDA_CHECK_ERROR
#pragma warning( push )
#pragma warning( disable: 4127 )

	do
	{
		cudaError_t err = cudaGetLastError();
		if (cudaSuccess != err)
		{
			fprintf(stderr,
				"cudaCheckError() failed at %s:%i : %s.\n",
				file, line, cudaGetErrorString(err));
			exit(-1);
		}

		err = cudaThreadSynchronize();
		if (cudaSuccess != err)
		{
			fprintf(stderr,
				"cudaCheckError() with sync failed at %s:%i : %s.\n",
				file, line, cudaGetErrorString(err));
			exit(-1);
		}
	} while (0);
#pragma warning( pop )
#endif 
	return;
}

int * makeRandArray(const int size, const int seed) {
	srand(seed);
	int * array = new int[size];
	for (int i = 0; i < size; i++) {
		array[i] = std::rand() % 1000000;
	}
	return array;
}

__global__ void bucketSort(int *array, int *resultArray, int size)
{
	int aIndex = (blockDim.x * blockIdx.x) + threadIdx.x;
	if(aIndex<size){
		int numberOfLarggerElement = 0;
		for (int j = 0; j < size; j++) {
			if (array[aIndex] < array[j]) {
				numberOfLarggerElement++;
			}
		__syncthreads( ) ;
		}
		resultArray[size - numberOfLarggerElement - 1] = array[aIndex];
	}
}

int main(int argc, char* argv[])
{
	int * array;
	int size, seed;

	if (argc < 3) {
		std::cerr << "usage: "
			<< argv[0]
			<< " [amount of random nums to generate] [seed value for rand]"
			<< " [1 to print sorted array, 0 otherwise]"
			<< std::endl;
		exit(-1);
	}

	{
		std::stringstream ss1(argv[1]);
		ss1 >> size;
	}
	{
		std::stringstream ss1(argv[2]);
		ss1 >> seed;
	}
	
	array = makeRandArray(size, seed);
	cudaEvent_t startTotal, stopTotal;
	float timeTotal;
	cudaEventCreate(&startTotal);
	cudaEventCreate(&stopTotal);
	cudaEventRecord(startTotal, 0);

	// Create an array on the GPU to hold the initial numbers
	int *device_array;
	cudaMalloc((void **)&device_array, size * sizeof(int));

	// Create an array on the GPU to hold the sorted results numbers 
	int *sorted_array;
	cudaMalloc((void **)&sorted_array, size * sizeof(int));

	// Copy the data to the device array
	cudaMemcpy(device_array, &(array[0]), size * sizeof(int), cudaMemcpyHostToDevice);
	//dim3 threadsPerBlock(16, 32);
	//dim3 numBlocks((size + threadsPerBlock.x - 1) / threadsPerBlock.x,
	//	(size + threadsPerBlock.y - 1) / threadsPerBlock.y);

	//std::cerr << "data size: " << size << std::endl;
	//std::cerr << "block sizes: " << (size + threadsPerBlock.x - 1) / threadsPerBlock.x
	//	<< ", " << (size + threadsPerBlock.y - 1) / threadsPerBlock.y << std::endl;

	bucketSort << < (size/1024)+1, 1024 >> > (device_array, sorted_array, size);

	cudaMemcpy(array, sorted_array, size * sizeof(int), cudaMemcpyDeviceToHost);
	cudaEventRecord(stopTotal, 0);
	cudaEventSynchronize(stopTotal);
	cudaEventElapsedTime(&timeTotal, startTotal, stopTotal);
	cudaEventDestroy(startTotal);
	cudaEventDestroy(stopTotal);

	std::cerr << "Total time in seconds: "
		<< timeTotal / 1000.0 << std::endl;
	
}