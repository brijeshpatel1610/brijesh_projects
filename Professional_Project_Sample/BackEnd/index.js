var service = require('./priceRange-service'), conversionParmas = require('../middlewares/conversion-params');

module.exports = function(app) {
	// Define routes related to price range reports here

	// To get price range reports of products
	app.get('/priceRange/products', service.products);

	// To get a price range report of details of a product
	app.get('/priceRange/products/:productId', conversionParmas('ALL'), service.productDetails);
};