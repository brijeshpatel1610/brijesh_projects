var moment = require('moment');
var util = require('util');
var _ = require('lodash');
var q = require('q');
var sequelize = require('sequelize');
var config = require('../config');
var mysql = require('../utils/mysql-pool');
var logger = require('../utils/logger');
var constants = require('../utils/constants');
var manufacturerProductMonthlySummary = require('../models/manufacturerProductMonthlySummary');
var distributorPriceMonthlySummary = require('../models/distributorPriceMonthlySummary');
var manufacturerProduct = require('../models/manufacturerProduct');
var brand = require('../models/brand');
var company = require('../models/company');
var manufacturerProductMonthlySummaryCustomerGroups = require('../models/manufacturerProductMonthlySummaryCustomerGroups');

/*
 * ?startDateLT ?startDateGTE ?idType ?id ?LIMIT ?OFFSET
 */
var query = "SELECT dataRange as 'range', high, low, average as 'avg', id, productDescription as 'name' FROM (SELECT ((MAX(high) -  MIN(high)) * 100 ) / MIN(high) AS dataRange," + " MAX(high) AS high, MIN(high) AS low, id, AVG(high) AS average, productDescription FROM (SELECT MAX(`latestPrice`) AS high, " + "MIN(`latestPrice`) AS low, `product`.`id` AS id ,`latestPrice`, `product`.`description` AS productDescription FROM " + "`manufacturer_product_monthly_summary` AS `manufacturer_product_monthly_summary` LEFT OUTER JOIN " + "`distributor_price_monthly_summary` AS `distributor` ON `manufacturer_product_monthly_summary`.`id` = " + "`distributor`.`OWNER_ID` LEFT OUTER JOIN `manufacturer_product` AS `product` ON " + "`manufacturer_product_monthly_summary`.`PRODUCT_ID` = `product`.`id` WHERE (`manufacturer_product_monthly_summary`.`startDate` " + "< '?startDateLT' AND `manufacturer_product_monthly_summary`.`startDate` >= '?startDateGTE') AND " + "`manufacturer_product_monthly_summary`.`?idType` = ?id GROUP BY `distributor`.`DISTRIBUTOR_ID`, id) AS " + "secondInner group by id) AS firstInner WHERE dataRange > 0 and dataRange < 100 order by dataRange DESC LIMIT ?LIMIT OFFSET ?OFFSET";
/*
 * ?startDateLT ?startDateGTE ?id ?LIMIT ?OFFSET
 */
var query_customer_group = "SELECT dataRange as 'range', high, low, average as 'avg', id, productDescription as 'name' FROM (SELECT ((MAX(high) -  MIN(high)) * 100 ) / MIN(high) AS dataRange," + " MAX(high) AS high, MIN(high) AS low, id, AVG(high) AS average, productDescription FROM (SELECT MAX(`latestPrice`) AS high, " + "MIN(`latestPrice`) AS low, `product`.`id` AS id ,`latestPrice`, `product`.`description` AS productDescription FROM " + "`manufacturer_product_monthly_summary` AS `manufacturer_product_monthly_summary` LEFT OUTER JOIN " + "`distributor_price_monthly_summary` AS `distributor` ON `manufacturer_product_monthly_summary`.`id` = " + "`distributor`.`OWNER_ID` LEFT OUTER JOIN `manufacturer_product` AS `product` ON " + "`manufacturer_product_monthly_summary`.`PRODUCT_ID` = `product`.`id` LEFT OUTER JOIN `manufacturer_product_monthly_summary_customer_groups` AS `customer_group` ON `customer_group`.`MANUFACTURERPRODUCTMONTHLYSUMMARY_ID` = `manufacturer_product_monthly_summary`.`id` WHERE (`manufacturer_product_monthly_summary`.`startDate` " + "< '?startDateLT' AND `manufacturer_product_monthly_summary`.`startDate` >= '?startDateGTE') AND " + "`customer_group`.`CUSTOMERGROUPS_ID` = ?id GROUP BY `distributor`.`DISTRIBUTOR_ID`, id) AS " + "secondInner group by id) AS firstInner WHERE dataRange > 0 and dataRange < 100 order by dataRange DESC LIMIT ?LIMIT OFFSET ?OFFSET";

var priceRangeModel = function() {};

/**
 * To get price range reports of products
 * 
 * @param companyId
 * @param data
 * @returns {object}
 */
priceRangeModel.products = function(user, data) {
	var deffered = q.defer();
	logger.info(util.format('priceRangeModel.products request user:- %j data:- %j', user, data));

	findProductData(user, data).then(function(result) {
		deffered.resolve(result);
	}, function(error) {
		deffered.reject(error);
		logger.error(util.format('priceRangeModel.products error occured while fetching data for products:-  %s', error.stack));
	});

	return deffered.promise;
};

priceRangeModel.productDetails = function(user, data, params) {
	var deffered = q.defer();
	logger.info(util.format('priceRangeModel.productDetails request user:- %j data:- %j params:- %j', user, data, params));

	findProductDetailsData(user, data, params).then(function(result) {
		deffered.resolve(result);
	}, function(error) {
		deffered.reject(error);
		logger.error(util.format('priceRangeModel.productDetails error occured while fetching data for products:-  %s', error.stack));
	});

	return deffered.promise;
};

/**
 * To get a price range report of details of a product
 * 
 * @param companyId
 * @param data
 * @returns {object}
 */
function findProductData(user, data) {
	var deffered = q.defer();

	var rawQuery;
	if (constants.companyTypes[user.companyType] === 'CustomerGroup') {
		rawQuery = query_customer_group;
	} else {
		rawQuery = query;
	}
	rawQuery = rawQuery.replace('?startDateLT', moment(constants.codes[data.code].lt).tz(config.get('timezone')).format('YYYY-MM-DD HH:mm:ss'));
	rawQuery = rawQuery.replace('?startDateGTE', moment(constants.codes[data.code].gte).tz(config.get('timezone')).format('YYYY-MM-DD HH:mm:ss'));

	if (constants.companyTypes[user.companyType] === 'CustomerAccount')
		rawQuery = rawQuery.replace('?idType', 'CUSTOMERACCOUNT_ID');

	else if (constants.companyTypes[user.companyType] === 'CustomerLocation')
		rawQuery = rawQuery.replace('?idType', 'CUSTOMERLOCATION_ID');

	rawQuery = rawQuery.replace('?id', user.companyId);
	rawQuery = rawQuery.replace('?LIMIT', 100);
	rawQuery = rawQuery.replace('?OFFSET', data.page !== undefined && parseInt(data.page) > 0 ? (parseInt(data.page) - 1) * 100 : 0);

	mysql.query(rawQuery, {
		type : sequelize.QueryTypes.SELECT
	}).then(function(productRecord) {
		logger.info(util.format('priceRangeModel.products productRecord.length:- %d', productRecord.length));
		deffered.resolve(productRecord);
	}, function(error) {
		deffered.reject(error);
		logger.error(util.format('priceRangeModel.products error occured while fetching data for products:-  %s', error.stack));
	});

	return deffered.promise;
}

function findProductDetailsData(user, data, params) {
	var deffered = q.defer();

	var where = {
		PRODUCT_ID : params.productId,
		startDate : {
			$lt : constants.codes[data.code].lt,
			$gte : constants.codes[data.code].gte
		}
	};

	var include = [];
	include.push({
		model : manufacturerProduct,
		as : 'product',
		attributes : [ 'id', 'mpc', 'pack', 'PRODUCTBRAND_ID', 'description' ],
		include : [ {
			model : brand,
			as : 'brand',
			attributes : [ 'name' ]
		} ]
	});
	include.push({
		model : distributorPriceMonthlySummary,
		as : 'distributor',
		attributes : [ 'id', [ sequelize.fn('MAX', sequelize.col('latestPrice')), 'latestPrice' ] ],
		include : [ {
			model : company,
			as : 'company',
			attributes : [ 'id', 'name' ]
		} ]
	});

	if (constants.companyTypes[user.companyType] === 'CustomerAccount')
		where.CUSTOMERACCOUNT_ID = user.companyId;

	else if (constants.companyTypes[user.companyType] === 'CustomerLocation')
		where.CUSTOMERLOCATION_ID = user.companyId;

	else if (constants.companyTypes[user.companyType] === 'CustomerGroup')
		include.push({
			model : manufacturerProductMonthlySummaryCustomerGroups,
			as : 'manufacturerProductMonthlySummaryCustomerGroups',
			where : {
				CUSTOMERGROUPS_ID : user.companyId
			},
			atttributes : []
		});

	manufacturerProductMonthlySummary.findAll({
		where : where,
		include : include,
		attributes : [ 'id' ],
		group : [ 'distributor.DISTRIBUTOR_ID' ],
	}).then(function(productRecord) {
		// logger.info(util.format('priceRangeModel.productDetails productRecord:- %j', productRecord));
		logger.info(util.format('priceRangeModel.productDetails productRecord.length:- %d', productRecord.length));

		var productSummary = {};
		// check if there is record for the product in summary and manufacturerProduct tables
		if (productRecord.length > 0 && productRecord[0].product !== null) {

			productSummary.id = productRecord[0].id;
			productSummary.name = productRecord[0].product.description;
			productSummary.mfg = productRecord[0].product.PRODUCTBRAND_ID;
			productSummary.mpc = productRecord[0].product.mpc;
			productSummary.pack = productRecord[0].product.pack;
			productSummary.brand = productRecord[0].product.brand ? productRecord[0].product.brand.name : '';

			productSummary.distributors = [];

			for (var index = 0; index < productRecord.length; index++)
				if (productRecord[index].distributor !== null && productRecord[index].distributor.company !== null)
					productSummary.distributors.push({
						'name' : productRecord[index].distributor.company.name,
						// fetch highest latest price (as per 02.19.16 - after call conversation)
						'price' : productRecord[index].distributor.latestPrice,
					});
		}
		
		logger.info(util.format('priceRangeModel.productDetails productSummary.distributors:- %j', productSummary.distributors));
		productSummary.distributors = productSummary.distributors.sort(function(a, b) {
			return b.price - a.price
		});
		logger.info(util.format('priceRangeModel.productDetails productSummary.distributors:- %j', productSummary.distributors));
		deffered.resolve(productSummary);
	}, function(error) {
		deffered.reject(error);
		logger.error(util.format('priceRangeModel.productDetails error occured while fetching details for product:-  %s', error.stack));
	});

	return deffered.promise;
}

module.exports = priceRangeModel;