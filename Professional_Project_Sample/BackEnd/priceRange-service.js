var priceRange = require('./priceRange-model'), schemas = require('../utils/schemas');

// To get price range reports of products
var products = function (req, res) {
    // validate the request
    if (schemas.validate(req.query, schemas.priceRangeReportForProductsRequest)) {
        priceRange.products(req.user, req.query).then(function (data) {
            req.headers['response-data-length'] = data.length;
            return res.status(200).send({
                code: 200,
                data: data
            });
        }, function (error) {
            return res.status(500).send({
                code: 500,
                data: {}
            });
        });
    } else {
        // invalid schema
        return res.status(400).send({
            code: 400,
            data: {}
        });
    }
};

// To get a price range report of details of a product
var productDetails = function (req, res) {
    // validate the request
    if (schemas.validate(req.query, schemas.priceRangeReportForProductDetailsRequest)) {
        priceRange.productDetails(req.user, req.query, req.params).then(function (data) {
            req.headers['response-data-length'] = data.length;
            return res.status(200).send({
                code: 200,
                data: data
            });
        }, function (error) {
            return res.status(500).send({
                code: 500,
                data: {}
            });
        });
    } else {
        // invalid schema
        return res.status(400).send({
            code: 400,
            data: {}
        });
    }
};
module.exports = {
    products: products,
    productDetails: productDetails
};