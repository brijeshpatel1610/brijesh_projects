"use-strict";

var dashboardApp = angular.module('dashboardApp', []);

/**
 * controller to populate the screen of dasboard
 * method to call the service method to get the screen detail
 */
dashboardApp.controller('dashboardController', ['$scope', '$location', '$ionicSideMenuDelegate', '$cordovaInAppBrowser', 'userService', 'dashboardService', function ($scope, $location, $ionicSideMenuDelegate, $cordovaInAppBrowser, userService, dashboardService) {
        
//        get the user detail by calling the service method
        $scope.user = userService.getUser();
//        Note :- we have commented the below code as we have to hide the weekly report tab
        $scope.dashboard = {currentTab: 'saving'};

        $scope.dashboardTypeList = [
            {text: 'Last Year Savings', value: 'LastYearSavings'},
            {text: 'Last Month Savings', value: 'LastMonthSavings'},
            {text: 'YTD Savings', value: 'YTD'},
            {text: 'This Month Savings', value: 'ThisMonthSavings'}
        ];

        $scope.dashboardDetail = {type: 'YTD', detail: {}};
        
        //API call to get the dashboard detail by calling API
        var _callApiGetDashboardList = function () {
            dashboardService.getDashboardDetail($scope.dashboardDetail.type).then(function (response) {
                $scope.dashboardDetail.detail = angular.copy(response);
                $scope.dashboardDetail.detail.totalSavings = _.sum(_.values(_.omit(response, ['invoices', 'purchases'])));
                dashboardService.setDashboardData($scope.dashboardDetail);
                $scope.$broadcast('scroll.refreshComplete');
            }, function (error) {
                $scope.$broadcast('scroll.refreshComplete');
            });
        };
        
        //API call to get the weekly report by calling API
        var _callApiGetWeeklyReport = function(){
            dashboardService.getWeeklyFreshReport().then(function (response) {
                $scope.dashboardDetail.weeklyFreshReport = angular.copy(response);
                var i = 0;
                _.forEach($scope.dashboardDetail.weeklyFreshReport.highlights, function (highlightText) {
                    highlightText = highlightText.replace(/<\/?a>/g, "");
                    highlightText = highlightText.replace(/<a [^>]+>/g, "");
                    $scope.dashboardDetail.weeklyFreshReport.highlights[i] = highlightText.substring(0, 47);
                    i++;
                });
            }, function (error) {

            });
        };

        $scope.$on("$ionicView.enter", function () {
            $scope.viewEntered = true;
            $ionicSideMenuDelegate.canDragContent(true);
        });
        
        $scope.$on("$ionicView.beforeLeave", function () {
            $scope.viewEntered = false;
        });
        
        //initialization method that to be called when user visit the screen
        var _initDashboard = function () {
            //call the service method to populate the dashboard detail 
            $scope.dashboardDetail = angular.copy(dashboardService.getDashboardData());
            //condition to check that the dashboardDetail is populated or not
            if (_.isUndefined($scope.dashboardDetail.detail) || _.isNull($scope.dashboardDetail.detail) || _.isEmpty($scope.dashboardDetail.detail)) {
                //local method called in order to call API to get the dashboard detail
                _callApiGetDashboardList();
                _callApiGetWeeklyReport();
            }
        };
        
        //method prepared to call the API to refresh the date when user make the pull request by sliding the screen down
        $scope.updateDashboardDetail = function () {
            //reset the dashboard detail 
            dashboardService.resetDashboardDetail();
            _callApiGetDashboardList();
            _callApiGetWeeklyReport();
        };
        
        //method prepared to redirect the user to the web view screen when clicked on the weekly report content
        $scope.openWeeklyFreshReport = function () {
            $cordovaInAppBrowser.open('http://info.buyersedgepurchasing.com/market-report/february-25-16', '_blank');
        };

        //Initialization
        _initDashboard();
    }]);


/**
 * service prepared in order to call the API when required to populate the screen with data
 */
dashboardApp.factory('dashboardService', ['$http', '$q', 'buyersEdgeAPI', function ($http, $q, buyersEdgeAPI) {

        var service = {};
        
        var _dashboardDetail = {type: 'YTD', detail: {}};
        
        //get the dashboard detail
        service.getDashboardData = function () {
            return _dashboardDetail;
        };
        
        //set the dashboard detail
        service.setDashboardData = function (dashboardDetail) {
            _dashboardDetail = dashboardDetail;
        };
        
        //Method that call the API to get the detail of saving tab
        service.getDashboardDetail = function (code) {
            var deferred = $q.defer();

            $http.get(buyersEdgeAPI.base_url + '/dashboard?code=' + code).then(function (response) {
                _dashboardDetail.type = code;
                _dashboardDetail.detail = angular.copy(response.data.data);
                deferred.resolve(response.data.data);
            }, function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        };
        
        //method prepared to call API in order to get the weekly report
        service.getWeeklyFreshReport = function () {
            var deferred = $q.defer();

            $http.get(buyersEdgeAPI.base_url + '/weeklyreport').then(function (response) {
                _dashboardDetail.weeklyReport = angular.copy(response.data.data);
                deferred.resolve(response.data.data);
            }, function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        };
        
        //to reset the service variable of dashboard detail
        service.resetDashboardDetail = function () {
            _dashboardDetail = {type: 'YTD', detail: {}};
        };

        return service;

    }]);
