package com.brijesh.workspace.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.brijesh.workspace.model.Employee;
import com.brijesh.workspace.service.EmployeeRepository;

@RestController
@RequestMapping("/api")
public class EmployeeController {
	
	@Autowired
	EmployeeRepository employeeRepository;
	
	@RequestMapping(method = RequestMethod.POST)
	public Employee create(@RequestBody Employee employee){
	  Employee existingEmployee=null;
	  return existingEmployee;
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/employee/{employeeId}", produces="application/JSON")
	public ResponseEntity<Employee> get(@PathVariable String employeeId){
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");    
    	Employee emp = employeeRepository.loadUserByUserName("bpatel0216@gmail.com");
		if(emp!= null) {
			return new ResponseEntity<>(emp, headers, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(headers, HttpStatus.OK);
		}
	}
	
}