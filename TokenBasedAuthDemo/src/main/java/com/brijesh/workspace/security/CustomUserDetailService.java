package com.brijesh.workspace.security;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.brijesh.workspace.model.Employee;
import com.brijesh.workspace.service.EmployeeRepository;

@Service("customUserDetailsService")
public class CustomUserDetailService implements UserDetailsService{
	
	@Autowired
	private EmployeeRepository employeeRepository;

	@Override
	public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {

		if (usernameOrEmail.trim().isEmpty()) {
			throw new UsernameNotFoundException("username is empty");
		}

		Employee employee = employeeRepository.loadUserByUserName(usernameOrEmail);
		
		//System.out.print(employee.toString());
		
		if(employee == null) {
			throw new UsernameNotFoundException("User " + usernameOrEmail + " not found");
		}else {
			return new org.springframework.security.core.userdetails.User(employee.getUsername(), employee.getPassword(),true,true,true,true, new ArrayList<>());	
		}
	}
}
