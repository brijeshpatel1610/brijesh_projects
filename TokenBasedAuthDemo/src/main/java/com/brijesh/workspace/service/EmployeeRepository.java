package com.brijesh.workspace.service;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import com.brijesh.workspace.model.Employee;

@Service
public interface EmployeeRepository extends MongoRepository<Employee, String>, EmployeeCustomRepo  {

}
