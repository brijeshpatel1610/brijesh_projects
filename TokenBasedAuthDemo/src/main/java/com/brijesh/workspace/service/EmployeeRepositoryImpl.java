package com.brijesh.workspace.service;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.brijesh.workspace.model.Employee;

public class EmployeeRepositoryImpl implements EmployeeCustomRepo{

	@Autowired
	MongoTemplate mongoTemplate;
	
	@Override
	public Employee loadUserByUserName(String userName) {
		// TODO Auto-generated method stub
		  return mongoTemplate.findOne(new Query(Criteria.where("username").is(userName)), Employee.class);
		   
	}
	
	

}
