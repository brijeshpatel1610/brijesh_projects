package com.brijesh.workspace.service;

import com.brijesh.workspace.model.Employee;

public interface EmployeeCustomRepo {
	
	Employee loadUserByUserName(String userName);

	
}
